<?php

namespace App\Http\Middleware;


use Closure;
use Stevebauman\Location\Facades\Location;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {



        if ($request->is('ajax-view/*') || $request->is('translations/*')){

            return $next($request);
        }

        if ( session()->get('country',null) == null || session()->get('locale',null) == null  ){

            $position = Location::get($request->server('REMOTE_ADDR'));

            $c = null;
            $l = null;

            $countries = sessionCountries();
            try{
                foreach ( $countries as $country ){
                    if ( $country->code == $position->countryCode )
                        $c = $country->code;
                    foreach ($country->locales as $locale ){
                        $l = $locale->code;
                        break;
                    }
                }
            }catch (\Exception $e){
                $c = null;
                $l = null;
            }



            if ( $c == null || $l == null){
                session()->put('country','gg');
                session()->put('locale','gg');

            }else{
                session()->put('country',$c);

                session()->put('locale',$l);
            }

        }
        return $next($request);
    }
}
