<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessController extends Controller
{
    //
    public function MasterDesigner(Request $request){
        return view('manager.master-designer');
    }
    public function DesignerNewRequest(Request $request){
        return view('manager.designer-new-request');
    }
    public function DesignerRequest(Request $request){
        return view('manager.designer-request');
    }
}
