<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Stevebauman\Location\Facades\Location;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    public function getAjaxAPI(Request $request){
        return callAPI('get', urldecode($request->get('url')),
        [],
        getHeaders());
    }
    
    public function errorPage(){
        return view('error-page');
    }


    public function ajax(Request $request, $view){
        $variables = array();
        if($request->has('title')){
            $variables['title'] = $request->get('title');
        }
        switch($view){
            case 'where-to':
                return view('layouts.partials.where-to', $variables);
                break;
            case 'footer':
                return view('layouts.partials.footer', $variables);
                break;
            case 'categories-nav':
                return view('layouts.partials.mag.nav', $variables);
                break;
            case 'mag-article-slider':
                return view('layouts.partials.mag.article-slider', $variables);
                break;
            case 'latest-from':
                return view('layouts.partials.latest-from', $variables);
                break;
            case 'article-list':
                return view('layouts.partials.mag.article-list', $variables);
                break;
            case 'mag-article':
                return view('layouts.partials.mag.article-detail', $variables);
                break;
            case 'blog-article':
                return view('layouts.partials.blog.article-detail', $variables);
                break;
            case 'trip-how-it-works':
                return view('layouts.partials.trip.trip-nav', $variables);
                break;
            case 'trip-suggestion':
                return view('layouts.partials.trip.trip-suggestion', $variables);
                break;
            case 'mag-banner':
                return view('layouts.partials.mag.banner', $variables);
                break;
            case 'home-slider':
                return view('layouts.partials.home-slider', $variables);
                break;
            case 'trip-slider':
                return view('layouts.partials.trip.slider', $variables);
                break;
            case 'guide-slider':
                return view('layouts.partials.guide.guide-slider', $variables);
                break;
        }
    }

}

