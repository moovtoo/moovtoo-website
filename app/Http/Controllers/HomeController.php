<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendContact;
use Illuminate\Support\Facades\Session;
use Spatie\Async\Pool;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('');
    }


    public function index(Request $request)
    {


        return view('home' );
    }

    public function profile()
    {

        return view('profile');
    }

    public function mailContact(Request $request)
    {
        Mail::to('admin@moovtoo.com')
            ->send(new sendContact($request));

        try{
            $contact = json_decode(callAPI('POST', 'http://cms.moovtoo.com/api/store-contact',
                $request->all(),
                []
            ));
        }catch (\Exception $e){

        }

        return json_encode(['contactMessage' => 'Merci pour votre message, notre équipe reviendra vers vous dans les plus brefs délais.']);
    }

    public function changeCountry(Request $request , $code){

        $countries = sessionActiveCountries();
        $lan = 'gg';
        foreach ($countries as $country ){
            if ( strtolower($country->code) == strtolower($code)){
                foreach ($country->locales as $locale){
                    $lan = $locale->code;
                    break;
                }
                break;
            }
        }
        session()->put('country',$code);
        session()->put('locale',$lan);

        return redirect()->back();
    }
    public function changeLocale(Request $request , $code){

        session()->put('locale',$code);

        return redirect()->back();
    }
    public function setSession(Request $request){
        $data = $request->all();
        foreach ( $data as $k=>$v ){
            session()->put('moovtoo.'.$k , $v );
            // Log::info($k .":".$v);
        }
        return response()->json('success', 200);
    }
}
