<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuideController extends Controller
{
    //
    public function __construct()
    {
        // $this->middleware('');

    }

    public function subCategory(Request $request)
    {
        session()->put('moovtoo.domain','guide');

        return view('guide.sub-category-list');
    }

    public function index(Request $request){
        session()->put('moovtoo.domain','guide');

        return view('guide.home');
    }
    
    public function category(Request $request){
        session()->put('moovtoo.domain','guide');

        return view('guide.category');
    }
    public function detail(Request $request){
        session()->put('moovtoo.domain','guide');

        return view('guide.detail');
    }


    public function city(Request $request){
        session()->put('moovtoo.domain','guide');
        return view('guide.city');
    }



}
