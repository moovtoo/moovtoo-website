<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ( $data['email']!= ''){
            $user = User::query()->where('email',$data['email'] )->first();
            if ( $user != null and $user->deleted_at != null ){
                return Validator::make($data, [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255'],
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
                ]);
            }
        }

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        if ( $data['email']!= ''){
            $user = User::query()->where('email',$data['email'] )->first();
            if ( $user != null and $user->deleted_at != null ){
                 User::query()->where('email',$data['email'])->first()->forceDelete();
            }
        }
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function customRegister(Request $request)
    {
        // $this->validateLogin($request);
        $response = json_decode(callAPI(
            'POST', 
            'http://cms.moovtoo.com/api/register',
            $request->except('_token'),
            []
        ));

        if(array_has($response, 'success')){
            return json_encode(['register_message'=>$response['message']]);
        }else{
            return json_encode(['register_error'=>$response['error']]);
        }
    }

    public function verifyUser($token){
        try{
            $response = json_decode(callAPI('GET', 'http://cms.moovtoo.com/api/user/verify',
                ['token'=>$token],
                []
            ));
            return redirect()->route('login', ['message'=>$response->success]);
        }catch (\Exception $e){

        }
    }
}
