<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if(getUser() != null){
            return redirect()->route('profile');
        }
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

//        if($user!=null and $user->deleted_at != null) {
//            return redirect()->route('login')->withErrors(['custom_error'=>['Your account has been deleted.']]);
//        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    
    public function customLogin(Request $request)
    {
        // $this->validateLogin($request);
        $response = json_decode(callAPI(
            'POST', 
            'http://cms.moovtoo.com/api/login',
            $request->except('_token'),
            []
        ));

        if(array_has($response, 'success')){
            $token = $response->success->token;
            $user = $response->success->user;
            session()->put('user',$user);
            session()->put('cms_token', $token);
        }else{
            return json_encode(['login_error'=>$response['error']]);
        }

        

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        // if ($this->hasTooManyLoginAttempts($request)) {
        //     $this->fireLockoutEvent($request);

        //     return $this->sendLockoutResponse($request);
        // }

        // $user = DB::table('users')->where('email', $request->input('email'))->get()->first();
        // if($user!=null and $user->deleted_at != null) {
        //     return redirect()->route('login')->withErrors(['custom_error'=>['Your account has been deleted.']]);
        // }

        // if ($this->attemptLogin($request)) {
        //     return $this->sendLoginResponse($request);
        // }

        // // If the login attempt was unsuccessful we will increment the number of attempts
        // // to login and redirect the user back to the login form. Of course, when this
        // // user surpasses their maximum number of attempts they will get locked out.
        // $this->incrementLoginAttempts($request);

        // return $this->sendFailedLoginResponse($request);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        if($provider == 'facebook'){
            $user = Socialite::driver('facebook')->stateless()->user();
        }else if($provider == 'google'){
            $user = Socialite::driver('google')->stateless()->user();
        }
        $response = json_decode(
            callAPI(
                'GET', 
                'http://cms.moovtoo.com/api/provider-login',
                ['user'=>$user, 'provider'=>$provider],
                []
            )
        );
        // dd($response->user);
        Session::put('user' , collect($response->user) );
        Session::put('token' , $response->token );
        // $authUser = $this->findOrCreateUser($user, $provider);
        // Auth::login($authUser);
        return redirect()->route('home');
    }

    protected function authenticated(Request $request, $user)
    {
//        dd($user);
        if ($user->status == 'Active') {
            if (!$user->isAdmin()) {
                return redirect()->route('welcome');
            } elseif ($user->isAdmin()) {
                return redirect()->route('home');
            }
        }
        elseif($user->status == 'Inactive') {
            Auth::logout();
            return redirect()->route('login')->withErrors(['custom_error'=>['Your account is not active yet.']]);
        }

        else {
            Auth::logout();
            return redirect()->route('login')->with('custom_error', 'Your account had been deactivated.');
        }


    }

    public function logout(Request $request)
    {
        // $this->guard()->logout();

        // $request->session()->invalidate();

      session()->forget('user');
        session()->forget('token');

        return redirect('/login');
    }
}
