<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DayByDayController extends Controller
{
    //
    public function DayOne(Request $request){
        return view('day.day-one');
    }
    public function AllDays(Request $request){
        return view('day.all-days');
    }
}
