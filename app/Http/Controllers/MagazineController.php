<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;

class MagazineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('');

    }


    public function all(Request $request, $slug = null  )
    {
        session()->put('moovtoo.domain','mag');
        return view('magazine.list',compact('slug'));
    }

    public function index(Request $request){
        session()->put('moovtoo.domain','mag');
        return view('magazine.home');
    }
    
    public function articleDetail(Request $request, $category, $slug){
        session()->put('moovtoo.domain','mag');
        return view('magazine.article-detail', compact('slug'));
    }

}
