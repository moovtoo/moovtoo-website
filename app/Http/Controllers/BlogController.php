<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('');

    }


    public function all(Request $request, $slug = null  )
    {
        session()->put('moovtoo.domain','blog');
        return view('blog.list',compact('slug'));
    }

    public function index(Request $request){
        session()->put('moovtoo.domain','blog');
        return view('blog.home');
    }
    
    public function articleDetail(Request $request, $category, $slug){
        session()->put('moovtoo.domain','blog');
        return view('blog.article-detail', compact('slug'));
    }

}
