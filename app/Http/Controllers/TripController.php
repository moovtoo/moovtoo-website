<?php

namespace App\Http\Controllers;

use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TripController extends Controller
{
    public function index(Request $request){

        return view('trip.home');
    }
    public function create(Request $request){
        if ( $request->has('location')){
            $travelLocation = callAPIData('get',"http://cms.moovtoo.com/api/gen/v1/travel-location",
                ['slug'=>str_slug($request->input('location'))])[0];
            return view('trip.create-trip',compact('travelLocation'));
        }
        return view('trip.create-trip');

    }
    public function question(Request $request){

        return view('trip.question-trip');
    }
    public function questionStep2(Request $request){

        return view('trip.question-trip-step-2');
    }
    public function questionStep3(Request $request){

        return view('trip.question-trip-step-3');
    }
    public function questionPaymentPlan(Request $request){

        return view('trip.payment-plan');
    }
}
