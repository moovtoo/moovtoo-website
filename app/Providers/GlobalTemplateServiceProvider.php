<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GlobalTemplateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer(['layouts.partials.header'], function ($view) {
            $view->with('current_country', $this->getCurrentCountry());
            $view->with('current_locale', $this->getCurrentLocale());
            $view->with('countries', sessionActiveCountries());
        });
        
        view()->composer(['auth.login', 'layouts.partials.profile.about'], function ($view) {
            $view->with('countries', sessionCountries());
        });
        view()->composer(['auth.login', 'layouts.partials.trip.trip-suggestion'], function ($view) {
            $view->with('trips', $this->getTrips());
        });

        view()->composer(['layouts.partials.home-nav'], function ($view) {
            $view->with('domains', sessionDomains());
        });
        
        view()->composer(['layouts.partials.footer'], function ($view) {
            $view->with('storyCategories', $this->articleCategories('mag'));
            $view->with('blogCategories', $this->articleCategories('blog'));
        });
        view()->composer(['layouts.partials.mag.banner'], function ($view) {
            $view->with('banners', $this->magBanner());
        });
        view()->composer(['layouts.partials.home-slider'], function ($view) {
            $view->with('banners', $this->magBanner());
        });
        view()->composer(['layouts.partials.guide.guide-slider'], function ($view) {
            $view->with('banners', $this->magBanner());
        });
        view()->composer(['layouts.partials.trip.slider'], function ($view) {
            $view->with('banners', $this->magBanner());
        });
        view()->composer(['layouts.partials.mag.nav'], function ($view) {
            $view->with('categories', $this->articleCategories());
        });
        view()->composer(['layouts.partials.blog.nav'], function ($view) {
            $view->with('categories', $this->articleCategories());
        });
        view()->composer(['layouts.partials.mag.article-slider'], function ($view) {
            $view->with('articles', $this->articleSlider('mag'));
        });
        view()->composer(['layouts.partials.latest-from'], function ($view) {
            $view->with('latestBlog', $this->latestFrom('blog'));
            $view->with('latestMag', $this->latestFrom('mag'));
        });
        view()->composer(['layouts.partials.mag.article-list'], function ($view) {
            $view->with('articles', $this->articleList());
        });
        view()->composer(['layouts.partials.mag.article-detail'], function ($view) {
            $view->with('article', $this->getArticle('mag'));
            $view->with('res2', $this->articleList(null, 2));
        });
        view()->composer(['layouts.partials.blog.article-detail'], function ($view) {
            $view->with('article', $this->getArticle('blog'));
            $view->with('res2', $this->articleList(null, 2));
        });
        view()->composer(['layouts.partials.trip.trip-nav'], function ($view) {
            $view->with('howItWorks', $this->getTripHow());
        });

        view()->composer(['layouts.partials.where-to'], function ($view) {
            $view->with('locations', $this->getTravelLocations());
        });
        view()->composer(['layouts.partials.home-slider'], function ($view) {
            $view->with('travelLocations', $this->getTopTravelLocations());
        });
    }

    /**
     * Register services. 
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function articleSlider($sub){
        return
            callAPIData(
                'get', 
                'http://cms.moovtoo.com/api/gen/v1/articles',
                ['page'=>'1'],
                getHeaders($sub)

        );
    }


    public function getTravelLocations(){
        return
            callAPIData(
                'get',
                'http://cms.moovtoo.com/api/gen/v1/travel-locations',
                []
            );
    }
    public function getTopTravelLocations(){
        return
            callAPIData(
                'get',
                'http://cms.moovtoo.com/api/gen/v1/top-travel-location',
                ['is_top'=>1],
                []
            );
    }
    public function getTripHow(){
        return
            callAPIData(
                'get', 
                'http://cms.moovtoo.com/api/gen/v1/trip-how-it-works',
                [],
                getHeaders()

        );
    }
    
    public function getTrips(){
        return
            callAPIData(
                'get', 
                'http://cms.moovtoo.com/api/gen/v1/trip',
                [],
                getHeaders()

        );
    }
    
    public function getArticle($sub, $slug = null){
        $slug = $this->app->request->get('article');
        // dd($slug);
        try{
            return
                callAPIData(
                    'get',
                    'http://cms.moovtoo.com/api/gen/v1/get-article',
                    ['slug'=>$slug],
                    getHeaders($sub)
                )[0];
        }catch ( \Exception $e){
            return null;
        }

    }

    public function latestFrom($sub){
        try{
        return
            callAPIData(
                'get', 
                'http://cms.moovtoo.com/api/gen/v1/get-latest-blog',
                [],
                getHeaders($sub)
            )[0];
        }catch ( \Exception $e){
            return null;
        }
    }

    public function articleCategories($sub = null){
        return
            callAPIData('get', 'http://cms.moovtoo.com/api/gen/v1/article-categories',
            [],
            $sub == null ? getHeaders(): getHeaders($sub)
        );
    }

    public function magBanner($sub = null){
        return
            callAPIData('get', 'http://cms.moovtoo.com/api/gen/v1/sliders',
            [],
            $sub == null ? getHeaders(): getHeaders($sub)
            );
    }
    
    public function articleList($slug = null, $page = 1){
        if($this->app->request->has('category')){
            $slug = $this->app->request->get('category');
        }
        return
            json_decode(
                callAPI('get', 'http://cms.moovtoo.com/api/gen/v1/articles',
                    $slug == null ? ['page'=>$page]: ['page'=>$page , 'slug'=> $slug],
                    getHeaders()
            )
            ,false)->data;
    }

    public function getCurrentCountry(){
        try{

            return  session()->get('country');
        }catch (\Exception $exception ){
            return "LB";
        }
    }

    public function getCurrentLocale(){
        try{

            return  session()->get('locale');
        }catch (\Exception $exception ){
            return "LB";
        }
    }
}
