<?php

namespace App;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Session;

class User implements Authenticatable
{
    /**
     * @return  string
     */
    public function getAuthIdentifierName()
    {
        try{
            $user = session()->get('moovtoo.user');
            return $user['name'];
        }catch (\Exception $e){
            return null;
        }
    }

    /**
     * @return  mixed
     */
    public function getAuthIdentifier()
    {
        try{
            $user = session()->get('moovtoo.user');
            return $user['id'];
        }catch (\Exception $e){
            return null;
        }
    }

    /**
     * @return  string
     */
    public function getAuthPassword()
    {
        return null;
    }

    /**
     * @return  string
     */
    public function getRememberToken()
    {
        try{
            return session()->get('moovtoo.token');
        }catch (\Exception $e){
            return null;
        }
    }

    /**
     * @param    string  $value
     * @return  void
     */
    public function setRememberToken($value)
    {
        // Store a new token user for the "remember me" functionality
    }

    /**
     * @return  string
     */
    public function getRememberTokenName()
    {
        // Return the name of the column / attribute used to store the "remember me" token
    }
}
