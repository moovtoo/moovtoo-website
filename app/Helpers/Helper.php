<?php

if (! function_exists('alias')) {
    function alias($default ) {
        $old_locale = strtolower(session()->get("moovtoo.old_local","en"));
        $loc =  session()->get('locale','en');
        if ( $loc == 'gg')
            $loc ='en';
        if ( $old_locale != $loc){
            session()->forget("moovtoo.translateList");
        }

        $listMap = session()->get("moovtoo.translateList",null);
        if ($listMap == null){
            try{
                session()->put("moovtoo.old_local", strtolower(session()->get('locale','en')));
                // \Illuminate\Support\Facades\Log::info("http://cms.moovtoo.com/api/translations/$loc");
                $listMap =  json_decode(callAPI('GET',"http://cms.moovtoo.com/api/translations/$loc"),true)['data'];
                session()->put('moovtoo.translateList',$listMap);
            }catch(\Exception $e){

            }
        }

        if ( !array_has($listMap , str_slug($default)) ){
            callAPI('POST',"http://cms.moovtoo.com/api/translation",['default'=>$default]);
        }
        return array_has($listMap , str_slug($default))? $listMap[str_slug($default)] : 'missing_'.str_slug($default).'_translation';
    }
}

if ( ! function_exists( 'sessionCountries') ){

    function sessionCountries(){
        if ( session()->get('moovtoo.session_countries',null) == null ){
            session()->put('moovtoo.session_countries',json_decode(callAPI('get',"http://cms.moovtoo.com/api/countries/0"))->data);
        }
            return session()->get('moovtoo.session_countries');

    }

}

if ( ! function_exists( 'sessionActiveCountries') ){

    function sessionActiveCountries(){
        if ( session()->get('moovtoo.session_active_countries',null) == null ){
            session()->put('moovtoo.session_active_countries',json_decode(callAPI('get',"http://cms.moovtoo.com/api/countries/1"))->data);
        }
        return session()->get('moovtoo.session_active_countries');

    }

}

if ( ! function_exists( 'getHeaders') ){

    function getHeaders( $domain = null ){
        if ( session()->get('country',null) != null && session()->get('country',null) != 'gg'){
            $header[]="country:".session()->get('country');
        }

        if ( session()->get('locale',null) != null && session()->get('locale',null) != 'gg'){
            $header[]="language:".session()->get('locale');
        }
        if ($domain== null) {
            $dom = array_first(explode('.', str_replace('http://','',str_replace('https://','',request()->url()))));
            if($dom == 'moovtoo'){
                $dom .= ".com";
            }
            $header[] = "domain:" . $dom;
        }
        else
            $header[]="domain:$domain";

        return $header;
    }

}

if ( ! function_exists( 'getUser') ){

    function getUser(){
        return session()->get('moovtoo.user', null);
    }

}

if ( ! function_exists( 'sessionDomains') ){

    function sessionDomains(){

        if ( session()->get('moovtoo.session_domains',null) == null ){
            session()->put('moovtoo.session_domains',json_decode(callAPI('get',"http://cms.moovtoo.com/api/domains"))->data);
        }
        return session()->get('moovtoo.session_domains');

    }

}

if ( ! function_exists( 'callAPI')){

    function callAPI($method, $url, $data=null , $header = array() ){
        $curl = curl_init();

         \Illuminate\Support\Facades\Log::info($url);
         \Illuminate\Support\Facades\Log::info(implode(',',$header));
         \Illuminate\Support\Facades\Log::info('call');
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        $header [] = 'Content-Type: application/json';

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        curl_close($curl);

            return $result;
    }
}

if ( ! function_exists( 'callAPIData')){

    function callAPIData($method, $url, $data=null , $header = array() ){
        $curl = curl_init();

        // \Illuminate\Support\Facades\Log::info($url);
        // \Illuminate\Support\Facades\Log::info(implode(',',$header));
        // \Illuminate\Support\Facades\Log::info('call');
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        $header [] = 'Content-Type: application/json';

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        curl_close($curl);

        try{
            return json_decode($result,false)->data->data;
        }catch ( Exception $e){
            return array();
        }

    }
}