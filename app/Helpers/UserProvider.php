<?php
/**
 * Created by PhpStorm.
 * User: rabeearkadan
 * Date: 2/21/19
 * Time: 12:13 PM
 */

namespace App\Helpers;



use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as IlluminateUserProvider;
use Illuminate\Support\Facades\Session;

class UserProvider implements IlluminateUserProvider
{

    public function retrieveById($identifier)
    {
        try{
            return session()->get('moovtoo.user');
        }catch (\Exception $e){
            return null;
        }
    }


    public function retrieveByToken($identifier, $token)
    {
        try{
            return session()->get('moovtoo.user');
        }catch (\Exception $e){
            return null;
        }
    }


    public function updateRememberToken(Authenticatable $user, $token)
    {
        // Save the given "remember me" token for the given user
    }


    public function retrieveByCredentials(array $credentials)
    {
        // Get and return a user by looking up the given credentials
    }


    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        // Check that given credentials belong to the given user
    }

}