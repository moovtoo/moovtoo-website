<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$domain =  parse_url(config('app.url'), PHP_URL_HOST);


    $domain =  parse_url(config('app.url'), PHP_URL_HOST);
    Route::domain($domain)->group(function () {
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::get('logout', 'Auth\LoginController@logout')->name('logout');
        Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
    });


    Route::domain('mag.'.$domain)->group(function () {
        Route::get('/all', 'MagazineController@all')->name('mag.all');
        Route::get('/', 'MagazineController@index')->name('mag.home');
        Route::get('category/{slug?}', 'MagazineController@all')->name('mag.category');
        Route::get('article/{category?}/{slug?}', 'MagazineController@articleDetail')->name('mag.article.detail')->where('slug','.+');
    });



    Route::domain('blog.'.$domain)->group(function () {
        Route::get('/', 'BlogController@index')->name('blog.home');
        Route::get(strtolower(alias('category')).'/{slug?}', 'BlogController@all')->name('blog.category');
        Route::get(strtolower(alias('article')).'/{category?}/{slug?}', 'BlogController@articleDetail')->name('blog.article.detail')->where('slug','.+');
    });

    Route::domain('guide.'.$domain)->group(function () {
        Route::get('/category/{slug?}', 'GuideController@category')->name('guide.category');
        Route::get('/','GuideController@index')->name('guide.home');
        Route::get('/sub-category/{slug?}/{sub?}', 'GuideController@subCategory')->name('guide.sub.category');
        Route::get('/detail/{slug?}/{sub?}', 'GuideController@detail')->name('guide.detail');
        Route::get('/city/{slug?}', 'GuideController@city')->name('city-guide');

    });


    Route::domain('trip.'.$domain)->group(function () {
        Route::get('/','TripController@index')->name('trip.home');
        Route::post('/create-trip','TripController@create')->name('trip.create-trip');
        Route::get('/question-trip','TripController@question')->name('trip.question-trip');
        Route::get('/question-trip-step-2','TripController@questionStep2')->name('trip.question-trip-step-2');
        Route::get('/question-trip-step-3','TripController@questionStep3')->name('trip.question-trip-step-3');
        Route::get('/payment-plan','TripController@questionPaymentPlan')->name('trip.payment-plan');
    });

    Route::domain('business.'.$domain)->group(function () {
        Route::get('/master-designer','BusinessController@MasterDesigner')->name('manager.master-designer');
        Route::get('/designer-new-request','BusinessController@DesignerNewRequest')->name('manager.designer-new-request');
        Route::get('/designer-request','BusinessController@DesignerRequest')->name('manager.designer-request');
    });

    Route::get('/profile/day-one', 'DayByDayController@DayOne')->name('day.day-one');
    Route::get('/profile/all-days', 'DayByDayController@AllDays')->name('day.all-days');
    Route::get('ajax-view/{view?}','Controller@ajax')->name('ajax.view');
    Route::get('/country/{code}', 'HomeController@changeCountry')->name('country.change');
    Route::post('/session', 'HomeController@setSession')->name('session.post');

    Route::domain('trip.'.$domain)->group(function () {
        Route::get('/','TripController@index')->name('trip.home');
        Route::get('/create-trip','TripController@create')->name('trip.create-trip');
        Route::get('/question-trip','TripController@question')->name('trip.question-trip');
        Route::get('/question-trip-step-2','TripController@questionStep2')->name('trip.question-trip-step-2');
        Route::get('/question-trip-step-3','TripController@questionStep3')->name('trip.question-trip-step-3');
        Route::get('/payment-plan','TripController@questionPaymentPlan')->name('trip.payment-plan');
    });


    Route::group([ 'prefix'=>'dev'], function () {
        Route::get('/', 'HomeController@index')->name('home');
    });

    Route::group([ 'prefix'=>'dev','middleware'=>['c_auth']], function () {
        Route::get('/profile', 'HomeController@profile')->name('profile');
    });
    Route::post('/contact-us/mail','HomeController@mailContact')->name('contact.post');


    Route::get('ajax-view/{view?}','Controller@ajax')->name('ajax.view');
    Route::get('/country/{code}', 'HomeController@changeCountry')->name('country.change');
    Route::get('/locale/{code}', 'HomeController@changeLocale')->name('locale.change');
    Route::post('/session', 'HomeController@setSession')->name('session.post');

    Route::get('/ajax-get-api', 'Controller@getAjaxAPI')->name('ajax-get-api');
    Route::get('/error', 'Controller@errorPage')->name('error');

    Route::get('/', function () {
        return view('moovtoo-welcome');
    })->name('welcome');


