@extends('layouts.app')

{{-- @section('title', $article->name.' - ') --}}
@push('css')
    <style>
        #ajax-categories-nav{
            padding-top:50px;
        }
    </style>
@endpush

@section('content')
    <section id="ajax-categories-nav">

    </section>
    @include('layouts.partials.advertisement', ['name'=>'test'])
    <section id="ajax-blog-article">

    </section>
    {{-- @include('layouts.partials.mag.article-detail', ['article'=>$article, 'sub'=>'mag']) --}}
    @include('layouts.partials.advertisement', ['name'=>'test'])
    @include('layouts.partials.mag.footer')
@endsection

@push('js')
    <script>
        $(function(){
            callAjax('categories-nav', 'ajax-categories-nav');
            callAjax('blog-article', 'ajax-blog-article', '?article='+'{{$slug}}');
        })
    </script>
@endpush