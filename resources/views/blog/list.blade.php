@extends('layouts.app')

@section('content')
    @include('layouts.partials.mag.banner')
    <section id="ajax-categories-nav">

    </section>
    @include('layouts.partials.mag.article-list', ['sub'=>'blog'])
    @include('layouts.partials.advertisement', ['name'=>'test'])

    @include('layouts.partials.mag.footer')
@endsection

@push('js')
    <script>

        $(function(){
            callAjax('categories-nav', 'ajax-categories-nav');
        })

        $('.btn-article-list-more').on('click', function(){
            var url = $(this).attr('data-url');
            var slug = '{{ $slug == null ? "": $slug }}';
            $.ajax({
                type: "GET",
                url: '{{route('ajax-get-api')}}'+'?url='+ encodeURIComponent(url +  (slug == "" ?"":"&slug=" + slug)),
                success: function(data)
                {
                    data = JSON.parse(data);
                    data = data.data;
                    var next_url = data.next_page_url;
                    var articles = data.data;
                    if(next_url != null)
                        $('.btn-article-list-more').attr('data-url', next_url);
                    else
                        $('.btn-article-list-more').hide();
                    div = '<div class="row">';
                    $.each(articles, function(key, value){
                        if(key != 0 && key%2 == 0){
                            div += '</div><div class="row">';
                        }
                        var sharamit = "";
                        var cats = value.categories;
                        for( var i in cats){
                            if(cats[i].parent_id != null){
                                sharamit += cats[i].name;
                            }
                        }
                        div +=  '<div class="col-md-6">'
                                +'<div class="post-box">'
                                +'<div class="post-box-img">'
                                +'<a href="{{route('blog.article.detail')}}/'+value.categories[0].slug+'/'+value.slug+'">'
                                +'<img src="http://cms.moovtoo.com/public/'+value.photo+'" class="post-box-img-main img-responsive">'
                                +'<img src="{{asset('images/horizontal-overlay.png')}}" class="post-box-img-overlay">'
                                // +'<p>'+value.photo_caption+'</p>'
                                +'</a>'
                                +'</div>'
                                +'<div class="post-box-details">'
                                +'<div class="post-box-date"><strong>'
                                + String(sharamit)
                                +'</strong> '+value.date_created+'</div>'
                                +'<div class="post-box-title">'
                                +'<a href="{{route('blog.article.detail')}}/'+value.categories[0].slug+'/'+value.slug+'">'
                                +value.name
                                +'</a>'
                                +'</div>'
                                +'</div>'
                                +'</div>'
                                +'</div>';
                    })
                    div += '</div>';
                    $('#article-list').append(div);
                },
                error:function( data){
                    // console.log(data);
                }
            });
        })
    </script>
@endpush