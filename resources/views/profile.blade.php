@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fafafb;
        }
    </style>   
@endpush

@section('content')
    @include('layouts.partials.profile.user-header')
    @include('layouts.partials.profile.user-stats')
    @include('layouts.partials.profile.profile-tabs')
    @include('layouts.partials.profile.about')
    @include('layouts.partials.profile.interests')
@endsection

@push('js')
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>   
    <script>
        CKEDITOR.replace( 'description' );
    </script>

    <script>
        $(document).ready(function(){
            $('.stat-number').each(function () {
                var $this = $(this);
                jQuery({ Counter: 0 }).animate({ Counter: $this.attr('data-count') }, {
                    duration: 2000,
                    easing: 'swing',
                    step: function () {
                    $this.text(Math.ceil(this.Counter));
                    }
                });
            });
        })

        $('.profile-tab').on('click', function(){
            target= $(this).attr('target');
            location.href="#"+target;
            profileTab(target);
        })

        $('.profile-dropdown-title').on('click', function(){
            if($(this).next('.profile-dropdown-list').css('display') == "none"){
                $(this).next('.profile-dropdown-list').slideDown(400);
                $(this).find('i').removeClass('fa-plus');
                $(this).find('i').addClass('fa-minus');
            }else{
                $(this).next('.profile-dropdown-list').slideUp(400);
                $(this).find('i').removeClass('fa-minus');
                $(this).find('i').addClass('fa-plus');
            }
        })

        $(function(){
            ref = window.location.href.split('#')[1];
            if(ref != ""){
                profileTab(ref);
            }
        })

        function profileTab(ref){
            tab = $('.profile-tab[target="'+ref+'"]');
            target = $(tab).attr('target');
            $('.profile-tab').each(function(key, value){
                $(value).removeClass('active');
            })
            $(tab).addClass('active');
            $('.profile-tab-content').each(function(key, value){
                $(value).removeClass('active');
            })
            $('.profile-tab-content#'+target).addClass('active');
        }
    </script>
@endpush
