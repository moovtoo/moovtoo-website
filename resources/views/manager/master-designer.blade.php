@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fbfbfb;
        }
    </style>
@endpush

@section('content')
    @include('layouts.partials.header')
    @include('layouts.partials.manager.designer-banner')
    @include('layouts.partials.manager.designer-nav')
    {{--@include('layouts.partials.manager.new-request-table')--}}


@endsection
@push('js')
    <script>
        $(function(){
            $('.select-request-table .select2-selection__arrow b').html('<i class="fa fa-angle-down"></i>');
            // $('.notification-number').hide();
            // $('.table-delete-img').hide();
        })
    </script>
@endpush