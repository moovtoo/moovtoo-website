<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{env('APP_NAME')}}</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <style type="text/css">
        table { border-collapse: collapse;}
    </style>
</head>
<body>
<section class="row">
    <div class="pull-left">
          Bonjour Admin, <br />
    </div>
</section>
<section class="row">
    <div class="col-md-12">
        <h3>Message de contact: </h3>
        <h5>Nom: {{$name}} </h5>
        <h5>Email: {{$email}}</h5>
        <h5>Vous etes: {{$you_are}}</h5>
        <h5>Message: {!!$contactMessage!!}</h5>

    </div>
</section>
</body>
</html>

