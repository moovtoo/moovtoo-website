<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="shortcut icon" type="image/x-icon" href="{{asset(config('app.favicon', ''))}}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset("images/fav.png") }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="icon" href="{{asset(config('app.favicon', ''))}}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <!-- Styles -->
        <style>
            @font-face {
                font-family: 'poppins-t';
                src: url('../../public/fonts/Poppins-Thin.ttf') format('truetype'),
                url('../../public/fonts/Poppins-Thin.ttf') format('truetype');
                font-weight: 100;
                font-style: normal;
            }
            @font-face {
                font-family: 'poppins-sb';
                src: url('../../public/fonts/Poppins-SemiBold.ttf') format('truetype'),
                url('../../public/fonts/Poppins-SemiBold.ttf') format('truetype');
                font-weight: 100;
                font-style: normal;
            }
            @font-face {
                font-family: 'poppins-r';
                src: url('../../public/fonts/Poppins-Regular.ttf') format('truetype'),
                url('../../public/fonts/Poppins-Regular.ttf') format('truetype');
                font-weight: 100;
                font-style: normal;
            }
            @font-face {
                font-family: 'poppins-m';
                src: url('../../public/fonts/Poppins-Medium.ttf') format('truetype'),
                url('../../public/fonts/Poppins-Medium.ttf') format('truetype');
                font-weight: 100;
                font-style: normal;
            }
            @font-face {
                font-family: 'poppins-l';
                src: url('../../public/fonts/Poppins-Light.ttf') format('truetype'),
                url('../../public/fonts/Poppins-Light.ttf') format('truetype');
                font-weight: 100;
                font-style: normal;
            }
            @font-face {
                font-family: 'poppins-b';
                src: url('../../public/fonts/Poppins-Bold.ttf') format('truetype'),
                url('../../public/fonts/Poppins-Bold.ttf') format('truetype');
                font-weight: 100;
                font-style: normal;
            }
        </style>
        <style>
            html, body {
                background-color: white;
                margin: 0;
            }
            .row{
                padding: 0;
                margin: 0;
            }
            section{
                margin: 50px 0;
            }
            section.logo{
                margin: 20px 0;
            }
            .welcome-logo{
                display: flex;
            }
            .welcome-logo img{
                width: 120px;
                margin: auto;
            }
            .welcome-box-grey{
                background-color: #f7f7f7;
                text-align: center;
                font-size: 35px;
                padding: 60px 0;
                font-family: 'poppins-l';
            }
            .welcome-box-grey strong{
                font-weight: 100;
                font-family: 'poppins-b';
            }
            .welcome-title{
                font-size: 35px;
                text-align: center;
                margin-bottom: 40px;
                font-family: 'poppins-l';
            }
            .welcome-title strong{
                font-family: 'poppins-b';
                font-weight: 100;
            }
            .welcome-title .underline{
                width: 35px;
                margin: 0 auto;
                background-color: #e59023;
                height: 4px;
                margin-top: 5px;
            }
            .welcome-text{
                font-size: 22px;
                text-align: left;
                font-family: 'poppins-l';
                letter-spacing: 1px;
            }
            .welcome-btn{
                background-color: transparent;
                border: 1px solid black;
                width: 190px;
                height: 50px;
                text-align: center;
                font-size: 13px;
                display: block;
                color: black;
                text-transform: uppercase;
                margin: 0 auto;
                margin-top: 35px;
                font-family: 'poppins-b';
            }
            .item-box{
                margin-bottom: 35px;
            }
            .item-image{
                border: 1px solid #e7e7e7;
                height: 400px;
                display: flex;
                margin-bottom: 20px;
                font-family: 'poppins-b';
            }
            .item-image img{
                margin: auto;
                display: block;
                max-height: 350px;
                width: 90%;
            }
            .item-title{
                margin-bottom: 35px;
                font-size: 23px;
                font-family: 'poppins-sb';
                min-height: 73px;
            }
            .item-title .underline{
                height: 4px;
                width: 40px;
                margin-top: 5px;
            }
            .item-title .underline.green{
                background: #85ad78;
            }
            .item-title .underline.orange{
                background: #e59023;
            }
            .item-description{
                font-size: 17px;
                margin-bottom: 30px;
                font-family: 'poppins-l';
            }
            .item-link a{
                color: black;
                font-size: 17px;
                font-family: 'poppins-r';
            }
            .welcome-small-title{
                font-size: 22px;
                text-align: center;
                margin-bottom: 25px;
                font-family: 'poppins-l';
            }
            .contact-box{
                border: 1px solid #e0e0e0;
                background: #fbfbfb;
                padding: 40px 30px;
            }
            .contact-form > input{
                width: 100%;
                border: 1px solid #e0e0e0;
                height: 40px;
                padding-left: 15px;
                color: black;
            }
            .contact-form{
                margin-bottom: 20px;
            }
            .contact-form > input::placeholder, .contact-form > textarea::placeholder{
                color: black
            }
            .contact-form >  textarea{
                width: 100%;
                height: 100px;
                border: 1px solid #e0e0e0;
                padding: 15px;
            }
            .checkbox-inline{
                padding: 15px 0;
                padding-left: 20px;
                font-family: 'poppins-r';
                letter-spacing: 1px;
            }
            .checkbox-inline > input{

            }
            .contact-btn{
                background: transparent;
                border: 0;
                color: black;
                font-size: 24px;
                border-bottom: 1px solid black;
                display: block;
                margin: 15px auto;
                padding: 0;
                font-family: 'poppins-b';
            }
            .contact-policy-box{
                background: white;
                padding: 30px;
                border: 1px solid #e0e0e0;
                margin-top: 30px;
                letter-spacing: 1px;
                font-family: 'poppins-l';
            }
            #contactMessage{
                display: none;
                text-align: center;
                font-size: 15px;
                margin-bottom: 10px;
            }
            .help-block{
                color: red;
                margin: 0;
                font-size: 13px;
                margin-top: 2px;
            }
            button:focus, button:active, a:focus, a:active, .select2-container--default .select2-selection--single:focus{
                outline: none;
            }
            .select2-container--default .select2-selection--single{
                height: 35px;
                border: 1px solid #e0e0e0;
                border-radius: 0;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered{
                line-height: 33px;
                padding-left: 15px;
                color: black;
            }
            .select2-container--default .select2-selection--single .select2-selection__arrow{
                height: 33px;
                right: 5px;
            }
            .select2-container--default .select2-selection--single .select2-selection__arrow b{
                border-color: #000 transparent transparent transparent;
            }
            .flex-wrap{
                display: flex;
                flex-wrap: wrap;
            }
            .flex-wrap [class*="col-"]{
                display: flex;
                flex-direction: column;
            }
            @media(max-width:991px){
                .welcome-box-grey{
                    font-size: 30px;
                }
                .flex-wrap [class*="col-"]{
                    width: 100%;
                }
                .flex-wrap [class*="col-"]{
                    padding: 0;
                }
                .contact-box{
                    padding: 40px 10px;
                }
                .item-title{
                    min-height: unset;
                }
                .welcome-logo{

                }
            }
            @media(max-width:767px){
                .welcome-box-grey{
                    font-size: 25px;
                }
                .container{
                    padding:0;
                }
                .padding-xs-0{
                    padding: 0;
                }
                .welcome-text{
                    font-size: 20px;
                }
            }
        </style>
    </head>
    <body>

        <div class="container">
            <section class="logo">
                <div class="row">
                    <div class="col-md-12">
                        <div class="welcome-logo">
                            <img src="{{asset('images/moovtoo-welcome.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-md-12 padding-xs-0">
                        <div class="welcome-box-grey">
                            La plateforme qui met <strong>l’Intelligence Artificielle</strong><br>
                            au service d’un <strong>tourisme authentique,</strong>  <br>
                            <strong>durable</strong> et <strong>personnalisé</strong>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="welcome-box">
                            <div class="welcome-title">
                                Notre promesse
                                <div class="underline"></div>
                            </div>
                            <div class="welcome-text">
                                MOOVTOO est né pour mettre en relation les amoureux du voyage avec des acteurs peu visibles, professionnels d’un tourisme local et authentique. Nous vous promettons de belles découvertes, au fil de chemins que vous n’auriez pas forcément empruntés. C’est cela aussi, MOOVTOO : une intelligence artificielle éthique et responsable, qui tient compte de vos préférences mais vous emmène au-delà de vos centres d’intérêts habituels, et vous invite à découvrir et explorer l’inconnu, proche ou lointain. 
                            </div>
                            {{-- <button class="welcome-btn">En savoir +</button> --}}
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="welcome-box">
                            <div class="welcome-title">
                                Vous êtes ...
                                <div class="underline"></div>
                            </div>
                            <div class="row flex-wrap">
                                <div class="col-md-6">
                                    <div class="item-box">
                                        <div class="item-image">
                                            <img src="{{asset('images/welcome-avatar.svg')}}">
                                        </div>
                                        <div class="item-title">
                                            Professionnel du tourisme
                                            <div class="underline green"></div>
                                        </div>
                                        <div class="item-description">
                                            Être passionné par son activité ne veut pas forcément dire que l’on sait communiquer pour la faire connaître. On compte souvent sur le bouche-à-oreille… mais lui aussi a changé de canal, et a besoin d’un savoir-faire spécifique. Rejoindre la plateforme MOOVTOO, c’est profiter de notre expertise en communication globale, et assurer non seulement votre visibilité, mais aussi la mise en rapport avec un public ciblé : le vôtre. Celui qui cherche l’expérience que vous proposez, sans parvenir à la trouver.
                                        </div>
                                        {{-- <div class="item-link">
                                            <a href="javascript:void(0)">Plust d'infos</a>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="item-box">
                                        <div class="item-image">
                                            <img src="{{asset('images/welcome-guide.svg')}}">
                                        </div>
                                        <div class="item-title">
                                            Influenceur ou blogueur dans l’univers du tourisme
                                            <div class="underline orange"></div>
                                        </div>
                                        <div class="item-description">
                                            Vous partagez vos expériences touristiques sur votre blog, votre chaine YouTube ou encore votre compte Instagram… Pourquoi ne pas écrire sur une plateforme globale qui vous donnerait la parole, dans un esprit de liberté bienveillante, pour partager vos aventures avec une audience nouvelle ? Sur MOOVTOO, des marques sont heureuses de vous proposer de belles expériences à vivre et partager avec nos abonnés… qui deviendront peut-être les vôtres aussi.   
                                        </div>
                                        {{-- <div class="item-link">
                                            <a href="javascript:void(0)">Plust d'infos</a>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="item-box">
                                        <div class="item-title">
                                            Passionné de voyages
                                            <div class="underline orange"></div>
                                        </div>
                                        <div class="item-description">
                                            Se balader, découvrir, explorer, ici ou ailleurs, seul ou à plusieurs… Vous cherchez de nouvelles idées, vous refusez le déjà-vu, vous avez besoin qu’on vous surprenne, et de vivre des expériences dans un esprit de spontanéité. Faites confiance à MOOVTOO, il vous guide là où vous ne seriez peut-être pas allé… mais vous n’allez pas le regretter.
                                        </div>
                                        {{-- <div class="item-link">
                                            <a href="javascript:void(0)">Plust d'infos</a>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="item-box">
                                        <div class="item-image">
                                            <img src="{{asset('images/welcome-passport.svg')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-md-12 padding-xs-0">
                        <div class="welcome-box">
                            <div class="welcome-title">
                                <strong>Curieux?</strong><br>
                                Contactez-nous...
                                <div class="underline"></div>
                            </div>
                            <div class="welcome-small-title">
                                et on revient vers vous
                            </div>
                            <div class="contact-box">
                                <div id="contactMessage"></div>
                                <div class="contact-form">
                                    <input type="text" placeholder="Prénom, Nom" id="name" name="name">
                                    <span id="help-name" class="help-block"></span>
                                </div>
                                <div class="contact-form">
                                    <input type="email" placeholder="Email" id="email" name="email">
                                    <span id="help-email" class="help-block"></span>
                                </div>
                                <div class="contact-form">
                                    <select name="you_are" id="you_are" class="select2">
                                        <option value="">Vous êtes?</option>
                                        <option value="Professionnel du tourisme">Professionnel du tourisme</option>
                                        <option value="Influencer ou Blogger">Influencer ou Blogger</option>
                                        <option value="Passionné de voyages">Passionné de voyages</option>
                                        <option value="Autre">Autre</option>
                                    </select>
                                    <span id="help-you_are" class="help-block"></span>
                                </div>
                                <div class="contact-form">
                                    <textarea name="message" id="message" placeholder="VOTRE MESSAGE"></textarea>
                                    <span id="help-message" class="help-block"></span>
                                </div>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="policy" name="policy" required>
                                    J’accepte les conditions d’utilisation *
                                </label>
                                <div>
                                    <div class="g-recaptcha" data-sitekey="6LcXrJIUAAAAAGi-NJDegDCl184_JXktBUKgRVMB
                                    "></div>
                                        <span class="help-block help-crecapcha"></span>
                                    <button class="contact-btn" onclick="contactUs()">Envoyer</button>
                                </div>
                                <div class="contact-policy-box">
                                    <p>*Les données à caractère personnel recueillies font l’objet de traitements dont le responsable est NELCOM Group, conformément à la réglementation relative à la protection des données à caractère personnel.</p>
                                    <p>Elles sont traitées pour l’analyse de vos besoins, pour évaluer la pertinence des communications que nous pourrions vous envoyer. Ces données sont conservées pendant une durée de 2 ans.</p>
                                    <p>Vous disposez d’un droit d’accès, de rectification, d'effacement, d’opposition et de limitation du traitement. Vous pouvez aussi donner des instructions relatives à la conservation, à l’effacement et à la communication de vos données après votre décès. Pour exercer ces droits sur vos données, envoyez-nous un message à contact@moovtoo.com en précisant « Droit à l’oubli » dans l’objet du message.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        

    <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    {{-- <script src="https://www.google.com/recaptcha/api.js?render=6Lc2q5IUAAAAANTG2Wse-_Nd-JGHSJ7dgH82LtO0"></script> --}}
    <script>
        // grecaptcha.ready(function() {
        //     grecaptcha.execute('6Lc2q5IUAAAAANTG2Wse-_Nd-JGHSJ7dgH82LtO0', {action: 'homepage'});
        // });
    </script>

    <script>
        $(function(){
            $('.select2').select2({
                width:'100%'
            });
        })

        function contactUs(){
            var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);

            var email =  $('#email').val(),
                name = $('#name').val(),
                you_are = $('#you_are').val(),
                message = $('#message').val(),
                cap = $('#g-recaptcha-response').val();


            var error = false;

            if(name == ''){
                $('#help-name').html('Champ Requis');
                error = true;
            }else{
                $('#help-name').html('');
            }if(cap == ''){
            $('.help-crecapcha').html('Champ Requis');
                error = true;
            }else{
                $('.help-crecapcha').html('');
            }

            if(email == ''){
                $('#help-email').html('Champ Requis');
                error = true;
            }else if(!pattern.test(email)){
                $('#help-email').html('Enter a valid email');
                error = true;
            }else{
                $('#help-email').html('');
            }

            if(you_are == ''){
                $('#help-you_are').html('Champ Requis');
                error = true;
            }else{
                $('#help-you_are').html('');
            }
            if(message == ''){
                $('#help-message').html('Champ Requis');
                error = true;
            }else{
                $('#help-message').html('');
            }
            if(!$('#policy').prop('checked')){
                $('.checkbox-inline').css('color', 'red');
                error = true;
            }else{
                $('.checkbox-inline').css('color', 'black');
            }

            if(!error){
                $.ajax({
                    type: 'POST',
                    url: '{{route('contact.post')}}',
                    data: {
                        _token : "{{ csrf_token() }}",
                        email : email,
                        name : name,
                        you_are: you_are,
                        message : message,
                    },
                    dataType:'json',

                }).done(function (result) {
                    // if(result.contactMessage != null){
                        console.log(result)
                        console.log(result.contactMessage)
                        $('#contactMessage').html(result.contactMessage);
                        $('#contactMessage').show(400);
                        setTimeout(function(){
                            $('#contactMessage').html('');
                            $('#contactMessage').hide(400);
                        },5000);
                    // }
                    $('#email').val('');
                    $('#name').val('');
                    $('#you_are').val('');
                    $('#message').val('');
                    $('#policy').prop('checked', false);

                }).fail(function (data) {
                    $('#contactMessage').html("Something went wrong!");
                    $('#contactMessage').show(400);
                        setTimeout(function(){
                            $('#contactMessage').html('');
                            $('#contactMessage').hide(400);
                        },2000);
                });
            }

        }
    </script>
    </body>
</html>
