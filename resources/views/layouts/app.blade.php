<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') {{ config('app.name', 'Laravel') }}</title>

    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.favicon', '')) }}"> --}}
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("images/fav.png") }}">

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.favicon', '')) }}"> --}}
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("images/fav.png") }}">
    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <!-- Our Custom CSS -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">

    
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/ajax-views.js') }}"></script>
    <script src="{{ asset('js/cms_scripts.js') }}" type="application/javascript"></script>

    @stack('css')
</head>
<body>

<div id="loader" class="lds-flickr-box"><div class="lds-flickr"> <div></div><div></div><div></div></div> </div>

    @include('layouts.partials.header')
    @yield('content')
    <section id="ajax-footer">

    </section>
    {{-- @include('layouts.partials.footer') --}}

    <script type="text/javascript">

        var ajaxCounter = -1;
        function callAjax(view, id, params =null){
            if ( ajaxCounter == -1 ){
                ajaxCounter =1 ;
            }else
                ajaxCounter++;
            if(params != null){
                url = '{{route("ajax.view")}}/'+view+params;
            }
            else{
                url = '{{route("ajax.view")}}/'+view;
            }
            $.ajax({
                type: "GET",
                url: url,
                success: function(data)
                {
                    ajaxCounter--;
                    $('#'+id).html(data);
                    if ( ajaxCounter == 0 ){
                        setTimeout(function () {
                            if ( ajaxCounter == 0)
                                hideLoader();
                        },2000);

                    }
                },
                error:function( data){
                    ajaxCounter--;
                    if ( ajaxCounter == 0 ){
                        setTimeout(function () {
                            if ( ajaxCounter == 0)
                                hideLoader();
                        },2000);

                    }
                    // console.log(data);
                },done : function (data) {

                }
            });
        }

        function hideLoader() {
            document.getElementById("loader").style.display = "none";
        }


        $(function(){

            callAjax('footer', 'ajax-footer');

            $.fn.visible = function(partial) {

                var $t            = $(this),
                    $w            = $(window),
                    viewTop       = $w.scrollTop(),
                    viewBottom    = viewTop + $w.height(),
                    _top          = $t.offset().top,
                    _bottom       = _top + $t.height(),
                    compareTop    = partial === true ? _bottom : _top,
                    compareBottom = partial === true ? _top : _bottom;

                return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

            };
        })


        $(window).on("scroll", function() {
            if($(window).scrollTop() > 50) {
                $("header").addClass("active");
            } else {
                $("header").removeClass("active");
            }
        });

        if ($(".home-nav-body")[0]) {
            var stickyHeaderTop = $('.home-nav-body').offset().top;

            $(window).scroll(function () {
                if ($(document).scrollTop() > stickyHeaderTop - 50) {
                    $(".home-nav-body").addClass("fixed");
                    // $('body').css('padding-top', '104px');
                } else {
                    $(".home-nav-body").removeClass("fixed");
                    // $('body').css('padding-top', '50px');
                }
            });
        }


        $('.filter-btn-open').on('click', function () {
            target = $(this).attr('data-target');
            if($('#'+target).css('display') == "none"){
                $('#'+target).slideDown(400);
                $('#'+target).css('display', 'flex');
            }else{
                $('#'+target).slideUp(400);
            }
        })

        $('.nav-dropdown').on('click', function(){
            target = $(this).attr('data-target');
            if($('#'+target).css('display') == "none"){
                $('#'+target).slideDown(500);
                $(this).find('.dropdown-icon').find('i').removeClass('fa-angle-down');
                $(this).find('.dropdown-icon').find('i').addClass('fa-angle-up');
            }else{
                $('#'+target).slideUp(500);
                $(this).find('.dropdown-icon').find('i').removeClass('fa-angle-up');
                $(this).find('.dropdown-icon').find('i').addClass('fa-angle-down');
            }
        })
        
        $('.trip-dropdown-head').on('click', function(){
            if($(this).next('ul').css('display') == "none"){
                $(this).next('ul').slideDown(500);
                $(this).find('.trip-dropdown-icon').find('i').removeClass('fa-angle-down');
                $(this).find('.trip-dropdown-icon').find('i').addClass('fa-angle-up');
            }else{
                $(this).next('ul').slideUp(500);
                $(this).find('.trip-dropdown-icon').find('i').addClass('fa-angle-down');
                $(this).find('.trip-dropdown-icon').find('i').removeClass('fa-angle-up');
            }
        })

        $(function(){
            $('.nav-dropdown').each(function(key, value){
                target = $(value).attr('data-target');
                active = $('#'+target).find('.active').html();
                $("[data-target='"+target+"']").find('#nav-title').html(active);
            })
        })

        $(window).scroll(function(event) {

            $(".animation-x").each(function(i, el) {
                var el = $(el);
                if (el.visible(true)) {
                    el.addClass("come-in");
                }
            });
            $(".animation-x-inv").each(function(i, el) {
                var el = $(el);
                if (el.visible(true)) {
                    el.addClass("come-in-inv");
                }
            });
            $(".animation-y").each(function(i, el) {
                var el = $(el);
                if (el.visible(true)) {
                    el.addClass("come-in-y");
                }
            });

        });

        $(document).ready(function () {
            $('.select2').select2({
                width:'100%'
            });

            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, .overlay').on('click', function () {
                $('#sidebar').removeClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });

        $('.slider').slick({
            dots: true,
            infinite: true,
            speed: 1000,
            autoplay: true,
            autoplaySpeed: 3000,
            prevArrow: false,
            nextArrow: false,
            // cssEase: 'fade-out',
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
        $('.some-fun-slider').slick({
            autoplay: true,
            autoplaySpeed: 8000,
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 910,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ],
        });
        $('.guide-attractions-slider').slick({
            autoplay: true,
            autoplaySpeed: 8000,
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 910,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ],
        });
        $('.trip-slider').slick({
            dots: true,
            infinite: true,
            speed: 1000,
            autoplay: true,
            autoplaySpeed: 3000,
            prevArrow: false,
            nextArrow: false,
            // cssEase: 'fade-out',
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
        $('.disconnect-slider').slick({
            dots: false,
            infinite: true,
            speed: 1000,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                    }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                    }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ],
            nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><img src="{{asset('images/slider-right.png')}}"></button>',
            prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button" style=""><img src="{{asset('images/slider-left.png')}}"></button>',
        });
        $('.travel-tips-slider').slick({
            autoplay: true,
            autoplaySpeed: 8000,
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                // {
                // breakpoint: 1024,
                //     settings: {
                //         slidesToShow: 3,
                //         slidesToScroll: 3,
                //         }
                //     },
                {
                    breakpoint: 910,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                        }
                    },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                        }
                    }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ],
        });
    </script>
    @stack('js')
</body>
</html>
