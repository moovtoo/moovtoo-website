<div class="suggestion-title">
    {{alias('MOOVTOO GUIDE')}}
</div>
@include('layouts.partials.title', ['title'=>'What are you interested in?'])
<div class="grey-container">
    <div class="container">
        <div class="row home-box">
            <div class="col-md-12">
                <div class="row vertical-middle">
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/accomodation.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/events.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/foodDrinks.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/religiousTourism.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/poolBeaches.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/transportTravel.svg')}}">
                        </div>
                    </div>
                </div>
                <div class="row vertical-middle">
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/accomodation.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/events.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/foodDrinks.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/religiousTourism.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/poolBeaches.svg')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 guide-box">
                        <div class="guide-div animation-x">
                            <img src="{{asset('images/guide/transportTravel.svg')}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>