<div class="container full-xs-container">
    @include('layouts.partials.title', ['title'=>'Latest From'])
    <div class="row home-box">
        <div class="col-md-6">
            <div class="post-box animation-y">
                @if (isset($latestBlog) and $latestBlog != null)
                    <div class="post-box-img">
                        <a href="{{route('blog.article.detail', ['slug'=>$latestBlog->slug, 'category'=>$latestBlog->categories[0]->slug])}}">
                        @if(isset ($latestBlog->photo))
                            <img src="http://cms.moovtoo.com/public/{{$latestBlog->photo}}" class="post-box-img-main img-responsive">
                        @else
                            <img src="{{asset('images/blog.png')}}" class="post-box-img-main img-responsive">
                        @endif
                        <img src="{{asset('images/horizontal-overlay.png')}}" class="post-box-img-overlay">
                        </a>
                        <p>{{alias('The Blog')}}</p>
                    </div>
                    <div class="post-box-details">
                        <div class="post-box-date"><strong>{{alias('DILETTANTES')}}: </strong>{{$latestBlog->date_created}}</div>
                        <div class="post-box-title">
                            <a href="{{route('blog.article.detail', ['slug'=>$latestBlog->slug, 'category'=>$latestBlog->categories[0]->slug])}}">
                                {{$latestBlog->name}}
                            </a>
                        </div>
                        <a href="javascript:void(0)" class="post-box-link">{{alias('View all articles')}}</a>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="post-box animation-y">
                @if (isset($latestMag) and $latestMag != null)
                    <div class="post-box-img">
                        <a href="{{route('mag.article.detail', ['slug'=>$latestMag->slug, 'category'=>$latestMag->categories[0]->slug])}}">
                        @if(isset ($latestMag->photo))
                            <img src="http://cms.moovtoo.com/public/{{$latestMag->photo}}" class="post-box-img-main img-responsive">
                        @else
                            <img src="{{asset('images/mag.png')}}" class="post-box-img-main img-responsive">
                        @endif
                        <img src="{{asset('images/horizontal-overlay.png')}}" class="post-box-img-overlay">                            </a>
                        <p>{{alias('The Mag')}}</p>
                    </div>
                    <div class="post-box-details">
                        <div class="post-box-date"><strong>{{alias('DILETTANTES')}}: </strong>{{$latestMag->date_created}}</div>
                        <div class="post-box-title" >
                            <a href="{{route('mag.article.detail', ['slug'=>$latestMag->slug, 'category'=>$latestMag->categories[0]->slug])}}">{{$latestMag->name}}
                            </a>
                        </div>
                        <a href="{{route('mag.category')}}" class="post-box-link">{{alias('View all articles')}}</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>