<div class="row " style="background-color: #313a45" >
    <div class="col-md-10 offset-2">
        <div class="filters-div ">
            <div class="space-filters">
                <div class="fiter-title">
                    BURGERS IN PARIS
                </div>
            </div>
           <div class="space-filters">
               <div class="currency ">
                   Average Price
                   <a class="euro">$</a>
                   <a class="euro">$$</a>
                   <a class="euro">$$$</a>
                   <a class="euro">$$$$</a>
               </div>
           </div>
          <div class="space-filters">
              <div class="filter-btn ">
                  <a class="filter-btns active">Open Now</a>
                  <a class="filter-btns">Open Now</a>
                  <a class="filter-btns">Open Now</a>
              </div>
          </div>
            <div class="space-filters">
                <div class="all-filters">
                  <button class="filter-btn-open" data-target="filter-list"><img src="{{asset('images/vector.png')}}">Filters </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row filters-grey" id="filter-list">
    <div class="container filters-container">
        <ul class="sub-filters">
            <li><label class="sub-filter-title">Filter By</label></li>
            <li>Top Rated</li>
            <li>Most Viewed</li>
            <li>Recommended</li>
        </ul>
        <ul class="sub-filters">
            <li><label class="sub-filter-title">Features</label></li>
            <li><input type="checkbox" > Delivery</li>
            <li><input type="checkbox" > Open Now</li>
            <li><input type="checkbox" > Takes Reservation</li>
            <li><input type="checkbox" > Kids Friendly</li>
        </ul>
        <ul class="sub-filters sub-filters-top">
            <li><input type="checkbox" > Delivery</li>
            <li><input type="checkbox" > Open Now</li>
            <li><input type="checkbox" > Takes Reservation</li>
            <li><label class="sub-filter-read-more">More options</label></li>
        </ul>
        <ul class="sub-filters">
            <li><label class="sub-filter-title">Cuisine </label></li>
            <li><input type="checkbox" > Delivery</li>
            <li><input type="checkbox" > Open Now</li>
            <li><input type="checkbox" > Takes Reservation</li>
            <li><input type="checkbox" > Kids Friendly</li>
        </ul>
        <ul class="sub-filters sub-filters-top ">
            <li><input type="checkbox" > Open Now</li>
            <li><input type="checkbox" > Takes Reservation</li>
            <li><input type="checkbox" > Kids Friendly</li>
            <li><label class="sub-filter-read-more">More options</label></li>
        </ul>
        <ul class="sub-filters">
            <li><label class="sub-filter-title">Ambaince </label></li>
            <li><input type="checkbox" > Delivery</li>
            <li><input type="checkbox" > Open Now</li>
            <li><input type="checkbox" > Takes Reservation</li>
            <li><input type="checkbox" > Kids Friendly</li>
            <li><label class="sub-filter-read-more">More options</label></li>
        </ul>
        <ul class="sub-filters">
            <li><label class="sub-filter-title">Dress code </label></li>
            <li><input type="checkbox" > Delivery</li>
            <li><input type="checkbox" > Open Now</li>
            <li><input type="checkbox" > Takes Reservation</li>
            <li><input type="checkbox" > Kids Friendly</li>
            <li><label class="sub-filter-read-more" >More options</label></li>
        </ul>
    </div>
</div>