<div class="home-slider">
    <div class="trip-banner-days">
        <img src="{{asset('images/designer-banner.png')}}">
    </div>
    <div class="slider-search-box">
        <div class="day-slider-mid-trip">
            <div class="slider-change-cover">
                <img class="cover-camera-img" src="{{asset('images/cover-camera.png')}}">
                <span>{{alias('Change Cover')}}</span>
            </div>
            <div class="user-profile">
                <img class="day-user-profile-img" src="{{asset('images/user.png')}}">
            </div>
            <div class="banner-suggestion-title">
                Hello John
            </div>
            <div class="slider-title-day">Your Statistics so far </div>

            <div class="day-banner-info">
                <div class="day-banner-detail">25 Trips</div>
                <div class="day-banner-detail">3 returning customers</div>
                <div class="day-banner-detail">10 Reviews</div>
                <div class="day-banner-detail">5 Deals</div>
            </div>
        </div>
    </div>
</div>
