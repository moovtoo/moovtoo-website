<form id="main-form" enctype="multipart/form-data">
<div class="row">
    <div class="col-md-12 padding-0 ">
        <div class="trip-nav">
            <div class="trip-nav-box">
                <div class="arrow-up-white"></div>
                <div class="row">
                    <div class="col-md-12 display-day-nav white-nav">
                        <div class="col-md-6 offset-3">
                            <div class="col-md-4">
                                <div class="designer-tab-title " id="tab-1" onclick="openTab(1)">
                                    New requests
                                    <div class="underline-orange-app"></div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="designer-tab-title" id="tab-2" onclick="openTab(2)">
                                    Ongoing requests <div class="notification-number">4</div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="designer-tab-title" id="tab-3" onclick="openTab(3)">
                                    Past requests <div class="notification-number">3</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div id="1" class="tab-content" style="display:block;">
        <div class="row">
            <div class="col-md-12">
                <div class="request-table">
                    <table>
                        <tr>
                            <th>Client name</th>
                            <th>Destination</th>
                            <th>Dates</th>
                            <td>Theme</td>
                            <td>Type</td>
                            <td>Budget/dat</td>
                            <td class="table-search-row"><img class="table-search-img" src="{{asset('images/search.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="2" class="tab-content" >
        <div class="row">
            <div class="col-md-12">
                <div class="request-table">
                    <table>
                        <tr>
                            <th>Client name</th>
                            <th>Destination</th>
                            <th>Dates</th>
                            <td>Theme</td>
                            <td>Type</td>
                            <td>Budget/dat</td>
                            <td>Status</td>
                            <td class="table-search-row"><img class="table-search-img" src="{{asset('images/search.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td >
                                <div class="select-request-table">
                                    <select type="text" class="form-control select2 " >
                                        <option selected >Assign trip to</option>
                                        <option>John Doe</option>
                                        <option>John Doe</option>
                                    </select>
                                </div>
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>

                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td >
                                <div class="select-request-table">
                                    <select type="text" class="form-control select2 " >
                                        <option selected >Assign trip to</option>
                                        <option>John Doe</option>
                                        <option>John Doe</option>
                                    </select>
                                </div>
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>

                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td >
                                <div class="select-request-table">
                                    <select type="text" class="form-control select2 " >
                                        <option selected >Assign trip to</option>
                                        <option>John Doe</option>
                                        <option>John Doe</option>
                                    </select>
                                </div>
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>

                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td >
                                <div class="select-request-table">
                                    <select type="text" class="form-control select2 " >
                                        <option selected >Assign trip to</option>
                                        <option>John Doe</option>
                                        <option>John Doe</option>
                                    </select>
                                </div>
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>

                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td >
                                <div class="select-request-table">
                                    <select type="text" class="form-control select2 " >
                                        <option selected >Assign trip to</option>
                                        <option>John Doe</option>
                                        <option>John Doe</option>
                                    </select>
                                </div>
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>

                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td>80Euro</td>
                            <td >
                                <div class="select-request-table">
                                    <select type="text" class="form-control select2 " >
                                        <option selected >Assign trip to</option>
                                        <option>John Doe</option>
                                        <option>John Doe</option>
                                    </select>
                                </div>
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="3" class="tab-content" >
        <div class="row">
            <div class="col-md-12">
                <div class="request-table">
                    <table>
                        <tr>
                            <th>Client name</th>
                            <th>Destination</th>
                            <th>Dates</th>
                            <td>Theme</td>
                            <td>Type</td>
                            <td>Budget/dat</td>
                            <td class="table-search-row"><img class="table-search-img" src="{{asset('images/search.png')}}"></td>

                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td class="table-select-show">
                                80Euro<img class="table-delete-img" src="{{asset('images/request-table-delete.png')}}">
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td class="table-select-show">
                                80Euro<img class="table-delete-img" src="{{asset('images/request-table-delete.png')}}">
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td class="table-select-show">
                                80Euro<img class="table-delete-img" src="{{asset('images/request-table-delete.png')}}">
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td class="table-select-show">
                                80Euro<img class="table-delete-img" src="{{asset('images/request-table-delete.png')}}">
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td class="table-select-show">
                                80Euro<img class="table-delete-img" src="{{asset('images/request-table-delete.png')}}">
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                        <tr>
                            <td>Jonathan Doe</td>
                            <td>Paris</td>
                            <td>Sun 14- Wed 18</td>
                            <td>Adventure</td>
                            <td>Solo</td>
                            <td class="table-select-show">
                                80Euro<img class="table-delete-img" src="{{asset('images/request-table-delete.png')}}">
                            </td>
                            <td class="table-search-row"><img class="table-show-img" src="{{asset('images/table-show.png')}}"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

</form>

@push('js')
    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    @endpush



