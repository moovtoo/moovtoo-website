<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer-section">
                    <div class="footer-title">
                        Stories
                    </div>
                    @if (isset($storyCategories))
                        @foreach ($storyCategories as $category)
                            <div class="footer-link">
                                <a href="{{route('mag.category', $category->slug)}}">{{$category->name}}</a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer-section">
                    <div class="footer-title">
                        Guide
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Destination</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Eco Tourism</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Nightlife</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Outdoor</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer-section">
                    <div class="footer-title">
                        Community
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Destination</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Eco Tourism</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Nightlife</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Outdoor</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer-section">
                    <div class="footer-title">
                        Blog
                    </div>
                    @if (isset($blogCategories))
                        @foreach ($blogCategories as $category)
                            <div class="footer-link">
                                <a href="{{route('blog.category', $category->slug)}}">{{$category->name}}</a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer-section">
                    <div class="footer-title">
                        Multimedia
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Destination</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Eco Tourism</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Nightlife</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Outdoor</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer-section">
                    <div class="footer-title">
                        View
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Destination</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Eco Tourism</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Nightlife</a>
                    </div>
                    <div class="footer-link">
                        <a href="javascript:void(0)">Outdoor</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="footer-section">
                    <div class="footer-title">
                        Download the app
                    </div>
                    <div class="footer-download">
                        <a href="javascript:void(0)">
                            <img src="{{asset('images/apple-store.svg')}}">
                        </a>
                        <a href="javascript:void(0)">
                            <img src="{{asset('images/play-store.svg')}}">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="footer-section">
                    <div class="footer-title">
                        Community
                    </div>
                    <div class="footer-form">
                        <input type="text">
                        <input type="text">
                        <button>Sign up</button>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="footer-nav">
                <img src="{{asset('moovtoo/logo.png')}}">
                <div class="footer-nav-links">
                    <div class="footer-nav-link"><a href="javascript:void(0)">About Us</a></div>
                    <div class="footer-nav-link"><a href="javascript:void(0)">Work for Us</a></div>
                    <div class="footer-nav-link"><a href="javascript:void(0)">Contact Us</a></div>
                    <div class="footer-nav-link"><a href="javascript:void(0)">Press, trade and advertising</a></div>
                    <div class="footer-nav-link"><a href="javascript:void(0)">Privacy policy</a></div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="footer-social-links">
                <div class="footer-social-link">
                    <a href="javascript:void(0)">
                        <img src="{{asset('images/icons/socials/fb.svg')}}">
                    </a>
                </div>
                <div class="footer-social-link">
                    <a href="javascript:void(0)">
                        <img src="{{asset('images/icons/socials/gp.svg')}}">
                    </a>
                </div>
                <div class="footer-social-link">
                    <a href="javascript:void(0)">
                        <img src="{{asset('images/icons/socials/inst.svg')}}">
                    </a>
                </div>
                <div class="footer-social-link">
                    <a href="javascript:void(0)">
                        <img src="{{asset('images/icons/socials/twit.svg')}}">
                    </a>
                </div>
                <div class="footer-social-link">
                    <a href="javascript:void(0)">
                        <img src="{{asset('images/icons/socials/linkedin.svg')}}">
                    </a>
                </div>
                <div class="footer-social-link">
                    <a href="javascript:void(0)">
                        <img src="{{asset('images/icons/socials/yt.svg')}}">
                    </a>
                </div>
                <div class="copyright">
                    &copy; 2019 <a href="javascript:void(0)">moovtoo.com</a>
                </div>
            </div>
        </div>
    </div>
</footer>