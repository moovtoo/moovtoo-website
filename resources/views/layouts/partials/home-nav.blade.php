 <div class="row">
        <div class="col-md-12 padding-0">
            <div class="arrow-up-white"></div>
            <div class="home-nav">
                @foreach( $domains as $domain)
                {{-- <span><a href="{{"http://".$domain->name.".".$domain->parent->name}}@else{{"http://".$domain->name}}@endif">{{$domain->ref}}</a> </span> --}}
                    @if( $domain->type == 'sub' and $domain->is_published)
                        <span><a href="{{"http://".$domain->name.".".$domain->parent->name}}">{{$domain->ref}}</a> </span>
                    @endif
                @endforeach

            </div>
        </div>
 </div>