<div class="container full-xs-container">
    @include('layouts.partials.title', ['title'=>'Concierge Services'])
    <div class="row home-box">
        <div class="col-md-8">
            <img src="{{asset('images/car.jpg')}}" class="img-responsive img-concierge">
        </div>
        <div class="col-md-4">
            <div class="concierge-side">
                <div class="middle">
                    <p>Airport Vip Taxi</p>
                    <button class="btn rectangle-btn white">Book Now</button>
                </div>
            </div>
        </div>
        <div class="side-btns">
            <div class="side-btn blue">
                <img src="{{asset('images/phone.png')}}" class="img-responsive">
            </div>
            <div class="side-btn grey">
                <img src="{{asset('images/euro.png')}}" class="img-responsive">
            </div>
        </div>
    </div>
</div>