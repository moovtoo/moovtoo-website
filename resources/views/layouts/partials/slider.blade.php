<div class="home-slider">
    <div class="main-slider">
        @foreach ($sliders->data as $slider)
            <img src="http://cms.moovtoo.com/public/{{$slider->photo}}">
        @endforeach
    </div>
    <div class="slider-search-box">
        <div class="slider-mid">
            <div class="slider-title">Where would you like to go ?</div>
        </div>
    </div>
    <div class="slider-overlay">
        <img src="{{asset('images/slider-overlay.png')}}">
    </div>
</div>
<script>
    $(function(){
        $('.main-slider').slick({
            dots: true,
            infinite: true,
            speed: 1000,
            autoplay: true,
            autoplaySpeed: 3000,
            prevArrow: false,
            nextArrow: false,
            slidesToShow: 1,
            fade: true,
            slidesToScroll: 1,
        });
    })
</script>