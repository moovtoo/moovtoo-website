<div class="home-slider">
    <div class="slider">
        @foreach ($banners as $banner)
            <img src="{{'http://cms.moovtoo.com/public/'.$banner->photo}}">
        @endforeach

    </div>
    <div class="slider-search-box">
        <div class="slider-mid">
            <div class="slider-title">{{alias('Create your')}} <span>{{alias('Customized Trip')}}</span></div>
                <div class="choose-destination-title">
                    <a class="choose-destination-btn" data-toggle="modal" data-target="#whereto" style="cursor: pointer">{{alias('Choose Your Destination')}}</a>
                </div>
                <div class="top-destinations">
                    {{alias('Top Destinations')}}
                </div>
                <div class="destinations-boxes">
                    @foreach( $travelLocations as $travelLocation)
                    @if($travelLocation->is_top)

                                <div class="destination-box">
                                    <form action="{{route('trip.create-trip')}}" method="post" id="top-location-btn">
                                        <input id="location" type="hidden" name="location" class="where-top-location" value="{{$travelLocation->name}}">
                                        <button class="top-location-btn" >
                                    <div class="destination-img">
                                        <img src="{{'http://cms.moovtoo.com/public/'.$travelLocation->icon}}" class="img-responsive">
                                    </div>
                                    <div class="destination-name">{{$travelLocation->name}}</div>
                                        </button>
                                        @csrf
                                    </form>
                                </div>

                    @endif
                    @endforeach
                        {{--<div class="destination-box">--}}
                            {{--<div class="destination-img"><img src="{{asset('images/paris-slider.png')}}" class="img-responsive"></div>--}}
                            {{--<div class="destination-name">Paris</div>--}}
                        {{--</div>--}}
                    {{--<div class="destination-box">--}}
                        {{--<div class="destination-img"><img src="{{asset('images/amsterdam-banner.png')}}" class="img-responsive"></div>--}}
                        {{--<div class="destination-name">Amsterdam</div>--}}
                    {{--</div>--}}
                    {{--<div class="destination-box">--}}
                        {{--<div class="destination-img"><img src="{{asset('images/london-banner.png')}}" class="img-responsive"></div>--}}
                        {{--<div class="destination-name">London</div>--}}
                    {{--</div>--}}
                    {{--<div class="destination-box">--}}
                        {{--<div class="destination-img"><img src="{{asset('images/prague-banner.png')}}" class="img-responsive"></div>--}}
                        {{--<div class="destination-name">Prague</div>--}}
                    {{--</div>--}}

                </div>

        </div>
    </div>
    <div class="slider-overlay">
        <img src="{{asset('images/slider-overlay.png')}}">
    </div>
</div>
<script>
    $('.slider').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: false,
        nextArrow: false,
        // cssEase: 'fade-out',
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });
</script>