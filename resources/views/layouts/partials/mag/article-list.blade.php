<div class="mag-container container full-xs-container">
    <div class="row home-box mag-box">
        <div class="col-md-9 padding-0">
            <div id="article-list">
                <div class="row">

                    @foreach ($articles->data as $article)
                        @if($loop->index != 0 and $loop->index % 2 == 0)
                            </div>
                            @if( $loop->index == 4 )
                                @include('layouts.partials.mag.guide-event-latest')
                                @include('layouts.partials.mag.video')
                            @endif
                            <div class="row">
                        @endif
                        @include('layouts.partials.mag.article-item', ['article' => $article])
                    @endforeach
                </div>
            </div>
            @if($articles->next_page_url != null)
                <div class="row">
                    <div class="col-md-12">
                        <div class="middle-btn">
                            <button class="white-btn btn-article-list-more" data-url="{{$articles->next_page_url}}">{{alias('plus d\'articles')}}</button>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-3">
            {{-- <div class="mag-list-sidebar outline">
                <div class="title-sidebar">Most read</div>
                <div class="mag-sidebar-list">
                    <div class="col-md-12">
                        @foreach ($articles->data->data as $art)
                            @if($loop->index < 3)
                                <div class="sidebar-article">
                                    <img src="http://cms.moovtoo.com/public/{{$art->photo}}">
                                    <p><a href="{{route('mag.article.detail', $art->slug)}}">{{$art->name}}</a></p>
                                </div>
                            @endif
                        @endforeach
                        <div class="middle-btn">
                            <button class="white-btn">Voir tous les articles</button>
                        </div>
                    </div>
                </div>
            </div> --}}
            @include('layouts.partials.side-advertisement', ['name'=>'test'])
        </div>
    </div>
</div>