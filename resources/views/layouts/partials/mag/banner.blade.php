<div class="home-slider mag-banner">
    <div class="row">
        <div class="col-md-4">
            <div class="mag-banner-left">
                <img src="{{asset('images/mag-banner-overlay.png')}}">
                <div class="mag-banner-info">
                    <div class="mag-banner-info-box">
                        <div class="mag-banner-title">{{alias('La Bourgogne')}}</div>
                        <div class="mag-banner-city">France</div>
                        <div class="mag-banner-description">La Bourgogne, or 157 East 72nd Street is an attractive prewar condominium built in 1923, known for its stunning roof terrace with panoramic city views. </div>
                        <button class="mag-banner-button">le Mini guide Bourgogne</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="mag-banner-right">
                @foreach ($banners as $banner)
                    <img src="{{'http://cms.moovtoo.com/public/'.$banner->photo}}">
                    @break
                @endforeach
            </div>
        </div>
    </div>
</div>