@include('layouts.partials.title', ['title'=>'A ne pas manquer'])
<div class="row home-box mag-box">
    <div class="col-md-6">
        <div class="latest-title">
            {{alias('Latest from the guide')}}
        </div>
        <div class="latest-item">
            <div class="latest-item-box">
                <img src="{{asset('images/black.png')}}">
                <p>Lebanon has 18 religious communities</p>
            </div>
            <div class="item-underline"></div>
        </div>
        <div class="latest-item">
            <div class="latest-item-box">
                <img src="{{asset('images/black.png')}}">
                <p>It has 40 daily newspapers</p>
            </div>
            <div class="item-underline"></div>
        </div>
        <div class="latest-item">
            <div class="latest-item-box">
                <img src="{{asset('images/black.png')}}">
                <p>It has 42 universities</p>
            </div>
            <div class="item-underline"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="latest-title">
            {{alias('Events not to be missed')}}
        </div>
        <div class="latest-item">
            <div class="latest-item-box">
                <img src="{{asset('images/black.png')}}">
                <p>Lebanon has 18 religious communities</p>
            </div>
            <div class="item-underline"></div>
        </div>
        <div class="latest-item">
            <div class="latest-item-box">
                <img src="{{asset('images/black.png')}}">
                <p>It has 40 daily newspapers</p>
            </div>
            <div class="item-underline"></div>
        </div>
        <div class="latest-item">
            <div class="latest-item-box">
                <img src="{{asset('images/black.png')}}">
                <p>It has 42 universities</p>
            </div>
            <div class="item-underline"></div>
        </div>
    </div>
</div>