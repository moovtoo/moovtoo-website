@if(isset($article))
    <div class="col-md-6">
        <div class="post-box">
            <div class="post-box-img">
                <a href="{{route( session('moovtoo.domain','') .'.article.detail', ['slug'=>$article->slug, 'category'=>$article->categories[0]->slug])}}">
                    <img src="http://cms.moovtoo.com/public/{{$article->photo}}" class="post-box-img-main img-responsive">
                    <img src="{{asset('images/horizontal-overlay.png')}}" class="post-box-img-overlay">
                    {{-- <p>{{$article->photo_caption}}</p> --}}
                </a>

            </div>
            <div class="post-box-details">
                <div class="post-box-date">
                    <strong>
                        @foreach ($article->categories as $category)
                            @if($category->parent_id != null)
                                {{$category->name}}
                            @endif
                        @endforeach
                </strong>{{$article->date_created}}</div>
                <div class="post-box-title"><a href="{{route(session('moovtoo.domain','') .'.article.detail', ['slug'=>$article->slug, 'category'=>$article->categories[0]->slug])}}">{{$article->name}}</a></div>
            </div>
        </div>
    </div>
@endif