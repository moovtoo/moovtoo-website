<div class="container">
    <div class="row home-box">
        <div class="col-md-6">
            <div class="latest-title">
                Latest From the guide <a href="javascript:void(0)">{{alias('View All')}}</a>
                <div class="underline"></div>
            </div>
            <div class="latest-box">
                <div class="latest-guide-box grey">
                    <div class="guide-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="guide-box-details">
                        <div class="guide-box-date"><strong>{{alias('DILETTANTES')}}: </strong>December 13, 2019, 08h12</div>
                        <div class="guide-box-title">The upside and the downside of living in beirut during the holidays</div>
                    </div>
                </div>
                <div class="latest-guide-box grey">
                    <div class="guide-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="guide-box-details">
                        <div class="guide-box-date"><strong>{{alias('DILETTANTES')}}: </strong>December 13, 2019, 08h12</div>
                        <div class="guide-box-title">The upside and the downside of living in beirut during the holidays</div>
                    </div>
                </div>
                <div class="latest-guide-box grey">
                    <div class="guide-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="guide-box-details">
                        <div class="guide-box-date"><strong>{{alias('DILETTANTES')}}: </strong>December 13, 2019, 08h12</div>
                        <div class="guide-box-title">The upside and the downside of living in beirut during the holidays</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="latest-title">
                Latest Events <a href="javascript:void(0)">{{alias('View All')}}</a>
                <div class="underline"></div>
            </div>
            <div class="latest-box">
                <div class="agenda-box grey">
                    <div class="agenda-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="agenda-box-details">
                        <div class="agenda-box-title">{{alias('Where\'s the music')}}?</div>
                        <div class="agenda-box-time"><img src="{{asset('images/time.png')}}">12:00 AM - 11:00 PM</div>
                        <div class="agenda-box-location"><img src="{{asset('images/location.png')}}">Forum De Beirut</div>
                    </div>
                </div>
                <div class="agenda-box grey">
                    <div class="agenda-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="agenda-box-details">
                        <div class="agenda-box-title">{{alias('Where\'s the music')}}?</div>
                        <div class="agenda-box-time"><img src="{{asset('images/time.png')}}">12:00 AM - 11:00 PM</div>
                        <div class="agenda-box-location"><img src="{{asset('images/location.png')}}">Forum De Beirut</div>
                    </div>
                </div>
                <div class="agenda-box grey">
                    <div class="agenda-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="agenda-box-details">
                        <div class="agenda-box-title">{{alias('Where\'s the music')}}?</div>
                        <div class="agenda-box-time"><img src="{{asset('images/time.png')}}">12:00 AM - 11:00 PM</div>
                        <div class="agenda-box-location"><img src="{{asset('images/location.png')}}">Forum De Beirut</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>