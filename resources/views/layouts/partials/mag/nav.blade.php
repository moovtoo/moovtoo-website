<div class="home-nav-body">
    <div class="row">
        <div class="col-md-12">
            <div class="home-nav hidden-xs">
                @foreach( $categories as $category )
                    <span><a href="{{route(session()->get('moovtoo.domain').'.category',['slug'=> $category->slug])}}"
                             class="categories-nav @if(str_contains(Request::path(), '/'.$category->slug)) active @endif" > {{$category->name}}</a> </span>
                @endforeach
            </div>
            <div class="home-nav-dropdown visible-xs">
                <div class="nav-dropdown" data-target="mag-nav">
                    <span id="nav-title">{{alias('Categories')}}</span>
                    <span class="dropdown-icon"><i class="fa fa-angle-down"></i></span>
                </div>
                <ul class="nav-dropdown-list" id="mag-nav">
                    @foreach( $categories as $category )
                        <li><a href="{{route(session()->get('moovtoo.domain').'.category',['slug'=> $category->slug])}}"
                                 class="categories-nav @if(str_contains(Request::path(), '/'.$category->slug)) active @endif" > {{$category->name}}</a> </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>