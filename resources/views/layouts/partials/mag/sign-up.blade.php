<div class="partial-signup">
    <div class="moovtoo-sign-up-title">
        Hello, not a MOOVTOO Member yet? Sign up to be part of our community!
    </div>
    <div class="sign-up-btns">
        <a class="btn-social btn-moovtoo" href="javascript:void(0)">
            Sign up with moovtoo
        </a>
        <a class="btn-social btn-facebook">
            <span class="fa fa-facebook"></span> Sign in with Facebook
        </a>
        <a class="btn-social btn-googld">
            <span class="fa fa-facebook"></span> Sign in with Google
        </a>
    </div>
</div>