<div class="shop-items">
    <div class="shop-title">
        E-shop
        <div class="underline"></div>
    </div>
    <div class="shop-item">
        <div class="shop-item-img">
            <img src="{{asset('images/item1.png')}}">
        </div>
        <div class="shop-item-details">
            <div class="shop-item-title">
                Formage de la Bourgogne
            </div>
            <div class="shop-item-price">30 Euros</div>
            <div class="shop-item-btns">
                <button class="blue-btn">Ajouter au panier</button>
                <button class="orange-btn left-15 top-sm-10">Visiter le E-shop</button>
            </div>
        </div>
    </div>
    <div class="shop-title">
        Culture
        <div class="underline"></div>
    </div>
    <div class="shop-item">
        <div class="shop-item-img">
            <img src="{{asset('images/item2.png')}}">
        </div>
        <div class="shop-item-details">
            <div class="shop-item-title">
                Formage de la Bourgogne
            </div>
            <div class="shop-item-price">30 Euros</div>
            <div class="shop-item-btns">
                <button class="blue-btn">Ajouter au panier</button>
                <button class="orange-btn left-15 top-sm-10">Visiter le E-shop</button>
            </div>
        </div>
    </div>
    <div class="shop-title">
        Experiences
        <div class="underline"></div>
    </div>
    <div class="shop-item">
        <div class="shop-item-img">
            <img src="{{asset('images/item2.png')}}">
        </div>
        <div class="shop-item-details">
            <div class="shop-item-title">
                Formage de la Bourgogne
            </div>
            <div class="shop-item-price">30 Euros</div>
            <div class="shop-item-btns">
                <button class="blue-btn">Ajouter au panier</button>
                <button class="orange-btn left-15 top-sm-10">Visiter le E-shop</button>
            </div>
        </div>
    </div>
</div>