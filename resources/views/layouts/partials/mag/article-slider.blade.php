<div class="container">
    <div class="trip-margin-top"></div>
    @include('layouts.partials.title', ['title'=>'Dossier à la une'])
    <div class="row home-box">
        <div class="col-md-12">
            <div class="article-slider">
                @foreach ($articles as $article)
                    <div class="post-box">
                        <div class="post-box-img">
                            <a href="{{route('mag.article.detail', ['slug'=>$article->slug, 'category'=>$article->categories[0]->slug])}}">
                                <img src="http://cms.moovtoo.com/public/{{$article->photo}}" class="post-box-img-main img-responsive">
                                <img src="{{asset('images/horizontal-overlay.png')}}" class="post-box-img-overlay">
                                <p>{{$article->photo_caption}}</p>
                            </a>
                        </div>
                        <div class="post-box-details">
                            {{-- <div class="post-box-date"><strong>DILETTANTES: </strong>{{$article->date_created}}</div> --}}
                            <div class="post-box-title"><a href="{{route('mag.article.detail', ['slug'=>$article->slug, 'category'=>$article->categories[0]->slug])}}">{{$article->name}}</a></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.article-slider').slick({
            dots: false,
            infinite: true,
            speed: 1000,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                    }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                    }
                },
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ],
            nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><img src="{{asset('images/slider-right.png')}}"></button>',
            prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button" style=""><img src="{{asset('images/slider-left.png')}}"></button>',
        });
    })
</script>