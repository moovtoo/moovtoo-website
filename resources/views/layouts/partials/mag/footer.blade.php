<div class="mag-container container full-xs-container">
    @include('layouts.partials.title', ['title'=>'Moovtoo une platforme...'])
    <div class="home-box mag-box grey">
        <div class="row platform-box">
            <div class="col-md-3 col-sm-6">
                <div class="platform-item">
                    <img src="{{asset('images/platform4.png')}}">
                    <div class="platform-detail">
                        <div class="platform-title">{{alias('Disconnect')}}</div>
                        <div class="platform-description">Place your campaign quickly and easily. Set up within a few
                            minutes Place your campaign quickly and easily. </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="platform-item">
                    <img src="{{asset('images/platform3.png')}}">
                    <div class="platform-detail">
                        <div class="platform-title">{{alias('La Boutique')}}</div>
                        <div class="platform-description">Place your campaign quickly and easily. Set up within a few
                            minutes Place your campaign quickly and easily. </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="platform-item">
                    <img src="{{asset('images/platform2.png')}}">
                    <div class="platform-detail">
                        <div class="platform-title">{{alias('L\'Agenda')}}</div>
                        <div class="platform-description">Place your campaign quickly and easily. Set up within a few
                            minutes Place your campaign quickly and easily. </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="platform-item">
                    <img src="{{asset('images/platform1.png')}}">
                    <div class="platform-detail">
                        <div class="platform-title">{{alias('Guide')}}</div>
                        <div class="platform-description">Place your campaign quickly and easily. Set up within a few
                            minutes Place your campaign quickly and easily. </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row platform-box">
            <div class="col-md-12">
                <div class="sponsors-title">
                    <span class="bold">Pubs Sponsorises </span>par Moovtoo
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="sponsor-box">
                    <img src="{{asset('images/sponsor1.png')}}">
                    <div class="sponsor-detail">
                        <div class="sponsor-title">
                            Publicite
                        </div>
                        <div class="sponsor-description">
                            Place your campaign quickly and easily. Set up within a few minutes.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="sponsor-box">
                    <img src="{{asset('images/sponsor2.png')}}">
                    <div class="sponsor-detail">
                        <div class="sponsor-title">
                            Publicite
                        </div>
                        <div class="sponsor-description">
                            Place your campaign quickly and easily. Set up within a few minutes.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="sponsor-box">
                    <img src="{{asset('images/sponsor3.png')}}">
                    <div class="sponsor-detail">
                        <div class="sponsor-title">
                            Publicite
                        </div>
                        <div class="sponsor-description">
                            Place your campaign quickly and easily. Set up within a few minutes.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="sponsor-box">
                    <img src="{{asset('images/sponsor1.png')}}">
                    <div class="sponsor-detail">
                        <div class="sponsor-title">
                            Publicite
                        </div>
                        <div class="sponsor-description">
                            Place your campaign quickly and easily. Set up within a few minutes.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>