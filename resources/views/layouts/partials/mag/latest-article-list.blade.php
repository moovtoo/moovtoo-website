@include('layouts.partials.title', ['title'=>'Moovtoo une platform'])
<div class="mag-container container full-xs-container">
    <div class="row home-box mag-box">
        <div class="col-md-9">
            <div class="row">
                @foreach ($res1->data->data as $article)
                    @if($loop->index != 0 and $loop->index % 2 == 0)
                        </div>
                        <div class="row">
                    @endif
                    @include('layouts.partials.mag.article-item', ['article' => $article])
                @endforeach
            </div>
        </div>
        <div class="col-md-3">
            <div class="mag-sidebar margin-minus-15 hidden-xs">
                @include('layouts.partials.side-advertisement', ['name'=>'test'])
            </div>
        </div>
    </div>
</div>