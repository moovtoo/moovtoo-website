@include('layouts.partials.title', ['title'=>'The Agenda'])
<div class="grey-container">
    <div class="container">
        <div class="row home-box home-box-flex">
            <div class="col-md-8">
                <div class="agenda-box animation-x">
                    <div class="agenda-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="agenda-box-details">
                        <div class="agenda-box-title">{{alias('Where\'s the music?')}}</div>
                        <div class="agenda-box-time"><img src="{{asset('images/time.png')}}">12:00 AM - 11:00 PM</div>
                        <div class="agenda-box-location"><img src="{{asset('images/location.png')}}">Forum De Beirut</div>
                    </div>
                </div>
                <div class="agenda-box animation-x">
                    <div class="agenda-box-img">
                        <img src="{{asset('images/agenda2.png')}}" class="img-responsive">
                    </div>
                    <div class="agenda-box-details">
                        <div class="agenda-box-title">{{alias('Where\'s the music?')}}</div>
                        <div class="agenda-box-time"><img src="{{asset('images/time.png')}}">12:00 AM - 11:00 PM</div>
                        <div class="agenda-box-location"><img src="{{asset('images/location.png')}}">Forum De Beirut</div>
                    </div>
                </div>
                <div class="agenda-box animation-x">
                    <div class="agenda-box-img">
                        <img src="{{asset('images/agenda.png')}}" class="img-responsive">
                    </div>
                    <div class="agenda-box-details">
                        <div class="agenda-box-title">{{alias('Where\'s the music?')}}</div>
                        <div class="agenda-box-time"><img src="{{asset('images/time.png')}}">12:00 AM - 11:00 PM</div>
                        <div class="agenda-box-location"><img src="{{asset('images/location.png')}}">Forum De Beirut</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 margin-minus-15 agenda-side animation-x">
                <div class="advertisement-title">{{alias('Advertisement')}}</div>
                <img src="{{asset('images/agenda-side.png')}}" class="img-responsive agenda-side-img">
            </div>
            <div class="col-md-12 text-center">
                <a href="javascript:void(0)" class="btn agenda-btn white">{{alias('View All Events')}}</a>
            </div>
        </div>

    </div>
</div>