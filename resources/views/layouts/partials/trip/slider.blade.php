<div class="home-slider">
    <div class="trip-slider">
        @foreach ($banners as $banner)
            <img src="{{'http://cms.moovtoo.com/public/'.$banner->photo}}">
        @endforeach
    </div>
    <div class="slider-search-box">
        <div class="slider-mid">
            <div class="slider-title-trip">{{alias('I would like to')}}</div>
            <div class="slider-trip-btns">
                <a href="{{route('trip.create-trip')}}">{{alias('Create my own trip')}}</a>
                <a href="#">{{alias('Customize and existing trip')}}</a>
            </div>
        </div>
    </div>
    <div class="slider-overlay">
        <img src="{{asset('images/slider-overlay.png')}}">
    </div>
</div>
<script>
    $('.trip-slider').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: false,
        nextArrow: false,
        // cssEase: 'fade-out',
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });
</script>