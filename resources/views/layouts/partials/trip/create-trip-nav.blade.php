<div class="row">
    <div class="col-md-12">
        <div class="trip-nav">
            <div class="create-trip-nav-box">
                <div class="create-trip-arrow-up"></div>
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group select-create-trip">
                                    <div class="date-box">
                                        <div class="date-icon-box" >
                                            <span class="fa fa-calendar" id="date_created_icon" ></span>
                                        </div>
                                        <input type="date" class="form-control" id="date_created" name="date_created" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Departure date"  >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group select-create-trip">
                                    <div class="date-box">
                                        <div class="date-icon-box" >
                                            <span class="fa fa-calendar" id="date_created_icon" ></span>
                                        </div>
                                        <input type="date" class="form-control" id="date_created" name="date_created" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Arrival date" data-date=""  data-date-format="DD-MM-YYYY" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 radio-create-trip">
                                {{--<input type="checkbox" name="" value=""> <span>don’t have dates yet</span><br>--}}
                                <div class="round">
                                    <input type="checkbox" id="checkbox" />
                                    <label for="checkbox"></label>
                                    <span>don’t have dates yet</span>

                                </div>
                                {{--<input type="radio" name="gender" value="other"><span>don’t have dates yet</span><br>--}}
                                {{--<div class="input-left">--}}
                                    {{--<input type="radio" id="f-option" name="remember_me">--}}
                                    {{--<label for="f-option">don’t have dates yet</label>--}}
                                    {{--<div class="check"></div>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                        <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group select-create-trip">
                                        <select type="text" class="form-control select2 " >
                                            <option  data-thumbnail="images/create-trip-calendar.png">Travel Theme</option>
                                            <option  data-thumbnail="images/create-trip-calendar.png">Travel Theme</option>
                                            <option  data-thumbnail="images/create-trip-calendar.png">Travel Theme</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group select-create-trip">
                                    <select type="text" class="form-control select2 " >
                                        <option value=""><img src="{{asset('images/create-trip-calendar.png')}}" class="img-responsive">Travel Type</option>
                                        <option value=""><img src="{{asset('images/create-trip-calendar.png')}}" class="img-responsive">Travel Type</option>
                                        <option value=""><img src="{{asset('images/create-trip-calendar.png')}}" class="img-responsive">Travel Type</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group select-create-trip">
                                    <select type="text" class="form-control select2 " >
                                        <option value=""><img src="{{asset('images/create-trip-calendar.png')}}" class="img-responsive">Daily budget</option>
                                        <option value=""><img src="{{asset('images/create-trip-calendar.png')}}" class="img-responsive">Daily budget</option>
                                        <option value=""><img src="{{asset('images/create-trip-calendar.png')}}" class="img-responsive">Daily budget</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="nav-btn">
                            <a class="go-btn" href="{{route('trip.question-trip')}}">Go</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

