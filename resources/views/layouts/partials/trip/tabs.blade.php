<div class="trip-tabs">
    <div class="trip-tab hidden-xs">
        <div class="trip-tab-links active" id="tab-1" >All</div>
        <div class="trip-tab-links" id="tab-2" >ADVENTURE</div>
        <div class="trip-tab-links" id="tab-3" >RELAXATION</div>
        <div class="trip-tab-links" id="tab-4" >CULTURE</div>
        <div class="trip-tab-links" id="tab-5" >BUDGET FRIENDLY</div>
        <div class="trip-tab-links" id="tab-6" >FAMILY TRIP</div>
    </div>
    <div class="trip-tab-dropdown visible-xs">
        <div class="trip-dropdown-head">
            <span class="trip-dropdown-active">Choose a theme</span>
            <span class="trip-dropdown-icon"><i class="fa fa-angle-down"></i></span>
        </div>
        <ul>
            <li class="active" id="tab-1" >All</li>
            <li class="" id="tab-2" >ADVENTURE</li>
            <li class="" id="tab-3" >RELAXATION</li>
            <li class="" id="tab-4" >CULTURE</li>
            <li class="" id="tab-5" >BUDGET FRIENDLY</li>
            <li class="" id="tab-6" >FAMILY TRIP</li>
        </ul>
    </div>
</div>