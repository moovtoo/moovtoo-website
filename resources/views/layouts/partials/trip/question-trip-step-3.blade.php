<div class="row">
    <div class="col-md-12">
        <div class="trip-nav">
            <div class="create-trip-nav-box">
                <div class="create-trip-arrow-up"></div>
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="top-title-orange-tip-steps">
                                        <div class="trip-margin-top"></div>
                                        Step 3 of 3
                                        <div class="underline-orange"></div>
                                    </div>
                                    <div class="top-title-steps">
                                        <div class="steps-margin-top"></div>
                                        Let's Recap!
                                    </div>
                                    <div class="filling-info">
                                        Review all the info before sending them to your travel advisor and proceed to payment
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        What is your destination?
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <ul class="step-3-detail">
                                        <li>France</li>
                                    </ul>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        What is your Travel Type?
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <ul class="step-3-detail">
                                        <li>Culture</li>
                                        <li>Relaxation</li>
                                    </ul>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        When Would you like to travel?
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <ul class="step-3-detail">
                                        <li>Travelling in February for 5 days</li>
                                    </ul>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        Your Budget
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <ul class="step-3-detail">
                                        <li>50 euros per person per day</li>
                                    </ul>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        What are you looking for?
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="sub-title-steps">Accomodation type</div>
                                    <ul class="step-3-detail">
                                        <li>Mid Range Hotel ( 3 stars )</li>
                                    </ul>
                                    <div class="sub-title-steps">Services you might need</div>
                                    <ul class="step-3-detail">
                                        <li>Car rental</li>
                                        <li>Airport pick up</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        For Food & Drinks you prefer?
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <ul class="step-3-detail">
                                        <li>Local Food</li>
                                        <li>Trendy</li>
                                        <li>Hipster</li>
                                    </ul>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        At which tempo would you like to spend your journey
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">

                                        <ul class="step-3-detail tempo-detail">
                                            <div class="ul-display-flex">
                                            <li>Fast tempo</li><span class="span-detail-l"> I would like to experience as many things as possible</span>
                                            </div>
                                       </ul>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        Why did you choose this destination?
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <ul class="step-3-detail">
                                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                            ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu . </li>
                                    </ul>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="steps-btns">
                                        <div class="back-step">
                                            <a href="{{route('trip.question-trip-step-2')}}">
                                                <img src="{{asset('images/steps-arrow-left.svg')}}" class=" img-responsive">
                                            </a>
                                        </div>
                                        <div class="next-step">
                                            <a href="{{route('trip.payment-plan')}}">
                                                <img src="{{asset('images/steps-arrow-right.svg')}}" class=" img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

