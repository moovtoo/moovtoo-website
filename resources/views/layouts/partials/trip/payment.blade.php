<div class="container">
    <div class="row">
        <div class="col-md-12 padding-0">
            <div class="payment-plan">
                <div class="trip-suggestion-box">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="suggestion-title">
                                {{alias('MOOVTOO SUGGESTION')}}
                            </div>
                            <div class="top-title-orange">
                                Our Payment Plans
                                <div class="underline-orange"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="payment-box">
                                <div class="colored-price-blue">
                                    <div class="payment-price">
                                        <div class="payment-price-currency">
                                            <span class="currency">$</span>19</div>
                                        <div class="payment-per-month">
                                            Per Month
                                        </div>
                                    </div>
                                    <div class="payment-description">
                                        STANDARD
                                    </div>
                                </div>
                                <div class="payment-detail-box">
                                    <div class="payment-detail"> Custom daily travel plan
                                        get paired with an expert trip designer
                                        unlimited email/ chat with your designer
                                        includes up to 4 travels</div>
                                    <button class="payment-buy-btn">Buy Now</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="payment-box">
                                <div class="colored-price-orange">
                                    <div class="payment-price">
                                        <div class="payment-price-currency">
                                            <span class="currency">$</span>19</div>
                                        <div class="payment-per-month">
                                            Per Month
                                        </div>
                                    </div>
                                    <div class="payment-description">
                                        STANDARD
                                    </div>
                                </div>
                                <div class="payment-detail-box">
                                    <div class="payment-detail"> Custom daily travel plan
                                        get paired with an expert trip designer
                                        unlimited email/ chat with your designer
                                        includes up to 4 travels</div>
                                    <button class="payment-buy-btn-blue">Buy Now</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="payment-box">
                                <div class="colored-price-blue">
                                    <div class="payment-price">
                                        <div class="payment-price-currency">
                                            <span class="currency">$</span>19</div>
                                        <div class="payment-per-month">
                                            Per Month
                                        </div>
                                    </div>
                                    <div class="payment-description">
                                        STANDARD
                                    </div>
                                </div>
                                <div class="payment-detail-box">
                                    <div class="payment-detail"> Custom daily travel plan
                                        get paired with an expert trip designer
                                        unlimited email/ chat with your designer
                                        includes up to 4 travels</div>
                                    <button class="payment-buy-btn">Buy Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>