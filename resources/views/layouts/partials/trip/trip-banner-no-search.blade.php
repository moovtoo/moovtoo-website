<div class="home-slider">
    <div class="trip-banner">
        <img src="{{asset('images/trip-background.jpg')}}">
    </div>
    <div class="slider-search-box">
        <div class="slider-mid-trip">
            <div class="slider-title-trip">{{alias('Create My Own Trip')}}</div>
        </div>
    </div>
</div>
