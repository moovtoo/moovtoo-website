<div class="row">
    <div class="col-md-12">
        <div class="trip-nav">
            <div class="trip-nav-box">
                <div class="arrow-up"></div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="trip-margin-top">
                                </div>
                              <div class="top-title-orange">
                                  {{alias('How Does It Works')}}
                              <div class="underline-orange"></div>
                          </div>
                        </div>
                    </div>
                <div class="trip-nav-box-detail">
                    {{count($howItWorks)}} {{alias('Simple steps to book you dream journy')}}
                </div>
            </div>
            <div class="trip-nav-items">
                @foreach($howItWorks as $how)
                    <div class="trip-nav-item">
                        <div class="trip-nav-image">
                            <a href="javascript:void(0)">
                                <img src="http://cms.moovtoo.com/public/{{$how->icon}}" class="img-responsive">
                            </a>
                        </div>
                        <div class="trip-nav-detail">
                            <a href="javascript:void(0)">
                                {{$how->name}}
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

