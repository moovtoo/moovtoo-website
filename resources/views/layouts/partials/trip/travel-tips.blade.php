<div class="row " >
    <div class="col-md-12 padding-0">
        <div class="trip-suggestion">
            <div class="trip-suggestion-box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="suggestion-title">
                            {{alias('MOOVTOO SUGGESTION')}}
                        </div>
                        <div class="top-title-orange">
                            Travel Articles & Tips
                            <div class="underline-orange"></div>
                        </div>
                        @include('layouts.partials.trip.tabs')
                        <div class="travel-tips-slider container">
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="suggestion-box">
                                <div class="suggestion-box-image">
                                    <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                </div>
                                <div class="suggestion-detail">
                                    <div class="suggestion-detail-title">
                                        Hiking in the Alpes
                                    </div>
                                    <div class="trip-suggestion-detail-div">
                                        <div class="suggestion-detail-location">
                                            <div class="suggestion-location-img"><img src="{{asset('images/suggestion-person.png')}}" class="img-responsive"></div>
                                            <div class="suggestion-location-name">John Doe</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>