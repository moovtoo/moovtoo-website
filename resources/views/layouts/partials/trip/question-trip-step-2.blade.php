<div class="row">
    <div class="col-md-12">
        <div class="trip-nav">
            <div class="create-trip-nav-box">
                <div class="create-trip-arrow-up"></div>
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="top-title-orange-tip-steps">
                                        <div class="trip-margin-top"></div>
                                        Step 2 of 3
                                        <div class="underline-orange"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="title-steps">
                                    For Food & Drinks you prefer?
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="local-cuisine" name="local-cuisine" />
                                        <label for="local-cuisine"></label>
                                        <span> Local cuisine</span>
                                    </div>

                                </div>
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="trendy-places" name="trendy-places" />
                                        <label for="trendy-places"></label>
                                        <span> Trendy places</span>
                                    </div>

                                </div>
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="halal" name="halal" />
                                        <label for="halal"></label>
                                        <span> Halal</span>
                                    </div>
                                </div>
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="casual" name="casual" />
                                        <label for="casual"></label>
                                        <span>Casual</span>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="hipster-place" name="hipster-place" />
                                        <label for="hipster-place"></label>
                                        <span>Hipster place</span>
                                    </div>

                                </div>
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="fine-dining" name="fine-dining" />
                                        <label for="fine-dining"></label>
                                        <span>Fine dining</span>
                                    </div>
                                </div>
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="party-scenes" name="party-scenes" />
                                        <label for="party-scenes"></label>
                                        <span>Party scenes</span>
                                    </div>

                                </div>
                                <div class="col-md-3 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="vegan" name="vegan" />
                                        <label for="vegan"></label>
                                        <span>Vegan</span>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="provide-us">
                                        <textarea id="myTextarea" rows="3" placeholder="Any additional requirement you would like us to take into consideration?&#10;- Food allergies..."></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="title-steps">
                                        At which tempo would you like to spend your journey
                                    </div>
                                </div>
                                <div class="col-md-12 padding-0">
                                    <div class="squaredThree-booked left">
                                        <input type="checkbox" value="None" id="fast-tempo" name="fast-tempo" />
                                        <label for="fast-tempo"></label>
                                        <span>Fast tempo </span><span class="span-detail-l">i would like to experience as many things as possible</span>
                                    </div>

                                </div>
                                <div class="col-md-12 padding-0">
                                    <div class="squaredThree-booked left">
                                        <input type="checkbox" value="None" id="moderate-tempo" name="moderate-tempo" />
                                        <label for="moderate-tempo"></label>
                                        <span>Moderate tempo </span><span class="span-detail-l">i would like to mix between multi experiences and relaxation</span>
                                    </div>

                                </div>
                                <div class="col-md-12 padding-0">
                                    <div class="squaredThree-booked left">
                                        <input type="checkbox" value="None" id="slow-tempo" name="slow-tempo" />
                                        <label for="slow-tempo"></label>
                                        <span>Slow tempo </span><span class="span-detail-l">i would like to spend relaxing vacation wit the important sight seeing</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="title-steps">
                                    Why did you choose this destination?
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="provide-us">
                                        <textarea id="myTextarea" rows="3" placeholder="Help your trip advisor know what triggered you to choose this exact destination  "></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="next-step-btn">
                                        <a class="next-btn"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="steps-btns">
                                        <div class="back-step">
                                            <a href="{{route('trip.question-trip')}}">
                                                <img src="{{asset('images/steps-arrow-left.svg')}}" class=" img-responsive">
                                            </a>
                                        </div>
                                        <div class="next-step">
                                            <a href="{{route('trip.question-trip-step-3')}}">
                                                <img src="{{asset('images/steps-arrow-right.svg')}}" class=" img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

