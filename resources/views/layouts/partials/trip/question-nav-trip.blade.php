<div class="row">
    <div class="col-md-12">
        <div class="trip-nav">
            <div class="create-trip-nav-box">
                <div class="create-trip-arrow-up"></div>
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="top-title-orange-tip-steps">
                                            <div class="trip-margin-top"></div>
                                            Step 1 of 3
                                            <div class="underline-orange"></div>
                                        </div>
                                        <div class="top-title-steps">
                                            <div class="steps-margin-top"></div>
                                           Let's go!
                                        </div>
                                        <div class="filling-info">
                                            By filling all the info below, you are helping your trip advisor creating your unique journey based on your exact needs.
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="title-steps">
                                    When would you like to travel?
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 padding-0">
                                    <div class="form-group select-create-steps">
                                        <div class="date-box">
                                            <div class="date-icon-box" >
                                                <span class="fa fa-calendar" id="date_created_icon" ></span>
                                            </div>
                                            <input type="date" class="form-control" id="date_created" name="date_created" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Approximate date"  >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 padding-0">
                                    <div class="form-group select-create-steps">
                                        <div class="number-of-days">
                                        <span>
                                                Number of days
                                        </span>
                                        </div>
                                        <div class="date-box">
                                            <input type="number" class="form-control" id="" name="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="title-steps">
                                    What are you looking for?
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="sub-title-steps">
                                        Accomodation type
                                    </div>
                                </div>
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="budget-hotel" name="budget-hotel" />
                                        <label for="budget-hotel"></label>
                                        <span>Budget hotel ( 1 -2  stars )</span>
                                    </div>

                                </div>
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="Mid-range" name="Mid-range" />
                                        <label for="Mid-range"></label>
                                        <span>Mid-range hotel ( 3-4 stars )</span>
                                    </div>

                                </div>
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="Luxury" name="Luxury" />
                                        <label for="Luxury"></label>
                                        <span> Luxury hotel ( 5+)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="Private" name="Private" />
                                        <label for="Private"></label>
                                        <span>Private Home</span>
                                    </div>

                                </div>
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="guest-house" name="guest-house" />
                                        <label for="guest-house"></label>
                                        <span>Guest house</span>
                                    </div>

                                </div>
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="villa" name="villa" />
                                        <label for="villa"></label>
                                        <span>Villa</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree-booked left">
                                        <input type="checkbox" value="None" id="booked-place" name="booked-place" />
                                        <label for="booked-place"></label>
                                        <span>I already booked a place</span>
                                    </div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                <div class="provide-us">
                                    <textarea id="myTextarea" rows="8" placeholder="Provides us with the details of the venue you booked&#10;&#10; - Name &#10;&#10; - Address &#10;&#10; - Or simply copy paste the link of the reservation "></textarea>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="sub-title-steps">
                                        Service you might need
                                    </div>
                                </div>
                                <div class="col-md-2 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="airport" name="airport" />
                                        <label for="airport"></label>
                                        <span>Airport pickup</span>
                                    </div>

                                </div>
                                <div class="col-md-2 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="rental" name="rental" />
                                        <label for="rental"></label>
                                        <span>Car Rental</span>
                                    </div>

                                </div>
                                <div class="col-md-2 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="driver" name="driver" />
                                        <label for="driver"></label>
                                        <span>Driver</span>
                                    </div>
                                </div>

                                <div class="col-md-2 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="guide" name="guide" />
                                        <label for="guide"></label>
                                        <span>Guide</span>
                                    </div>
                                </div>
                                <div class="col-md-2 padding-0">
                                    <div class="squaredThree left">
                                        <input type="checkbox" value="None" id="travel-insurance" name="travel-insurance" />
                                        <label for="travel-insurance"></label>
                                        <span>Travel Insurance</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 padding-0">
                                    <div class="squaredThree-booked left">
                                        <input type="checkbox" value="None" id="flight" name="flight" />
                                        <label for="flight"></label>
                                        <span>I already booked a flight</span>
                                    </div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 padding-0">
                                    <div class="provide-us">
                                        <textarea id="myTextarea" rows="3" placeholder="Any other service (s) you may need? let us know"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="steps-btns">

                                        <div class="next-step">
                                            <a href="{{route('trip.question-trip-step-2')}}">
                                                <img src="{{asset('images/steps-arrow-right.svg')}}" class=" img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

