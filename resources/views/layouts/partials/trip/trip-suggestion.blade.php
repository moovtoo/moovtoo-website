
<div class="row ">
    <div class="col-md-12 ">
        <div class="trip-suggestion">
            <div class="container">
                <div class="arrow-down"></div>
                <div class="trip-suggestion-box">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="suggestion-title">
                                {{alias('MOOVTOO SELECTION')}}
                            </div>
                            @isset($travelLocation)
                                @include('layouts.partials.title', ['title'=>$travelLocation->name])
                            @else
                                @include('layouts.partials.title', ['title'=>'Select Destination'])
                            @endisset

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @include('layouts.partials.trip.tabs')
                            <div class="your-class">
                                @foreach ($trips as $trip)
                                    <div class="suggestion-box">
                                        <div class="suggestion-box-image ">
                                            <img src="http://cms.moovtoo.com/public/{{$trip->image}}" class="img-responsive">
                                            {{-- <div class="image-price">800 €</div> --}}
                                        </div>
                                        <div class="suggestion-detail">
                                            <div class="suggestion-detail-title">
                                                {{$trip->name}}
                                            </div>
                                            <div class="trip-suggestion-detail-div">
                                                @isset($trip->location)
                                                <div class="suggestion-detail-location">
                                                    <div class="suggestion-location-img">
                                                        <img src="{{asset('images/orange-location.png')}}"
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="suggestion-location-name">{{$trip->location->name}}</div>
                                                </div>
                                                @endisset
                                                <div class="suggestion-detail-rating">
                                                    <div class="suggestion-rating-img">
                                                        <img src="{{asset('images/suggestion-face.png')}}"
                                                            class="img-responsive">
                                                    </div>
                                                    <div class="suggestion-rating-name">8.0 Superb</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="more-detail col-md-12">
                                            <div class="suggestion-calendar col-md-5">
                                                <div class="calendar-img">
                                                    <img src="{{asset('images/suggestion-calendar.png')}}" class="img-responsive">
                                                </div>
                                                <div class="calendar-days">
                                                    {{\Carbon\Carbon::parse($trip->end_date)->diffInDays(\Carbon\Carbon::parse($trip->start_date))}}
                                                </div>
                                            </div>
                                            <div class="suggestion-person col-md-6 offset-1">
                                                <div class="person-img">
                                                    <img src="{{asset('images/suggestion-person.png')}}" class="img-responsive">
                                                </div>
                                                <div class="person-count">
                                                    {{$trip->persons}} person
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="more-suggestions-btn">
                                <a class="more-suggestion-btn">More Packages</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.your-class').slick({
        autoplay: true,
        autoplaySpeed: 8000,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            // {
            // breakpoint: 1024,
            //     settings: {
            //         slidesToShow: 3,
            //         slidesToScroll: 3,
            //         }
            //     },
            {
                breakpoint: 910,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                    }
                },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                    }
                }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
    });
</script>