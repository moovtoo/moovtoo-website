<div class="row background-white" >
    <div class="trip-margin-top">
    </div>
    <div class="col-md-5 hidden-xs">
        <div class="download-app-img">
            <img src="{{asset('images/iphone-in-hand.png')}}" class="img-responsive">
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="download-app">
            <div class="top-title-orange-app">
                {{alias('Download our mobile APP')}}
                <div class="underline-orange-app"></div>
            </div>
            <div class="download-app-detail">
                Qisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex eacommodo consequat.
                Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius claritas.
                Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
            </div>
            <div class="download-app-bullet">
                <div class="app-bullet"><img src="{{asset('images/app-detail-arrow.png')}}" class="img-responsive">Nam libertempor cum soluta nobis eleifend option;</div>
                <div class="app-bullet"><img src="{{asset('images/app-detail-arrow.png')}}" class="img-responsive">Option conguenihil imperdiet doming id quod mazim placerat facer;</div>
                <div class="app-bullet"><img src="{{asset('images/app-detail-arrow.png')}}" class="img-responsive">Eodem modotypi, qui nunc nobis videntur parum futurum;</div>
                <div class="app-bullet"><img src="{{asset('images/app-detail-arrow.png')}}" class="img-responsive">Investigationes demonstraverunt lectores</div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 visible-xs">
        <div class="download-app-img">
            <img src="{{asset('images/iphone-in-hand.png')}}" class="img-responsive">
        </div>
    </div>
</div>