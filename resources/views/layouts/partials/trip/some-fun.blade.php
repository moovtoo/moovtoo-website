<div class="row ">
    <div class="col-md-12 ">
        <div class="trip-suggestion">
            <div class="container">
                <div class="arrow-down"></div>
                <div class="trip-suggestion-box">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="suggestion-title">
                                {{alias('MOOVTOO SUGGESTION')}}
                            </div>
                            @include('layouts.partials.title', ['title'=>'Let\'s have some fun'])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @include('layouts.partials.trip.tabs')
                            <div class="some-fun-slider">
                                <div class="suggestion-box">
                                    <div class="suggestion-box-image-event">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                         <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Alpes France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">4 hours</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"> <img src="{{asset('images/reviews.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">8.0 Superb</div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="trip-organized-by">
                                        <div class="organized-by-img"><img src="{{asset('images/organized-by-profile.png')}}" class="img-responsive"></div>
                                        <div class="organized-by-name">Organized by <span>yakoukou</span></div>
                                    </div>
                                </div>
                                <div class="suggestion-box">
                                    <div class="suggestion-box-image-event">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Alpes France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">4 hours</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"> <img src="{{asset('images/reviews.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">8.0 Superb</div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="trip-organized-by">
                                        <div class="organized-by-img"><img src="{{asset('images/organized-by-profile.png')}}" class="img-responsive"></div>
                                        <div class="organized-by-name">Organized by <span>yakoukou</span></div>
                                    </div>
                                </div>
                                <div class="suggestion-box">
                                    <div class="suggestion-box-image-event">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Alpes France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">4 hours</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"> <img src="{{asset('images/reviews.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">8.0 Superb</div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="trip-organized-by">
                                        <div class="organized-by-img"><img src="{{asset('images/organized-by-profile.png')}}" class="img-responsive"></div>
                                        <div class="organized-by-name">Organized by <span>yakoukou</span></div>
                                    </div>
                                </div>
                                <div class="suggestion-box">
                                    <div class="suggestion-box-image-event">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Alpes France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">4 hours</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"> <img src="{{asset('images/reviews.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">8.0 Superb</div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="trip-organized-by">
                                        <div class="organized-by-img"><img src="{{asset('images/organized-by-profile.png')}}" class="img-responsive"></div>
                                        <div class="organized-by-name">Organized by <span>yakoukou</span></div>
                                    </div>
                                </div>
                                <div class="suggestion-box">
                                    <div class="suggestion-box-image-event">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Alpes France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">4 hours</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"> <img src="{{asset('images/reviews.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">8.0 Superb</div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="trip-organized-by">
                                        <div class="organized-by-img"><img src="{{asset('images/organized-by-profile.png')}}" class="img-responsive"></div>
                                        <div class="organized-by-name">Organized by <span>yakoukou</span></div>
                                    </div>
                                </div>
                                <div class="suggestion-box">
                                    <div class="suggestion-box-image-event">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Alpes France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">4 hours</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"> <img src="{{asset('images/reviews.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">8.0 Superb</div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="trip-organized-by">
                                        <div class="organized-by-img"><img src="{{asset('images/organized-by-profile.png')}}" class="img-responsive"></div>
                                        <div class="organized-by-name">Organized by <span>yakoukou</span></div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="more-suggestions-btn">
                                <a class="more-suggestion-btn" >All activities</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
