<div class="row">
    <div class="col-md-12">
        <div class="trip-nav">
            <div class="create-trip-nav-box">
                <div class="create-trip-arrow-up"></div>
                <div class="row">
                    <div class="payment-container">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="top-title-orange-tip-steps">
                                        <div class="trip-margin-top"></div>
                                        {{alias('Moovtoo Plans')}}
                                        <div class="underline-orange"></div>
                                    </div>
                                    <div class="top-title-steps">
                                        <div class="steps-margin-top"></div>
                                        {{alias('Choose a plan')}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @include('layouts.partials.trip.final-payment')
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="steps-btns">
                                        <div class="back-step">
                                            <a href="{{route('trip.question-trip-step-3')}}">
                                                <img src="{{asset('images/steps-arrow-left.svg')}}" class=" img-responsive">
                                            </a>
                                        </div>
                                        <div class="next-step">
                                            <a href="#">
                                                <img src="{{asset('images/steps-arrow-right.svg')}}" class=" img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

