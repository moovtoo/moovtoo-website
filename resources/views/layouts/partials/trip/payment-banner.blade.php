<div class="home-slider">
    <div class="trip-banner">
        <img src="{{asset('images/trip-background.jpg')}}">
    </div>
    <div class="slider-search-box">
        <div class="slider-mid-trip">
            <div class="banner-suggestion-title">{{alias('You\'re few steps from finalising your journey')}}</div>
            <div class="slider-title-trip">{{alias('Tell Us What You Like')}}</div>
        </div>
    </div>
</div>
