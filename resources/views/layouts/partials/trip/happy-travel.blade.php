<div class="row ">
    <div class="col-md-12 padding-0">
        <div class="travel-suggestion">
            <div class="trip-suggestion-box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="trip-travel-title">
                            {{alias('MOOVTOO SUGGESTION')}}
                        </div>
                        <div class="travel-top-title-orange">
                            Our Happy Travellers
                            <div class="underline-orange"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6  travel-img-box">
                                <div class=" travel-img">
                                    <img src="{{asset('images/happy-travel.png')}}" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="travel-class ">
                                    <div class="travel-slider">
                                        <div class="travel-slider-img">
                                            <img src="{{asset('images/travel-photo.png')}}" class="img-responsive">
                                        </div>
                                        <div class="travel-slider-description">
                                            “ We had a blast in Nice!
                                            Our trip was exactly as we wanted it to be :) ”
                                        </div>
                                        <div class="travel-slider-name">
                                            Nicol
                                        </div>

                                    </div>
                                    <div class="travel-slider">
                                        <div class="travel-slider-img">
                                            <img src="{{asset('images/travel-photo.png')}}" class="img-responsive">
                                        </div>
                                        <div class="travel-slider-description">
                                            “ We had a blast in Nice!
                                            Our trip was exactly as we wanted it to be :) ”
                                        </div>
                                        <div class="travel-slider-name">
                                            Nicol
                                        </div>

                                    </div>
                                    <div class="travel-slider">
                                        <div class="travel-slider-img">
                                            <img src="{{asset('images/travel-photo.png')}}" class="img-responsive">
                                        </div>
                                        <div class="travel-slider-description">
                                            “ We had a blast in Nice!
                                            Our trip was exactly as we wanted it to be :) ”
                                        </div>
                                        <div class="travel-slider-name">
                                            Nicol
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@push('js')
<script>
    $('.travel-class').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
    });
</script>
@endpush