<div class="home-slider">
    <div class="trip-banner">
        @isset($travelLocation)
            <img src="{{'http://cms.moovtoo.com/public/'.$travelLocation->featured_image}}">
        @else
            <img src="{{asset('images/trip-background.jpg')}}">
        @endif
    </div>
    <div class="slider-search-box">
        <div class="slider-mid-trip">
            <div class="slider-title-trip">{{alias('Create My Own Trip')}}</div>
            <div class="row search-bar-trip" >
                <div class="col-md-8 search-bar-margin ">
                    <div class="slider-search-trip static-search " >
                        <div class="autocomplete-trip">
                            <input   class="search-input" id="myInput" type="text" name="myCountry"  value="@isset($travelLocation){{$travelLocation->name}}@endisset" placeholder="Choose a destination...">
                        </div>
                        <div class="search-i-trip">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
