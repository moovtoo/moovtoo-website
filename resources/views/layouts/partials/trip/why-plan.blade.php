<div class="row ">
    <div class="col-md-12 padding-0">
        <div class="trip-suggestion">

            <div class="trip-suggestion-box">

                <div class="row">

                    <div class="col-md-12">
                        <div class="suggestion-title">
                            <div class="trip-margin-top">
                            </div>
                            {{alias('MOOVTOO SUGGESTION')}}
                        </div>
                        <div class="top-title-orange">
                            Why Plan Your Trips With Moovtoo?
                            <div class="underline-orange"></div>
                        </div>
                        <div class="container ">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="travel-option">
                                    <div class="travel-option-img">
                                        <img src="{{asset('images/time-saving.svg')}}" class="img-responsive">
                                    </div>
                                    <div class="travel-option-detail">
                                        Time Saving
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="travel-option">
                                    <div class="travel-option-img">
                                        <img src="{{asset('images/professional-experts.svg')}}" class="img-responsive">
                                    </div>
                                    <div class="travel-option-detail">
                                        Professional Experts
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="travel-option">
                                    <div class="travel-option-img">
                                        <img src="{{asset('images/low-fees.svg')}}" class="img-responsive">
                                    </div>
                                    <div class="travel-option-detail">
                                        Low Fees
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="travel-option">
                                    <div class="travel-option-img">
                                        <img src="{{asset('images/unique-experience.svg')}}" class="img-responsive">
                                    </div>
                                    <div class="travel-option-detail">
                                        Unique Experience
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>