<div class="community-box">
    <div class="container">
        <div class="row home-box">
            <div class="col-md-12">
{{--                @include('layouts.partials.title', ['title'=>'Community'])--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="travel-top-title-orange">
                            Community
                            <div class="underline-orange"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="community-comments">
                            <div class="row comment-box animation-x">
                                <div class="col-md-2 col-sm-2">
                                    <img src="{{asset('images/user1.png')}}" class="comment-user-img">
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="comment-user-name">Joe Doe</div>
                                    <div class="comment-date">February 11 at 7:25pm</div>
                                    <div class="comment-text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it</div>
                                </div>
                            </div>
                            <div class="row comment-box animation-x">
                                <div class="col-md-2 col-sm-2">
                                    <img src="{{asset('images/user2.png')}}" class="comment-user-img">
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="comment-user-name">Joe Doe</div>
                                    <div class="comment-date">February 11 at 7:25pm</div>
                                    <div class="comment-text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it</div>
                                </div>
                            </div>
                            <div class="row comment-box animation-x">
                                <div class="col-md-2 col-sm-2">
                                    <img src="{{asset('images/user1.png')}}" class="comment-user-img">
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="comment-user-name">Joe Doe</div>
                                    <div class="comment-date">February 11 at 7:25pm</div>
                                    <div class="comment-text">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it</div>
                                </div>
                            </div>
                            <div class="row comment-box animation-x">
                                <div class="col-md-2 col-sm-2">
                                    <img src="{{asset('images/user1.png')}}" class="comment-user-img">
                                </div>
                                <div class="col-md-10 col-sm-10 flex">
                                    <input type="text" name="comment" placeholder="Write a comment..." class="form-input comment-input">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="community-login">
                            <div class="login-form animation-x-inv">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="top-title">
                                            JOIN OUR COMMUNITY
                                            <div class="underline"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group-login">
                                    <div class="input-login">
                                        <img src="{{asset('images/email.svg')}}">
                                        <input type="email" class="" placeholder="Email Address">
                                    </div>
                                    <div class="input-login">
                                        <img src="{{asset('images/password.svg')}}">
                                        <input type="password" class="" placeholder="Password">
                                    </div>
                                    <button class="btn-community-login">Login to your account</button>
                                    <div class="login-footer">
                                        <div class="input-left">
                                            <input type="radio" id="f-option" name="remember_me">
                                            <label for="f-option">Remember me</label>
                                            <div class="check"></div>
                                        </div>
                                        <div class="input-right">
                                            <p><span>New here?</span> Sign in!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>