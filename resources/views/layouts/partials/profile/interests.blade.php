<div class="container profile-tab-content" id="interests">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-box top-25">
                <div class="tab-head">
                    {{alias('Interests')}}
                </div>
            </div>
            <div class="profile-box top-5 bottom">
                <div class="tab">
                    <div class="tab-title">
                        {{alias('What are you interested about? Moovtoo will send you suggestions')}}
                    </div>
                    <div class="row">
                        <div class="profile-interests-list">
                            <div class="profile-dropdown-title">
                                Outdoors
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="profile-dropdown-list">
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="profile-interests-list">
                            <div class="profile-dropdown-title">
                                Outdoors
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="profile-dropdown-list">
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="profile-interests-list">
                            <div class="profile-dropdown-title">
                                Outdoors
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="profile-dropdown-list">
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="profile-interests-list">
                            <div class="profile-dropdown-title">
                                Outdoors
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="profile-dropdown-list">
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="profile-interests-list">
                            <div class="profile-dropdown-title">
                                Outdoors
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="profile-dropdown-list">
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="profile-interests-list">
                            <div class="profile-dropdown-title">
                                Outdoors
                                <i class="fa fa-plus"></i>
                            </div>
                            <div class="profile-dropdown-list">
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="you_are" value="person">Hiking
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>