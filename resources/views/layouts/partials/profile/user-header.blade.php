<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-box top-25">
                <div class="user-cover">
                    <img src="{{asset('images/user-cover.png')}}">
                </div>
                <div class="user-profile">
                    <img class="user-profile-img" src="{{asset('images/user.png')}}">
                    <div class="user-name">Jean Moulin</div>
                    <div class="user-location">Paris - France</div>
                </div>
            </div>
        </div>
    </div>
</div>