<div class="container profile-tab-content" id="about">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-box top-25">
                <div class="tab-head">
                    {{alias('About')}}
                </div>
            </div>
            <div class="profile-box top-5 bottom">
                <div class="tab">
                    <div class="tab-title underline">
                        {{alias('Contact and Basic info')}}
                    </div>
                    <div class="about-radio">
                        <label class="label-inline">
                            {{alias('You are')}}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="you_are" value="person">{{alias('A Person')}}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="you_are" value="company">{{alias('A Company')}}
                        </label>
                    </div>
                    <div class="row about-form">
                        <div class="col-md-6">
                            <div class="form-group about-form-group">
                                <label>{{alias('Full Name')}}</label>
                                <input type="text" name="name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group about-form-group">
                                <label>{{alias('Email')}}</label>
                                <input type="email" name="email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group about-form-group">
                                <label>{{alias('Bith Date')}}</label>
                                <input type="date" name="dob">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group about-form-group">
                                <label>{{alias('Country')}}</label>
                                <select name="country_id" id="country_id" class="select2">
                                    <option value="">{{alias('Select Country')}}</option>
                                    @foreach ($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group about-form-group">
                                <label>{{alias('Spoker Language')}}</label>
                                <input type="text" name="spoken_language">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group about-form-group">
                                <label>{{alias('Phone Number')}}</label>
                                <input type="tel" name="phone">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tab-title">
                                {{alias('Your Description')}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="about-form-group">
                                <textarea id="description" name="description"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tab-title">
                                {{alias('Your social media')}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="inline-form-group">
                                    <i class="fa fa-linkedin"></i>
                                    <input type="text" name="linkedin">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="inline-form-group">
                                    <i class="fa fa-twitter"></i>
                                    <input type="text" name="twitter">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="inline-form-group">
                                    <i class="fa fa-facebook"></i>
                                    <input type="text" name="facebook">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="inline-form-group">
                                    <i class="fa fa-google-plus"></i>
                                    <input type="text" name="google">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="inline-form-group">
                                    <i class="fa fa-youtube"></i>
                                    <input type="text" name="youtube">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>