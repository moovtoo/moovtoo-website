<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="profile-box top-5">
                <div class="profile-tabs">
                    <div class="profile-tab">Timeline</div>
                    <div class="profile-tab active" target="about">About</div>
                    <div class="profile-tab" target="interests">Interests</div>
                    <div class="profile-tab">Favourites</div>
                    <div class="profile-tab">Bookings</div>
                    <div class="profile-tab">Payments</div>
                </div>
            </div>
        </div>
    </div>
</div>