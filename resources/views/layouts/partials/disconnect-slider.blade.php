<div class="container">
    @include('layouts.partials.title', ['title'=>'Disconnect'])
    <div class="row home-box">
        <div class="col-md-12">
            <div class="disconnect-slider">
                <div class="disconnect-slide-image">
                    <img src="{{asset('images/slide1.png')}}">
                    <img src="{{asset('images/vertical-overlay.png')}}" class="disconnect-slide-overlay">
                    <p class="disconnect-slide-text">Test</p>
                </div>
                <div class="disconnect-slide-image">
                    <img src="{{asset('images/slide2.png')}}">
                    <img src="{{asset('images/vertical-overlay.png')}}" class="disconnect-slide-overlay">
                    <p class="disconnect-slide-text">Test</p>
                </div>
                <div class="disconnect-slide-image">
                    <img src="{{asset('images/paris.jpg')}}">
                    <img src="{{asset('images/vertical-overlay.png')}}" class="disconnect-slide-overlay">
                    <p class="disconnect-slide-text">Test</p>
                </div>
                <div class="disconnect-slide-image">
                    <img src="{{asset('images/slide1.png')}}">
                    <img src="{{asset('images/vertical-overlay.png')}}" class="disconnect-slide-overlay">
                    <p class="disconnect-slide-text">Test</p>
                </div>
                <div class="disconnect-slide-image">
                    <img src="{{asset('images/slide1.png')}}">
                    <img src="{{asset('images/vertical-overlay.png')}}" class="disconnect-slide-overlay">
                    <p class="disconnect-slide-text">Test</p>
                </div>
                <div class="disconnect-slide-image">
                    <img src="{{asset('images/slide1.png')}}">
                    <img src="{{asset('images/vertical-overlay.png')}}" class="disconnect-slide-overlay">
                    <p class="disconnect-slide-text">Test</p>
                </div>
                <div class="disconnect-slide-image">
                    <img src="{{asset('images/slide1.png')}}">
                    <img src="{{asset('images/vertical-overlay.png')}}" class="disconnect-slide-overlay">
                    <p class="disconnect-slide-text">Test</p>
                </div>
            </div>
        </div>
    </div>
</div>