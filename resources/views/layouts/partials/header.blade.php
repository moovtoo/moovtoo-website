<header>
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div id="dismiss">
            <i class="fas fa-arrow-left"></i>
        </div>
    
        <ul class="list-unstyled components">
            <p>Dummy Heading</p>
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Home</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Home 1</a>
                    </li>
                    <li>
                        <a href="#">Home 2</a>
                    </li>
                    <li>
                        <a href="#">Home 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">About</a>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Pages</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="#">Page 1</a>
                    </li>
                    <li>
                        <a href="#">Page 2</a>
                    </li>
                    <li>
                        <a href="#">Page 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Portfolio</a>
            </li>
            <li>
                <a href="#">Contact</a>
            </li>
        </ul>
    </nav>
    <nav class="navbar navbar-expand-lg navbar-light">
        <button type="button" id="sidebarCollapse" class="btn btn-navbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="header-logo">
            <a href="http://moovtoo.com/dev">
                <img src="{{asset('images/logo-moovtoo.svg')}}">
            </a>
        </div>
        @if ( $current_country != 'gg')
        @foreach($countries as $country)
            @if ( strtolower($country->code) == strtolower($current_country) and is_array($country->locales) and sizeof($country->locales)>1)
                <div class="pull-right header-login visible-xs">
                    <div class="country-dropdown header-country">
                        <span><a href="javascript:void(0)" > {{strtoupper($current_locale)}} <img src="{{asset('images/translate.svg')}}" class="header-country-image"/></a></span>
                        <div class="country-dropdown-content">
                            @foreach( $country->locales as $locale )
                                <p><a href="{{route('locale.change',strtolower($locale->code))}}" @if(strtoupper($current_locale) == strtoupper($locale->code)) class="active" @endif > {{$locale->name}}</a></p>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        @endif
        <div class="pull-right header-login visible-xs">
            <div class="country-dropdown header-country">
                <span><a href="javascript:void(0)" >  @if (strtolower($current_country)=='gg') {{'GLOBAL'}}@else{{strtoupper($current_country)}}@endif <img src="{{asset('images/icons/country.svg')}}" class="header-country-image"/></a></span>
                <div class="country-dropdown-content">
                    @foreach( $countries as $country )
                        <p><a href="{{route('country.change',strtolower($country->code))}}" @if(strtoupper($current_country) == strtoupper($country->code)) class="active" @endif > {{$country->name}}</a></p>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="pull-right header-login hidden-xs">
            @if ( strtolower($current_country) != 'gg')
            @foreach($countries as $country)
                @if (strtolower($country->code) == strtolower($current_country) and is_array($country->locales) and sizeof($country->locales)>1)
                    <div class="country-dropdown header-country">
                        <span><a href="javascript:void(0)" > {{strtoupper($current_locale)}} <img src="{{asset('images/translate.svg')}}" class="header-country-image"/></a></span>
                        <div class="country-dropdown-content">
                            @foreach( $country->locales as $locale )
                                <p><a href="{{route('locale.change',strtolower($locale->code))}}" @if(strtoupper($current_locale) == strtoupper($locale->code)) class="active" @endif > {{$locale->name}}</a></p>
                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach
            @endif
            <div class="country-dropdown header-country">
                <span><a href="javascript:void(0)" > @if (strtolower($current_country)=='gg') {{'GLOBAL'}}@else{{strtoupper($current_country)}}@endif <img src="{{asset('images/icons/country.svg')}}" class="header-country-image"/></a></span>
                <div class="country-dropdown-content">
                    @foreach( $countries as $country )
                        <p><a href="{{route('country.change',strtoupper($country->code))}}" @if(strtoupper($current_country) == strtoupper($country->code)) class="active" @endif > {{$country->name}}</a></p>
                    @endforeach
                </div>
            </div>

            @if(getUser() != null)
                <div class="user-dropdown">
                    <span>Welcome, {{getUser()['name']}}</span>
                    <div class="user-dropdown-list">
                        <p><a href="{{route('profile')}}">Profile</a></p>
                        <p><a href="{{route('logout')}}">Log out</a></p>
                    </div>
                </div>
            @else
                <a href="{{route('login')}}">Login</a>
                <span class="seperator">|</span>
                <a href="{{route('login')}}">Register</a>
            @endif
        </div>
    </nav>
</header>
