<div class="home-slider">
    <div class="slider">
        @foreach ($banners as $banner)
            <img src="{{'http://cms.moovtoo.com/public/'.$banner->photo}}">
        {{--<img src="{{asset('images/Guide.jpg')}}" >--}}
        @endforeach
    </div>
    <div class="slider-search-box">
        <div class="slider-mid">
            <div class="guide-slider-title">{{alias('Make your')}}<span>{{alias('Selection')}}</span></div>
            <div class="guide-slider-btns">
            <div class="choose-guide-theme"> {{alias('Choose a Theme')}}  <i class="fa fa-angle-right"></i></div>
            <div class="choose-guide-location">  {{alias('Choose a Location')}} <i class="fa fa-angle-right"></i></div>
            </div>
            <div class="guide-slider-tabs">
                <div class="guide-group-tabs">
                    <div class="guide-tab-details active">{{alias('Top Theme')}}</div>
                    <div class="guide-tab-details">Kids & Family</div>
                    <div class="guide-tab-details">Bobo</div>
                    <div class="guide-tab-details">Gastronomie</div>
                </div>
                <div class="seperator hidden-xs">|</div>
                <div class="guide-group-tabs">
                    <div class="guide-tab-details active">{{alias('Top Places')}}</div>
                    <div class="guide-tab-details">le Marais</div>
                    <div class="guide-tab-details">Tour Eiffel</div>
                    <div class="guide-tab-details">Louvres</div>
                    <div class="guide-tab-details">Notre Dame</div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="slider-search-box">--}}
        {{--<div class="slider-mid slider-mid-guide">--}}
            {{--<div class="slider-title">--}}
                {{--<img src="{{asset('images/food.png')}}">--}}
            {{--</div>--}}
            {{--<div class=" slider-title-guide">--}}
                {{--<select class="select2" >--}}
                    {{--FOOD & DRINKS--}}
                {{--</select>--}}
            {{--</div>--}}
            {{--<div class="slider-search-guide">--}}
                {{--<div class="search-select">--}}
                    {{--<select class="select2 ">--}}
                        {{--<option><img src="{{asset('images/food.png')}}">FOOD & DRINKS</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                    {{--<div class="autocomplete">--}}
                        {{--<input   class="search-input" id="myInput" type="text" name="myCountry" placeholder="Burger, pizza, coctails, desserts ...">--}}
                    {{--</div>--}}
                {{--<div class="autocomplete2" style="border-left: solid 1px #e2e2e2e2">--}}
                       {{--<label>{{alias('Location')}}</label> <input   class="search-input" id="myInput2" type="text" name="myCountry" placeholder="Paris">--}}
                    {{--</div>--}}

                {{--<input class="search-input" type="text" name="q" id="q" placeholder="Search">--}}
               {{--<div class="search-i">--}}
                   {{--<i class="fa fa-search"></i>--}}
               {{--</div>--}}
            {{--</div>--}}
            {{--<div class="slider-top-searches">--}}
                {{--<span class="top-searches">{{alias('Top Searches')}}: </span>--}}
                {{--<span class="top-search">Burger</span>--}}
                {{--<span class="top-search">Portugese</span>--}}
                {{--<span class="top-search">Lebanese</span>--}}
                {{--<span class="top-search">Dessert</span>--}}
                {{--<span class="top-search">Crepes</span>--}}
                {{--<span class="top-search">Cocktails</span>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="slider-overlay">--}}
        {{--<img src="{{asset('images/slider-overlay.png')}}">--}}
    {{--</div>--}}
</div>
<script>
    $('.slider').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: false,
        nextArrow: false,
        // cssEase: 'fade-out',
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });
</script>
@push('js')
    {{--<script>--}}
        {{--function autocomplete(inp, arr) {--}}
            {{--/*the autocomplete function takes two arguments,--}}
            {{--the text field element and an array of possible autocompleted values:*/--}}
            {{--var currentFocus;--}}
            {{--/*execute a function when someone writes in the text field:*/--}}
            {{--inp.addEventListener("input", function(e) {--}}
                {{--var a, b, i, val = this.value;--}}
                {{--/*close any already open lists of autocompleted values*/--}}
                {{--closeAllLists();--}}
                {{--if (!val) { return false;}--}}
                {{--currentFocus = -1;--}}
                {{--/*create a DIV element that will contain the items (values):*/--}}
                {{--a = document.createElement("DIV");--}}
                {{--a.setAttribute("id", this.id + "autocomplete-list");--}}
                {{--a.setAttribute("class", "autocomplete-items");--}}
                {{--/*append the DIV element as a child of the autocomplete container:*/--}}
                {{--this.parentNode.appendChild(a);--}}
                {{--/*for each item in the array...*/--}}
                {{--for (i = 0; i < arr.length; i++) {--}}
                    {{--/*check if the item starts with the same letters as the text field value:*/--}}
                    {{--if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {--}}
                        {{--/*create a DIV element for each matching element:*/--}}
                        {{--b = document.createElement("DIV");--}}
                        {{--/*make the matching letters bold:*/--}}
                        {{--b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";--}}
                        {{--b.innerHTML += arr[i].substr(val.length);--}}
                        {{--/*insert a input field that will hold the current array item's value:*/--}}
                        {{--b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";--}}
                        {{--/*execute a function when someone clicks on the item value (DIV element):*/--}}
                        {{--b.addEventListener("click", function(e) {--}}
                            {{--/*insert the value for the autocomplete text field:*/--}}
                            {{--inp.value = this.getElementsByTagName("input")[0].value;--}}
                            {{--/*close the list of autocompleted values,--}}
                            {{--(or any other open lists of autocompleted values:*/--}}
                            {{--closeAllLists();--}}
                        {{--});--}}
                        {{--a.appendChild(b);--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}
            {{--/*execute a function presses a key on the keyboard:*/--}}
            {{--inp.addEventListener("keydown", function(e) {--}}
                {{--var x = document.getElementById(this.id + "autocomplete-list");--}}
                {{--if (x) x = x.getElementsByTagName("div");--}}
                {{--if (e.keyCode == 40) {--}}
                    {{--/*If the arrow DOWN key is pressed,--}}
                    {{--increase the currentFocus variable:*/--}}
                    {{--currentFocus++;--}}
                    {{--/*and and make the current item more visible:*/--}}
                    {{--addActive(x);--}}
                {{--} else if (e.keyCode == 38) { //up--}}
                    {{--/*If the arrow UP key is pressed,--}}
                    {{--decrease the currentFocus variable:*/--}}
                    {{--currentFocus--;--}}
                    {{--/*and and make the current item more visible:*/--}}
                    {{--addActive(x);--}}
                {{--} else if (e.keyCode == 13) {--}}
                    {{--/*If the ENTER key is pressed, prevent the form from being submitted,*/--}}
                    {{--e.preventDefault();--}}
                    {{--if (currentFocus > -1) {--}}
                        {{--/*and simulate a click on the "active" item:*/--}}
                        {{--if (x) x[currentFocus].click();--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}
            {{--function addActive(x) {--}}
                {{--/*a function to classify an item as "active":*/--}}
                {{--if (!x) return false;--}}
                {{--/*start by removing the "active" class on all items:*/--}}
                {{--removeActive(x);--}}
                {{--if (currentFocus >= x.length) currentFocus = 0;--}}
                {{--if (currentFocus < 0) currentFocus = (x.length - 1);--}}
                {{--/*add class "autocomplete-active":*/--}}
                {{--x[currentFocus].classList.add("autocomplete-active");--}}
            {{--}--}}
            {{--function removeActive(x) {--}}
                {{--/*a function to remove the "active" class from all autocomplete items:*/--}}
                {{--for (var i = 0; i < x.length; i++) {--}}
                    {{--x[i].classList.remove("autocomplete-active");--}}
                {{--}--}}
            {{--}--}}
            {{--function closeAllLists(elmnt) {--}}
                {{--/*close all autocomplete lists in the document,--}}
                {{--except the one passed as an argument:*/--}}
                {{--var x = document.getElementsByClassName("autocomplete-items");--}}
                {{--for (var i = 0; i < x.length; i++) {--}}
                    {{--if (elmnt != x[i] && elmnt != inp) {--}}
                        {{--x[i].parentNode.removeChild(x[i]);--}}
                    {{--}--}}
                {{--}--}}
            {{--}--}}
            {{--/*execute a function when someone clicks in the document:*/--}}
            {{--document.addEventListener("click", function (e) {--}}
                {{--closeAllLists(e.target);--}}
            {{--});--}}
        {{--}--}}

        {{--/*An array containing all the country names in the world:*/--}}
        {{--var countries = ["food","food","food","food","food","restu"];--}}

        {{--/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/--}}
        {{--autocomplete(document.getElementById("myInput"), countries);--}}
    {{--</script>--}}
    @endpush