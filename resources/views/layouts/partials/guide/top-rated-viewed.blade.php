<div class="container full-xs-container">
    <div class="row home-box">
        <div class="col-md-6">
            <div class="white-container left">
                <div class="latest-title">
                    {{alias('Latest Articles')}}<div class="underline"></div>
                </div>

                <ul class="top-item-ul">
                    <li class="top-item">
                        <div class="top-item-box">
                            <img src="{{asset('images/black.png')}}">
                            <p>Lebanon has 18 religious communities</p>
                        </div>
                        <div class="item-underline"></div>
                    </li>
                    <li class="top-item">
                        <div class="top-item-box">
                            <img src="{{asset('images/black.png')}}">
                            <p>It has 40 daily newspapers</p>
                        </div>
                        <div class="item-underline"></div>
                    </li>
                    <li class="top-item">
                        <div class="top-item-box">
                            <img src="{{asset('images/black.png')}}">
                            <p>It has 42 universities</p>
                        </div>
                        <div class="item-underline"></div>
                    </li>
                    <li class="top-item">
                        <div class="top-item-box">
                            <img src="{{asset('images/black.png')}}">
                            <p>It has 42 universities</p>
                        </div>
                        <div class="item-underline"></div>
                    </li>
                    <li class="top-item ">
                        <div class="top-item-box ">
                            <img src="{{asset('images/black.png')}}">
                            <p>It has 42 universities</p>
                        </div>
                        <div class="item-underline"></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            <div class="white-container right">
                <div class="latest-title">
                    City facts<div class="underline"></div>
                </div>
                <ul class="top-item-ul">
                <li class="top-item">
                    <div class="top-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>Lebanon has 18 religious communities</p>
                    </div>
                    <div class="item-underline"></div>
                </li>
                <li class="top-item">
                    <div class="top-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>It has 40 daily newspapers</p>
                    </div>
                    <div class="item-underline"></div>
                </li>
                <li class="top-item">
                    <div class="top-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>It has 42 universities</p>
                    </div>
                    <div class="item-underline"></div>
                </li>
                <li class="top-item">
                    <div class="top-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>It has 42 universities</p>
                    </div>
                    <div class="item-underline"></div>
                </li>
                <li class="top-item">
                    <div class="top-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>It has 42 universities</p>
                    </div>
                    <div class="item-underline"></div>
                </li>
                </ul>
            </div>
        </div>
    </div>
</div>