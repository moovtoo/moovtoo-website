<div class="static-banner">
    <div class="banner">
        <img src="{{asset('images/welcome-paris.jpg')}}">
    </div>
    <div class="slider-search-box">
        <div class="slider-mid">
            <div class="static-banner-title">{{alias('Welcome to')}} <strong>Paris</strong></div>
        </div>
    </div>
    <div class="slider-overlay">
        <img src="{{asset('images/slider-overlay.png')}}">
    </div>
</div>