<div class="home-nav-body">
    <div class="row">
        <div class="col-md-12">
            <div class="home-nav hidden-xs">
                <span><a href="javascript:void(0)" class="categories-nav active"> Top attractions</a> </span>
                <span><a href="javascript:void(0)" class="categories-nav"> City guide</a> </span>
                <span><a href="javascript:void(0)" class="categories-nav"> Map</a> </span>
                <span><a href="javascript:void(0)" class="categories-nav"> Media gallery</a> </span>
                <span><a href="javascript:void(0)" class="categories-nav"> Latest articles</a> </span>
                <span><a href="javascript:void(0)" class="categories-nav"> City facts</a> </span>
            </div>
            <div class="home-nav-dropdown visible-xs">
                <div class="nav-dropdown" data-target="guide-nav">
                    <span id="nav-title">{{alias('Categories')}}</span>
                    <span class="dropdown-icon"><i class="fa fa-angle-down"></i></span>
                </div>
                <ul class="nav-dropdown-list" id="guide-nav">
                        <li><a href="javascript:void(0)" class="categories-nav active"> Top attractions</a> </li>
                        <li><a href="javascript:void(0)" class="categories-nav"> City guide</a> </li>
                        <li><a href="javascript:void(0)" class="categories-nav"> Map</a> </li>
                        <li><a href="javascript:void(0)" class="categories-nav"> Media gallery</a> </li>
                        <li><a href="javascript:void(0)" class="categories-nav"> Latest articles</a> </li>
                        <li><a href="javascript:void(0)" class="categories-nav"> City facts</a> </li>
                </ul>
            </div>
        </div>
    </div>
</div>