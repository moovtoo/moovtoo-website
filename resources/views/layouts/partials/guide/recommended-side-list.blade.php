<div class="recommended-guide">
    <div class="recommended-title">
        {{alias('Recommended for you')}}
    </div>
    <div class="row recommended-list">
        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-6">
            <div class="recommended-item">
                <img src="{{asset('images/burger.png')}}">
                <div class="recommended-item-detail">
                    <div class="recommended-item-title">
                        Roadster Diner
                    </div>
                    <div class="recommended-item-address">
                        Achreafieh
                    </div>
                    <div class="recommended-item-info">
                        <strong>Cuisines: </strong>Italian, French
                    </div>
                    <div class="recommended-item-info">
                        <strong>{{alias('Cost for two')}}: </strong>LBP45000
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-6">
            <div class="recommended-item">
                <img src="{{asset('images/burger.png')}}">
                <div class="recommended-item-detail">
                    <div class="recommended-item-title">
                        Roadster Diner
                    </div>
                    <div class="recommended-item-address">
                        Achreafieh
                    </div>
                    <div class="recommended-item-info">
                        <strong>Cuisines: </strong>Italian, French
                    </div>
                    <div class="recommended-item-info">
                        <strong>{{alias('Cost for two')}}: </strong>LBP45000
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>