<div class="row">
    <div class="container ">
        <div class="col-md-8 col-sm-12 sub-category-list">
            <div class="detail">
            <div class="detail-box">
                <div class="item-list-head-detail">
                    <div class="item-list-name">
                        <img src="{{asset('images/star.png')}}">   Chikers <span class="address">achrafiye, jemaize street</span>
                    </div>
                    <div class="float-detail">
                        <div class="item-list-number-detail">
                            <img src="{{asset('images/mobile.png')}}">
                            <span class="number">71687468</span>
                        </div>
                        <div class="item-list-stats-detail">
                            <div class="item-list-rating-detail green">
                                4.8
                            </div>
                            <div class="item-list-open-detail green">
                                Open now
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="detail-pic">
                <img src="{{asset('images/burger-detail.png')}}">
            </div>
            <button class="item-list-menu-btn-detail">
                <img src="{{asset('images/btn-menu.png')}}">{{alias('MENU')}}
            </button>
            </div>
            <div class="detail-data">
                <div class="row detail-padding">
                <div class="col-md-4 ">
                    <div class="detail-title">
                        Cuisines
                    </div>
                    <div class="detail-content active">
                        Italian,French
                    </div>
                    <div class="detail-title">
                        Cost For Two
                    </div>
                    <div class="detail-content">
                        LBP 45000
                    </div>
                    </div>
                    <div class="col-md-8 ">
                        <div class="row detail-title">
                            Opening Hours:
                        </div>
                        <div class="col-md-2 " style="padding: 0">
                            <div class="detail-content">
                                Monday:
                            </div>
                            <div class="detail-content">
                                Tuesday:
                            </div>
                            <div class="detail-content">
                                Wednesday:
                            </div>
                            <div class="detail-content">
                                Thursday:
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="detail-content">
                                12:30 PM to 3:30 PM
                            </div>

                            <div class="detail-content">
                                12:30 PM to 3:30 PM
                            </div>
                            <div class="detail-content">
                                12:30 PM to 3:30 PM
                            </div>
                            <div class="detail-content">
                                12:30 PM to 3:30 PM
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="detail-content">
                                Friday:
                            </div>
                            <div class="detail-content">
                                Saturday:
                            </div>
                            <div class="detail-content">
                                Sunday:
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="detail-content">
                                12:30 PM to 3:30 PM
                            </div>
                            <div class="detail-content">
                                12:30 PM to 3:30 PM
                            </div>
                            <div class="detail-content">
                                12:30 PM to 3:30 PM
                            </div>

                        </div>

                    </div>
                </div>
                <div class="row detail-padding">
                    <div class="col-md-4">
                        <div class="detail-title" >
                            <img src="{{asset('images/info.png')}}">{{alias('More Info')}}
                        </div>
                        <ul>
                            <li class="detail-content">
                                Delivary
                            </li>
                            <li class="detail-content">
                                Valet Parking
                            </li>
                            <li class="detail-content">
                                Table reservation
                            </li>
                            <li class="detail-content">
                                Drive-thru
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-8">
                        <div class="detail-title" >{{alias('Address')}}</div>
                        <div class="detail-content">
                            Achrafieh, Sassine square facing SGBL
                        </div>
                        <div class="detail-title" >
                            <img src="{{asset('images/like.png')}}"> {{alias('Follow us')}}
                        </div>
                        <div class="share-detail">
                            <img src="{{asset('images/facebook.png')}}">
                            <img src="{{asset('images/twitter.png')}}">
                            <img src="{{asset('images/google.png')}}">
                            <img src="{{asset('images/instagram.png')}}">
                            <img src="{{asset('images/youtube.png')}}">
                        </div>


                    </div>

                </div>

            </div>
            <div class="more-photos">
                <div class="row photo-btns">
                    <button class="detail-photo-btn active"> All(345) </button>
                    <button class="detail-photo-btn"> FOOD(124) </button>
                    <button class="detail-photo-btn"> INTERIOR(124) </button>
                    <button class="detail-photo-btn"> OUTDOOR </button>
                </div>
                <div class="detail-galary">
                    <img src="{{asset('images/detail-more-photo.png')}}">
                    <img src="{{asset('images/detail-more-photo.png')}}">
                    <img src="{{asset('images/detail-more-photo.png')}}">
                    <img src="{{asset('images/detail-more-photo.png')}}">
                    <img src="{{asset('images/detail-more-photo.png')}}">
                </div>
                <div class="view-all-photos">
                    <img src="{{asset('images/view-photo.png')}}">{{alias('View All Photos')}}
                </div>
            </div>
            <div class="venue-description">
                {{alias('VENUE DESCRIPTION')}}
            </div>
            <div class="detail-description">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </div>
            <div class="venue-description ">
                {{alias('VENUE REVIEWS')}}
            </div>
            <div class="detail-description">

                <div class="row review-text">
                    <textarea  placeholder="start your review..."></textarea>
                </div>
                <fieldset class="rating">
                    {{--<input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>--}}
                    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                    {{--<input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>--}}
                    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                    {{--<input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>--}}
                    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                    {{--<input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>--}}
                    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                    {{--<input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>--}}
                    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                </fieldset>

            </div>
            <div class="detail-description">
               <div class="row restaurant-detail-cmnts">
                   <img src="{{asset('images/user.png')}}">
                   <div class="restaurant-cmnt-div">
                   <div class="restaurant-cmnt-user-name">
                       Elie Kh
                   </div>
                   <div class="restaurant-cmnt-rating green">
                       4.8
                   </div>
                   </div>
                   <div class="restaurant-hours">
                       10h ago
                   </div>
               </div>
                <div class="row">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </div>
            <div class="detail-description">
                <div class="row restaurant-detail-cmnts">
                    <img src="{{asset('images/user.png')}}">
                    <div class="restaurant-cmnt-div">
                        <div class="restaurant-cmnt-user-name">
                            Elie Kh
                        </div>
                        <div class="restaurant-cmnt-rating green">
                            4.8
                        </div>
                    </div>
                    <div class="restaurant-hours">
                        10h ago
                    </div>
                </div>
                <div class="row">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </div>
            <div class="detail-description">
                <div class="row restaurant-detail-cmnts">
                    <img src="{{asset('images/user.png')}}">
                    <div class="restaurant-cmnt-div">
                        <div class="restaurant-cmnt-user-name">
                            Elie Kh
                        </div>
                        <div class="restaurant-cmnt-rating green">
                            4.8
                        </div>
                    </div>
                    <div class="restaurant-hours">
                        10h ago
                    </div>
                </div>
                <div class="row">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </div>
            <div class="detail-description">
                <div class="row restaurant-detail-cmnts">
                    <img src="{{asset('images/user.png')}}">
                    <div class="restaurant-cmnt-div">
                        <div class="restaurant-cmnt-user-name">
                            Elie Kh
                        </div>
                        <div class="restaurant-cmnt-rating green">
                            4.8
                        </div>
                    </div>
                    <div class="restaurant-hours">
                        10h ago
                    </div>
                </div>
                <div class="row">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </div>
            <div class="detail-description">
                <div class="row restaurant-detail-cmnts">
                    <img src="{{asset('images/user.png')}}">
                    <div class="restaurant-cmnt-div">
                        <div class="restaurant-cmnt-user-name">
                            Elie Kh
                        </div>
                        <div class="restaurant-cmnt-rating green">
                            4.8
                        </div>
                    </div>
                    <div class="restaurant-hours">
                        10h ago
                    </div>
                </div>
                <div class="row">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </div>

        </div>
        <div class="col-md-4 col-sm-12 category-list-side-menu">
            <div class="cat-list-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d167998.10803373056!2d2.206977064368058!3d48.858774103123785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis%2C+France!5e0!3m2!1sen!2slb!4v1549369082990"
                        width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            @include('layouts.partials.guide.recommended-side-list')
        </div>
    </div>
</div>