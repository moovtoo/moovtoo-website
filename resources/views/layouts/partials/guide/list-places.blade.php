@include('layouts.partials.title', ['title'=>$title])
{{--<div class="grey-container">--}}
    <div class="container">
        @include('layouts.partials.guide.tabs')
        <div class="row home-box">
            <div class="col-lg-3 col-md-6 place-div">
                <div class="place-box">
                    <div class="place-image">
                        <img src="{{asset('images/burger.png')}}">
                    </div>
                    <div class="place-info">
                        <div class="place-details">
                            <div class="place-title">
                                Cantina social
                            </div>
                            <div class="place-address">
                                Achrafieh, Tabaris, Jemmayzeh Street
                            </div>
                            <div class="place-category">
                                International Food
                            </div>
                        </div>
                        <div class="place-stats">
                            <div class="place-rating green">
                                4.8
                            </div>
                            <div class="place-open green">
                                Open now
                            </div>
                        </div>
                        <div class="place-price">
                            Average price for two: 45,000 L.L
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 place-div">
                <div class="place-box">
                    <div class="place-image">
                        <img src="{{asset('images/burger.png')}}">
                    </div>
                    <div class="place-info">
                        <div class="place-details">
                            <div class="place-title">
                                Cantina social
                            </div>
                            <div class="place-address">
                                Achrafieh, Tabaris, Jemmayzeh Street
                            </div>
                            <div class="place-category">
                                International Food
                            </div>
                        </div>
                        <div class="place-stats">
                            <div class="place-rating green">
                                4.8
                            </div>
                            <div class="place-open green">
                                Open now
                            </div>
                        </div>
                        <div class="place-price">
                            Average price for two: 45,000 L.L
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 place-div">
                <div class="place-box">
                    <div class="place-image">
                        <img src="{{asset('images/burger.png')}}">
                    </div>
                    <div class="place-info">
                        <div class="place-details">
                            <div class="place-title">
                                Cantina social
                            </div>
                            <div class="place-address">
                                Achrafieh, Tabaris, Jemmayzeh Street
                            </div>
                            <div class="place-category">
                                International Food
                            </div>
                        </div>
                        <div class="place-stats">
                            <div class="place-rating green">
                                4.8
                            </div>
                            <div class="place-open green">
                                Open now
                            </div>
                        </div>
                        <div class="place-price">
                            Average price for two: 45,000 L.L
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 place-div">
                <div class="place-box">
                    <div class="place-image">
                        <img src="{{asset('images/burger.png')}}">
                    </div>
                    <div class="place-info">
                        <div class="place-details">
                            <div class="place-title">
                                Cantina social
                            </div>
                            <div class="place-address">
                                Achrafieh, Tabaris, Jemmayzeh Street
                            </div>
                            <div class="place-category">
                                International Food
                            </div>
                        </div>
                        <div class="place-stats">
                            <div class="place-rating green">
                                4.8
                            </div>
                            <div class="place-open green">
                                Open now
                            </div>
                        </div>
                        <div class="place-price">
                            Average price for two: 45,000 L.L
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--</div>--}}