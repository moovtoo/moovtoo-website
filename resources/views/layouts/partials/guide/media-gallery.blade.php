<div class="container">
    <div class="trip-margin-top">
    </div>
    @include('layouts.partials.title', ['title'=>'Media Gallery'])
    <div class="row home-box guide-box">
        <div class="col-md-8">
            <img src="{{asset('images/media-video.png')}}" class="latest-video">
        </div>
        <div class="col-md-4">
            <div class="latest-videos-side">
                <div class="latest-item">
                    <div class="latest-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>The upside and the downside of living in Beirut during the holidays</p>
                    </div>
                    <div class="item-underline"></div>
                </div>
                <div class="latest-item">
                    <div class="latest-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>The upside and the downside of living in Beirut during the holidays</p>
                    </div>
                    <div class="item-underline"></div>
                </div>
                <div class="latest-item">
                    <div class="latest-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>The upside and the downside of living in Beirut during the holidays</p>
                    </div>
                    <div class="item-underline"></div>
                </div>
                <div class="latest-item">
                    <div class="latest-item-box">
                        <img src="{{asset('images/black.png')}}">
                        <p>The upside and the downside of living in Beirut during the holidays</p>
                    </div>
                    <div class="item-underline"></div>
                </div>
            </div>
        </div>
    </div>
</div>