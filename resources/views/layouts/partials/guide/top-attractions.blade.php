@include('layouts.partials.title', ['title'=>'Top attractions'])
<div class="top-attraction-list">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <div class="attraction-box">
                <img src="{{asset('images/att-1.png')}}" alt="">
                <div class="attraction-over">
                    <img src="{{asset('images/vertical-overlay.png')}}" alt="">
                    <div class="attraction-title">
                        Eiffel Tower
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="attraction-box">
                <img src="{{asset('images/att-2.png')}}" alt="">
                <div class="attraction-over">
                    <img src="{{asset('images/vertical-overlay.png')}}" alt="">
                    <div class="attraction-title">
                        Notre Dame Cathedral
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="attraction-box">
                <img src="{{asset('images/att-3.png')}}" alt="">
                <div class="attraction-over">
                    <img src="{{asset('images/vertical-overlay.png')}}" alt="">
                    <div class="attraction-title">
                        Louvre Museum
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="attraction-box">
                <img src="{{asset('images/att-4.png')}}" alt="">
                <div class="attraction-over">
                    <img src="{{asset('images/vertical-overlay.png')}}" alt="">
                    <div class="attraction-title">
                        Disney Land
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="attraction-box">
                <img src="{{asset('images/att-5.png')}}" alt="">
                <div class="attraction-over">
                    <img src="{{asset('images/vertical-overlay.png')}}" alt="">
                    <div class="attraction-title">
                        Arc de triomphe
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>