<div class="background-white">
@include('layouts.partials.title', ['title'=>'City guide'])
<div class="row">
    <div class="col-md-12 padding-0">
        <img src="{{asset('images/city-guide.png')}}" class="city-guide-background">
    </div>
</div>
<div class="container city-guide-container">
    <div class="row home-box">
        <div class="col-md-12">
            <div class="city-guide-box">
                <div class="row city-guide-buttons hidden-xs">
                    <div class="col-md-2 offset-md-1 col-xs-2">
                        <button class="city-guide-btn active">
                            <img src="{{asset('images/guide/foodDrinks.svg')}}" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <button class="city-guide-btn">
                            <img src="{{asset('images/guide/accomodation.svg')}}" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <button class="city-guide-btn">
                            <img src="{{asset('images/guide/poolBeaches.svg')}}" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <button class="city-guide-btn">
                            <img src="{{asset('images/guide/transportTravel.svg')}}" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <button class="city-guide-btn">
                            <img src="{{asset('images/guide/events.svg')}}" alt="">
                        </button>
                    </div>
                </div>
                <div class="row city-guide-buttons visible-xs">
                    <div class="col-md-12">
                        <div class="nav-dropdown" data-target="city-guide">
                            <span id="nav-title">{{alias('Categories')}}</span>
                            <span class="dropdown-icon"><i class="fa fa-angle-down"></i></span>
                        </div>
                        <ul class="nav-dropdown-list" id="city-guide">
                            <li><a href="javascript:void(0)" class="categories-nav active">{{alias('Food & Drinks')}}</a> </li>
                            <li><a href="javascript:void(0)" class="categories-nav">{{alias('Accomodation')}}</a> </li>
                            <li><a href="javascript:void(0)" class="categories-nav">{{alias('Pools & Beaches')}}</a> </li>
                            <li><a href="javascript:void(0)" class="categories-nav">{{alias('Transport & Travel')}}</a> </li>
                            <li><a href="javascript:void(0)" class="categories-nav">{{alias('Events')}}</a> </li>
                        </ul>
                    </div>
                </div>
                <div class="row city-guide-tab">
                    <div class="col-md-4">
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Pichet
                            </div>
                            <div class="city-guide-item-type">
                                International Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Terrase De paris
                            </div>
                            <div class="city-guide-item-type">
                                French Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Pichet
                            </div>
                            <div class="city-guide-item-type">
                                International Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Terrase De paris
                            </div>
                            <div class="city-guide-item-type">
                                French Food
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Pichet
                            </div>
                            <div class="city-guide-item-type">
                                International Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Terrase De paris
                            </div>
                            <div class="city-guide-item-type">
                                French Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Pichet
                            </div>
                            <div class="city-guide-item-type">
                                International Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Terrase De paris
                            </div>
                            <div class="city-guide-item-type">
                                French Food
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Pichet
                            </div>
                            <div class="city-guide-item-type">
                                International Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Terrase De paris
                            </div>
                            <div class="city-guide-item-type">
                                French Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Pichet
                            </div>
                            <div class="city-guide-item-type">
                                International Food
                            </div>
                        </div>
                        <div class="city-guide-item">
                            <div class="city-guide-item-title">
                                Le Terrase De paris
                            </div>
                            <div class="city-guide-item-type">
                                French Food
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-4">--}}
                        {{--<img src="{{asset('images/city-guide-ad.png')}}" alt="" class="city-guide-ad">--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
</div>