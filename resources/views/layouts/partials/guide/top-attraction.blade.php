<div class="row ">
    <div class="col-md-12 ">
        <div class="trip-suggestion">
            <div class="container">
                <div class="trip-margin-top"></div>
                <div class="trip-suggestion-box">
                    <div class="row">
                        <div class="col-md-12">
                            @include('layouts.partials.title', ['title'=>'Top Attractions'])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="guide-attractions-slider">
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                                <div class="suggestion-box-guide">
                                    <div class="suggestion-box-image-guide">
                                        <img src="{{asset('images/suggestion-image.png')}}" class="img-responsive">
                                        <div class="image-price">800 €</div>
                                    </div>
                                    <div class="suggestion-detail">
                                        <div class="suggestion-detail-title">
                                            Hiking in the Alpes
                                        </div>
                                        <div class="trip-suggestion-detail-div">
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/location.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Paris France</div>
                                            </div>
                                            <div class="suggestion-detail-location">
                                                <div class="suggestion-location-img"><img src="{{asset('images/time.svg')}}" class="img-responsive"></div>
                                                <div class="suggestion-location-name">Mon to Sun from 9.30 - 22.00</div>
                                            </div>


                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="more-suggestions-btn " style="display: block">
                                <a class="more-suggestion-btn" >All activities</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
