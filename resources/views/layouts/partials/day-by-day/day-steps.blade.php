<div class="container ">
    <div class="col-md-12 padding-0">
        <div class="row">
            <div class="steps-data">
                <div class="col-md-1 col-sm-1 col-xs-1 padding-0">
                <div class="step-number ">
                 1
                </div>
                </div>
                <div class="col-md-8 col-sm-8">
                <div class="step-data-time">
                    <img class="day-step-img" src="{{asset('images/day-steps-clock.png')}}">
                    <div class="day-step-time">8.30</div>
                </div>
                <div class="step-data-info">
                    Airport Pick up
                </div>
                </div>
                <div class="step-price">
                    30 Euros
                </div>
            </div>
            <div class="steps-data">
                <div class="col-md-1 col-sm-1 col-xs-1 padding-0">
                <div class="step-number ">
                 2
                </div>
                </div>
                <div class="col-md-8 col-sm-8">
                <div class="step-data-time">
                    <img class="day-step-img" src="{{asset('images/day-steps-clock.png')}}">
                    <div class="day-step-time">9.30</div>
                </div>
                <div class="step-data-info">
                    Arrival at Ritz Hotel      3 nights, 2 adults
                </div>
                </div>
                <div class="step-price">
                    150 Euros
                </div>
            </div>
            <div class="steps-data">
                <div class="col-md-12 col-sm-12 padding-0">
                <div class="col-md-1 col-sm-1 col-xs-1 padding-0 ">
                     <div class="step-number ">
                      3
                     </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-10">
                    <div class="step-data-time hidden-xs">
                    </div>
                    <div class="step-data-info">
                        Lunch time! Choose one out of 3 restaurant suggestions
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 padding-0">
                    <div class="day-steps-detail">
                        <div class="col-md-1 col-sm-1 check-col">
                            <div class="steps-detail-check hidden-xs">
                                <div class="round">
                                        <input type="checkbox" id="checkbox" />
                                        <label for="checkbox"></label>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 padding-0">
                            <div class="col-md-5 col-sm-5 padding-0">
                                <img class="day-step-detail-img" src="{{asset('images/step-restaurant.png')}}">
                                <div class="venue-not-booked visible-xs">Not booked yet</div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="step-restaurant-detail"><span>Type Of Cuisine :</span>Italian, French, International </div>
                                <div class="step-restaurant-detail"><span>Average price per person :</span>30€</div>
                                <div class="step-restaurant-detail"><span>Must try :</span>Pasta a la carbonara, Tiramisu</div>
                                <div class="step-restaurant-detail"><span>Opening hours :</span>Mon to Sun from 9.00 Am - 10:00 PM</div>
                                <div class="step-restaurant-detail"><span>Tips ( movie played at this resto )</span> </div>
                                <div class="more-step-restaurant-detail">> More about the venue</div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <ul class="restaurant-detail-booked hidden-xs">
                                <li>Not booked yet</li>
                            </ul>
                            <div class="restaurant-contact-guide">
                                <img class="restaurant-detail-guide-img" src="{{asset('images/contact-guide.png')}}">
                                <div class="restaurant-contact-guide-name">Contact Erick</div>
                            </div>
                            <div class="restaurant-contact-guide">
                                <img class="restaurant-detail-guide-img" src="{{asset('images/guide-location.png')}}">
                                <div class="restaurant-contact-guide-name">View directions</div>
                            </div>
                            <div class="choose-venue-btn visible-xs">
                                <a class="choose-this-venue-btn" href="#">Choose This Venue</a>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-md-12 col-sm-12 padding-0">
                        <div class="day-steps-detail">
                            <div class="col-md-1 col-sm-1 check-col">
                                <div class="steps-detail-check hidden-xs">
                                    <div class="round">
                                        <input type="checkbox" id="checkbox1" />
                                        <label for="checkbox1"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 padding-0">
                                <div class="col-md-5 col-sm-5 padding-0">
                                    <img class="day-step-detail-img" src="{{asset('images/step-restaurant.png')}}">
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div class="step-restaurant-detail"><span>Type Of Cuisine :</span>Italian, French, International </div>
                                    <div class="step-restaurant-detail"><span>Average price per person :</span>30€</div>
                                    <div class="step-restaurant-detail"><span>Must try :</span>Pasta a la carbonara, Tiramisu</div>
                                    <div class="step-restaurant-detail"><span>Opening hours :</span>Mon to Sun from 9.00 Am - 10:00 PM</div>
                                    <div class="step-restaurant-detail"><span>Tips ( movie played at this resto )</span> </div>
                                    <div class="more-step-restaurant-detail">> More about the venue</div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">

                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/contact-guide.png')}}">
                                    <div class="restaurant-contact-guide-name">Contact Erick</div>
                                </div>
                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/guide-location.png')}}">
                                    <div class="restaurant-contact-guide-name">View directions</div>
                                </div>
                                <div class="choose-venue-btn visible-xs">
                                    <a class="choose-this-venue-btn" href="#">Choose This Venue</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 padding-0">
                        <div class="day-steps-detail">
                            <div class="col-md-1 col-sm-1 check-col">
                                <div class="steps-detail-check hidden-xs">
                                    <div class="round">
                                        <input type="checkbox" id="checkbox2" />
                                        <label for="checkbox2"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 padding-0">
                                <div class="col-md-5 col-sm-5 padding-0">
                                    <img class="day-step-detail-img" src="{{asset('images/step-restaurant.png')}}">
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div class="step-restaurant-detail"><span>Type Of Cuisine :</span>Italian, French, International </div>
                                    <div class="step-restaurant-detail"><span>Average price per person :</span>30€</div>
                                    <div class="step-restaurant-detail"><span>Must try :</span>Pasta a la carbonara, Tiramisu</div>
                                    <div class="step-restaurant-detail"><span>Opening hours :</span>Mon to Sun from 9.00 Am - 10:00 PM</div>
                                    <div class="step-restaurant-detail"><span>Tips ( movie played at this resto )</span> </div>
                                    <div class="more-step-restaurant-detail">> More about the venue</div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/contact-guide.png')}}">
                                    <div class="restaurant-contact-guide-name">Contact Erick</div>
                                </div>
                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/guide-location.png')}}">
                                    <div class="restaurant-contact-guide-name">View directions</div>
                                </div>
                                <div class="choose-venue-btn visible-xs">
                                    <a class="choose-this-venue-btn" href="#">Choose This Venue</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="steps-data">
                <div class="col-md-12 col-sm-12 padding-0">
                    <div class="col-md-1 col-sm-1 col-xs-1 padding-0">
                        <div class="step-number ">
                            4
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10">
                        <div class="step-data-time hidden-xs">
                        </div>
                        <div class="step-data-info">
                            Take a walk in the downtown park
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 padding-0">
                        <div class="day-steps-detail">
                            <div class="col-md-1 col-sm-1 check-col">
                                <div class="steps-detail-check hidden-xs">
                                    <div class="round">
                                        <input type="checkbox" id="checkbox3" />
                                        <label for="checkbox3"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 padding-0">
                                <div class="col-md-5 col-sm-5 padding-0">
                                    <img class="day-step-detail-img" src="{{asset('images/step-garden.png')}}">
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div class="step-restaurant-detail"><span>Specification :   </span>Kids friendly, picnic friendly</div>
                                    <div class="step-restaurant-detail"><span>Average price per person :   </span>Not entrance fees</div>
                                    <div class="step-restaurant-detail"><span>Categories :   </span>Fauna, Flora</div>
                                    <div class="step-restaurant-detail"><span>Opening hours :   </span>Mon to Sun from 9.00 Am - 10:00 PM</div>
                                    <div class="step-restaurant-detail"><span>Tips: look what is written on the sphere scultpture</span> </div>
                                    <div class="more-step-restaurant-detail">> More about the venue</div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/contact-guide.png')}}">
                                    <div class="restaurant-contact-guide-name">Contact Erick</div>
                                </div>
                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/guide-location.png')}}">
                                    <div class="restaurant-contact-guide-name">View directions</div>
                                </div>
                                <div class="choose-venue-btn visible-xs">
                                    <a class="choose-this-venue-btn" href="#">Choose This Venue</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="steps-data">
                <div class="col-md-12 col-sm-12 padding-0">
                    <div class="col-md-1 col-sm-1 col-xs-1 padding-0">
                        <div class="step-number ">
                            5
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10">
                        <div class="step-data-time hidden-xs">
                        </div>
                        <div class="step-data-info">
                            Take a walk in the downtown park
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 padding-0">
                        <div class="day-steps-detail">
                            <div class="col-md-1 col-sm-1 check-col">
                                <div class="steps-detail-check hidden-xs">
                                    <div class="round">
                                        <input type="checkbox" id="checkbox4" />
                                        <label for="checkbox4"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 padding-0">
                                <div class="col-md-5 col-sm-5 padding-0">
                                    <img class="day-step-detail-img" src="{{asset('images/step-history.png')}}">
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div class="step-restaurant-detail"><span>Average price per person :</span> 10 euros instead of 20 moovtoo deals</div>
                                    <div class="step-restaurant-detail"><span>Opening hours :   </span>Mon to Sun from 9.00 Am - 10:00 PM</div>
                                    <div class="step-restaurant-detail"><span>Tips: First history museum in all Europe</span> </div>
                                    <div class="step-history-detail"><span>Events: 3D world war II projection</span>> More about the venue</div>
                                    <div class="step-restaurant-detail"><span>Saturday 14 May 2019 At 8.00PM </span> </div>
                                    <div class="more-step-restaurant-history">> More about the venue</div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/contact-guide.png')}}">
                                    <div class="restaurant-contact-guide-name">Contact Erick</div>
                                </div>
                                <div class="restaurant-contact-guide">
                                    <img class="restaurant-detail-guide-img" src="{{asset('images/guide-location.png')}}">
                                    <div class="restaurant-contact-guide-name">View directions</div>
                                </div>
                                <div class="choose-venue-btn visible-xs">
                                    <a class="choose-this-venue-btn" href="#">Choose This Venue</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>