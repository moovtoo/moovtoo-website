<div class="home-slider">
    <div class="trip-banner-days">
        <img src="{{asset('images/trip-background.jpg')}}">
    </div>
    <div class="slider-search-box">
        <div class="day-slider-mid-trip">
            <div class="user-profile">
                <img class="day-user-profile-img" src="{{asset('images/user.png')}}">
            </div>
            <div class="banner-suggestion-title">
                Hello John, Erick, your trip manager, check your journey plan
            </div>
            <div class="slider-title-day">{{alias('Welcome To')}} Switzerland</div>
            <div class="day-text-banner">Day 1
                <div class="underline-orange-app"></div></div>
            <div class="day-banner-info">
                <div class="day-banner-detail">Airport pickup</div>
                <div class="day-banner-detail">Accomodation</div>
                <div class="day-banner-detail">2 activities</div>
                <div class="day-banner-detail">3 Restaurants</div>
                <div class="day-banner-detail">Nightlife/drinks</div>
            </div>
        </div>
    </div>
</div>
