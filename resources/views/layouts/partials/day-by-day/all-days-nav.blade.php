<div class="row">
    <div class="col-md-12 padding-0">
        <div class="trip-nav">
            <div class="trip-nav-box">
                <div class="arrow-up-white"></div>
                <div class="row">
                    <div class="col-md-12 display-day-nav white-nav">
                        <div class="col-md-3 offset-md-2 col-sm-4  padding-0  day-by-day-title ">
                            <div class="top-title-orange-day">
                                {{alias('Day by day plans')}}
                                <div class="underline-orange-app"></div>
                            </div>
                        </div>
                        <div class="day-nav-boxes">
                            <a href="{{route('day.day-one')}}" class="day-one-box">14 <div class="day-name">SAT</div></a>
                            <div class="day-one-box">15 <div class="day-name">SUN</div></div>
                            <div class="day-one-box">16 <div class="day-name">MON</div></div>
                            <div class="day-all-box active">FULL <div class="day-name">DAYS</div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

