<div class="container padding-0">
    <div class="row">
        <div class="col-md-12">
            <div class="days-info-box">
            <div class="day-text-info">Day 1
                <div class="underline-orange-app"></div>
            </div>
            <div class="info-title-day">SATURDAY 14 MARCH 2019</div>
                <div class="all-days-tabs-info">
                    <div class="day-banner-detail">Airport pickup</div>
                    <div class="day-banner-detail">Accomodation</div>
                    <div class="day-banner-detail">2 activities</div>
                    <div class="day-banner-detail">3 Restaurants</div>
                    <div class="day-banner-detail">Nightlife/drinks</div>
                </div>
            </div>
            <div class="days-info-box">
                <div class="day-text-info">Day 2
                    <div class="underline-orange-app"></div>
                </div>
                <div class="info-title-day">SUNDAY 15 MARCH 2019</div>
                <div class="all-days-tabs-info">
                    <div class="day-banner-detail">Airport pickup</div>
                    <div class="day-banner-detail">Accomodation</div>
                    <div class="day-banner-detail">2 activities</div>
                    <div class="day-banner-detail">3 Restaurants</div>
                    <div class="day-banner-detail">Nightlife/drinks</div>
                </div>
            </div>
            <div class="days-info-box">
                <div class="day-text-info">Day 3
                    <div class="underline-orange-app"></div>
                </div>
                <div class="info-title-day">MONDAY 16 MARCH 2019</div>
                <div class="all-days-tabs-info">
                    <div class="day-banner-detail">Airport pickup</div>
                    <div class="day-banner-detail">Accomodation</div>
                    <div class="day-banner-detail">2 activities</div>
                    <div class="day-banner-detail">3 Restaurants</div>
                    <div class="day-banner-detail">Nightlife/drinks</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sum-all-days">
            <div class="total-fees">TOTAL: 1250 Euros</div>
        </div>
    </div>
    <div class="row">
        <div class="all-days-booking-btns">
            <a>Thank you, i will  manage my trip alone</a>
            <a>Let moovtoo book everything</a>
            <!-- Trigger/Open The Modal -->
            <a id="myBtn">This is not what i had in mind</a>
        </div>


        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close" style="color: black">&times;</span>
                    {{--<h2>Modal Header</h2>--}}
                </div>
                <div class="modal-body">
                    <div class="popup-user-profile">
                        <img class="day-user-profile-img" src="{{asset('images/user.png')}}">
                    </div>
                    <div class="popup-suggestion-title">
                        Hello John, Erick, your trip manager
                    </div>
                    <div class="popup-title-day">What can i propose to you instead? </div>
                    <div class="propose-msg">
                        <textarea id="myTextarea" rows="3" placeholder="- What exactly are you looking for?&#10;- What you didn’t like?"></textarea>
                    </div>
                    <div class="send-msg-btn">
                        <a class="send-msg-btn">Send Message</a>
                    </div>
                </div>

            </div>

    </div>
</div>
</div>
@push('js')
    <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>

@endpush
