<div class="mag-container container article-container">
    <div class="row home-box mag-box">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9">
                    <div class="post-detail-div">
                        <div class="post-detail-category">
                            {{-- @if($article->categories) {{$article->categories}} @endif --}}
                            @foreach ($article->categories as $category)
                                @if($category->parent_id != null)
                                    {{$category->name}}
                                @endif
                            @endforeach
                        </div>
                        <div class="post-detail-title">
                            {{$article->name}}
                        </div>
                        <div class="post-detail-date">
                            {{($article->city)? $article->city.' | '.$article->date_created : $article->date_created}}
                        </div>
                        <div class="post-detail-user">
                            <div class="post-user-detail-box">
                                @if($article->users[0]->photo)
                                    <img src="http://cms.moovtoo.com/public/{{$article->users[0]->photo}}">
                                @else
                                    <img src="{{asset('images/avatar.jpg')}}">
                                @endif
                                <div class="post-user-detail">
                                    <div class="post-user-name">
                                        {{$article->users[0]->name}}
                                    </div>
                                    <div class="post-user-position">
                                        {{$article->users[0]->position}}
                                    </div>
                                </div>
                            </div>
                            <div class="post-user-social-media">
                                <div class="post-user-social-title">
                                    Share
                                </div>
                                <div class="post-user-social-icons">
                                    <img src="{{asset('images/smi.svg')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="post-detail-div">
                    <div class="post-detail-short">
                        {!!$article->description!!}
                    </div>
                    <div class="post-detail-image">
                        <img src="http://cms.moovtoo.com/public/{{$article->photo}}">
                        <span class="article-caption">{{($article->photo_caption)? $article->photo_caption.'.' : ''}}</span>
                        <span class="article-photographer">{{($article->photographer)? 'Credit: '.$article->photographer->name : ''}}</span>
                    </div>
                    <div class="post-detail-description">
                        {!! $article->content !!}
                    </div>
                    <div class="post-detail-comments">
                        <div class="post-detail-comments-title">{{alias('Comment')}}</div>
                        <div class="post-detail-comment">
                            <img src="{{asset('images/user1.png')}}">
                            {{-- <input type="text" placeholder="Write a comment"> --}}
                            <textarea placeholder="Write a comment..."></textarea>
                        </div>
                        <div class="post-detail-comment-btn">
                            <button>Post</button>
                        </div>
                    </div>
                    <div class="mag-list-sidebar top-20 visible-xs">
                        <div class="shop-title">
                            {{alias('Articles Similarities')}}
                        <div class="underline"></div>
                        </div>
                        <div class="mag-sidebar-list list-similar-xs">
                            <div class="col-md-12">
                                @foreach ($res2->data as $art)
                                    @if($loop->index < 3)
                                        <div class="sidebar-article">
                                            @if($art->categories)
                                            <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>$art->categories[0]->slug ])}}">
                                            @else
                                            <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>'Unknown' ])}}">
                                            @endif
                                                <img src="http://cms.moovtoo.com/public/{{$art->photo}}">
                                            </a>
                                            <p>
                                                @if($art->categories)
                                                    <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>$art->categories[0]->slug ])}}">
                                                @else
                                                    <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>'Unknown' ])}}">
                                                @endif
                                                    {{$art->name}}
                                                </a>
                                            </p>
                                        </div>
                                    @endif
                                @endforeach
                                {{-- <div class="middle-btn">
                                    <button class="white-btn">Voir tous les articles</button>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    {{-- @include('layouts.partials.mag.sign-up') --}}
                    <div class="post-detail-video">
                        {{-- <div class="post-detail-video-title">À voir également sur moovtoo:</div> --}}
                        <div class="shop-title">
                                À voir également sur moovtoo
                            <div class="underline"></div>
                        </div>
                        <img src="{{asset('images/post-youtube.png')}}">
                    </div>
                    @include('layouts.partials.mag.shop-items')
                    <div class="post-detail-moovtoo row">
                        <div class="col-md-2">
                            <div class="moovtoo-vip-img">
                                <img src="{{asset('images/moovtoo.png')}}">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="moovtoo-vip-title">
                                Organise ton voyage a la Bourgogne avec moovtoo
                            </div>
                            <div class="moovtoo-vip-list">
                                <ul>
                                    <li>Guides Bilingues</li>
                                    <li>Accomadation assurees</li>
                                    <li>Assurance de voyage</li>
                                </ul>
                                <ul>
                                    <li>Voitures climatisees</li>
                                    <li>Payement en ligne</li>
                                    <li>Des prix competitifs</li>
                                </ul>
                                <button>En savoir +</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 main-side-menu">
                @include('layouts.partials.side-advertisement', ['name'=>'test'])
                <div class="col-md-12 guide-sidemenu">
                    <a href="{{route('city-guide')}}">
                        <img src="{{asset('images/guide-box.png')}}" class="ad-image img-responsive">
                    </a>
                </div>
                <div class="col-md-12 hidden-xs">
                    <div class="mag-list-sidebar top-20 outline">
                        <div class="title-sidebar">Articles similaires</div>
                        <div class="mag-sidebar-list">
                            <div class="col-md-12">
                                @foreach ($res2->data as $art)
                                    @if($loop->index < 3)
                                    <div class="sidebar-article">
                                        @if($art->categories)
                                            <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>$art->categories[0]->slug ])}}">
                                        @else
                                            <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>'Unknown' ])}}">
                                        @endif
                                                <img src="http://cms.moovtoo.com/public/{{$art->photo}}">
                                            </a>
                                            <p>
                                                @if($art->categories)
                                                    <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>$art->categories[0]->slug ])}}">
                                                @else
                                                    <a href="{{route('blog.article.detail', ['slug'=>$art->slug, 'category'=>'Unknown' ])}}">
                                                @endif
                                                    {{$art->name}}
                                                </a>
                                            </p>
                                        </div>
                                    @endif
                                @endforeach
                                {{-- <div class="middle-btn">
                                    <button class="white-btn">Voir tous les articles</button>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.title = "{{$article->name}}"+" - "+"{{ config('app.name', 'Laravel') }}";
</script>