@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fafafb;
        }
    </style>
@endpush

@section('content')
    @include('layouts.partials.header')
{{--    @include('layouts.partials.trip.slider')--}}
    <section id="ajax-trip-slider">
    </section>
    <section id="ajax-trip-how-it-works">
    </section>
    <section id="ajax-trip-suggestion">
    </section>
    @include('layouts.partials.trip.happy-travel')
    @include('layouts.partials.trip.why-plan')
    @include('layouts.partials.trip.payment')
    @include('layouts.partials.trip.travel-tips')
    @include('layouts.partials.trip.download-app')

@endsection

@push('js')
    <script>
        $(function(){
            callAjax('trip-how-it-works', 'ajax-trip-how-it-works');
            callAjax('trip-slider', 'ajax-trip-slider');
            callAjax('trip-suggestion', 'ajax-trip-suggestion', '?title=Find your perfect trip');
        })
    </script>
@endpush