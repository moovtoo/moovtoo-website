@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: white;
        }
    </style>
@endpush

@section('content')
    @include('layouts.partials.header')
    @include('layouts.partials.trip.payment-banner')
    @include('layouts.partials.trip.payment-nav')


@endsection

@push('js')
    <script>
        $(function(){
            $('.select-create-trip .select2-selection__arrow b').html('<i class="fa fa-angle-down"></i>');
            $('.top-title-orange').hide();
        })
    </script>
@endpush


