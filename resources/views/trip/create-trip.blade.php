@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: white;
        }
    </style>
@endpush

@section('content')
    @isset($travelLocation)
        @include('layouts.partials.header')
        @include('layouts.partials.trip.trip-banner',['travelLocation'=>$travelLocation])
        @include('layouts.partials.trip.create-trip-nav',['travelLocation'=>$travelLocation])
        @include('layouts.partials.trip.trip-suggestion',['travelLocation'=>$travelLocation])
        @include('layouts.partials.trip.happy-travel')
        @include('layouts.partials.trip.travel-tips')
    @else
        @include('layouts.partials.header')
        @include('layouts.partials.trip.trip-banner')
        @include('layouts.partials.trip.create-trip-nav')
        @include('layouts.partials.trip.trip-suggestion')
        @include('layouts.partials.trip.happy-travel')
        @include('layouts.partials.trip.travel-tips')
    @endisset

@endsection

@push('js')
    <script>
        $(function(){
            $('.select-create-trip .select2-selection__arrow b').html('<i class="fa fa-angle-down"></i>');
            $('.arrow-down').hide();
        })
    </script>
@endpush