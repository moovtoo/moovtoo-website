@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: white;
        }
    </style>
@endpush

@section('content')
    @include('layouts.partials.header')
    @include('layouts.partials.trip.trip-banner-no-search')
    @include('layouts.partials.trip.question-nav-trip')


@endsection

@push('js')
    <script>
        $(function(){
            $('.select-create-trip .select2-selection__arrow b').html('<i class="fa fa-angle-down"></i>');
            $('.arrow-down').hide();
        })
    </script>
@endpush


