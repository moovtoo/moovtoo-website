@extends('layouts.app')
@push('css')
    <style>
        .arrow-down{
         display: none;
         }
        .more-suggestions-btn {
            display: block;
        }
    </style>
    @endpush
@section('content')

    {{--@include('layouts.partials.home-slider')--}}
    <section id="ajax-home-slider">

    </section>
    @include('layouts.partials.where-to')
    @include('layouts.partials.home-nav')
    <section id="ajax-trip-suggestion">
    </section>
    {{--@include('layouts.partials.concierge-services')--}}
    @include('layouts.partials.guide')
    @include('layouts.partials.trip.some-fun')
    {{--@include('layouts.partials.disconnect-slider')--}}
    @include('layouts.partials.advertisement', ['background'=>'grey', 'name'=>'test'])
    <section id="ajax-latest-from">

    </section>
    @include('layouts.partials.agenda')
    @include('layouts.partials.advertisement', ['name'=>'test'])
    @include('layouts.partials.community')
    @include('layouts.partials.advertisement', ['name'=>'test'])
@endsection


@push('js')
    <script>
        $('#where-form input').keydown(function(e) {
            if (e.keyCode == 13) {
                $('#where-form').submit();
            }
        });

        if ($(".home-nav")[0]) {
            var HomenavHeaderTop;

            setTimeout(function () {
                $('.home-slider').ready(function(){
                    HomenavHeaderTop = $('.home-nav').parent().parent().offset().top ;
                    console.log(HomenavHeaderTop);
                });
            },8000);

            $(window).scroll(function () {
                if ($(document).scrollTop() > HomenavHeaderTop - 50) {
                    $(".home-nav").addClass("fixed");
                    // $('body').css('padding-top', '104px');
                } else {
                    $(".home-nav").removeClass("fixed");
                    // $('body').css('padding-top', '50px');
                }
            });
        }</script>
    <script>
        $(function(){
            // callAjax('trip-how-it-works', 'ajax-trip-how-it-works');
            callAjax('trip-suggestion', 'ajax-trip-suggestion', '?title=Trip Packages');
            callAjax('latest-from', 'ajax-latest-from');
            callAjax('home-slider', 'ajax-home-slider');
            callAjax('where-to', 'ajax-where-to');

        })
    </script>

@endpush
