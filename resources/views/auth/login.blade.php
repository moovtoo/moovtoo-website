<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.favicon', '')) }}"> --}}
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("images/fav.png") }}">

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.favicon', '')) }}"> --}}
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("images/fav.png") }}">
    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <!-- Our Custom CSS -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    @stack('css')
</head>
<body>
    <header class="login">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="header-logo">
                <a href="{{route('home')}}">
                    <span>Welcome to </span>
                    <img src="{{asset('images/logo-black.png')}}">
                </a>
            </div>
        </nav>
    </header>

    <div class="container login-container">
        <div class="row">
            <div class="col-md-5">
                <div class="login-box">
                    <div class="login-title">
                        {{alias('Already a user?')}}
                        <span>{{alias('Sign in')}}</span>
                    </div>
                    <div class="login-page-form">
                        <div class="error-message" id="login-error">
                        </div>
                        {{--<form method="POST" action="{{route('customLogin')}}">--}}
                            {{-- @csrf --}}
                            <div class="login-input">
                                <input type="text" name="login_email" id="login_email" placeholder="{{alias('Email')}}" value="{{ old('email') }}">
                                <span class="help-block" id="help-login_email"></span>
                            </div>
                            <div class="login-input">
                                <input type="password" name="login_password" id="login_password" class="login-input" placeholder="{{alias('Password')}}">
                                <span class="help-block" id="help-login_password"></span>
                            </div>
                            <div class="login-forget">
                                <a href="javascript:void(0)" class="orange-link">
                                    {{alias('Forget Password?')}}
                                </a>
                            </div>
                            <button onclick="login()" class="login-btn">
                                {{alias('Sign in')}}
                            </button>
                        {{--</form>--}}

                    </div>
                    <div class="text-center-orange">
                        {{alias('Or')}}
                    </div>
                    <div class="social-login">
                        <button class="btn-login-social facebook" data-id="facebook">
                            <div class="btn-social-img">
                                <img src="{{asset('images/facebook-btn.png')}}">
                            </div>
                            <div class="btn-social-text">
                                {{alias('Sign in with Facebook')}}
                            </div>
                        </button>
                        <button class="btn-login-social google" data-id="google">
                            <div class="btn-social-img">
                                <img src="{{asset('images/google-btn.png')}}">
                            </div>
                            <div class="btn-social-text">
                                {{alias('Sign in with Gmail')}}
                            </div>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="cirlce-text">
                    {{alias('Or')}}
                </div>
            </div>
            <div class="col-md-5">
                <div class="login-box">
                    <div class="login-title">
                        {{alias('New user?')}}
                        <span>{{alias('Sign up')}}</span>
                    </div>
                    <div class="login-page-form">
                        <div class="success-message" id="register-success">
                        </div>
                        <div class="error-message" id="register-error">
                        </div>
                        {{-- <form action=""> --}}
                            <div class="login-radio">
                                <label class="label-inline">
                                    {{alias('You are')}}
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" id="register_you_are" name="you_are" value="person">{{alias('A Person')}}
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" id="register_you_are" name="you_are" value="company">{{alias('A Company')}}
                                </label>
                                <span class="help-block" id="help-register_you_are"></span>
                            </div>
                            <div class="login-input">
                                <input type="text" id="register_name" name="name" placeholder="{{alias('Full Name')}}">
                                <span class="help-block" id="help-register_name"></span>
                            </div>
                            <div class="login-input">
                                <input type="text" id="register_email" name="email" placeholder="{{alias('Email')}}">
                                <span class="help-block" id="help-register_email"></span>
                            </div>
                            <div class="login-input">
                                <input type="password" id="register_password" name="password" placeholder="{{alias('Password')}}">
                                <span class="help-block" id="help-register_password"></span>
                            </div>
                            <div class="login-input">
                                <input type="password" id="register_password_confirmation" name="password_confirmation" placeholder="{{alias('Confirm Password')}}">
                                <span class="help-block" id="help-register_password_confirmation"></span>
                            </div>
                            <div class="login-input">
                                <select name="country_id" id="register_country_id" class="select2">
                                    <option value="">{{alias('Select Country')}}</option>
                                    @foreach ($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                                <span class="help-block" id="help-register_country_id"></span>
                            </div>
                            <div class="login-input">
                                <input type="tel" id="register_phone" name="phone" placeholder="{{alias('Phone Number')}}">
                                <span class="help-block" id="help-register_phone"></span>
                            </div>
                            <button onclick="register()" class="login-btn">
                                {{alias('Register')}}
                            </button>
                            <div class="login-checkbox">
                                <input type="checkbox" name="policy">
                                <label>{{alias('I accept the')}} <a href="javascript:void(0)" class="orange-link underline">{{alias('general terms and conditions of use')}}</a></label>
                            </div>
                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="ajax-footer">

    </section>


    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script type="text/javascript">
        function callAjax(view, id, params =null){
            if(params != null){
                url = '{{route("ajax.view")}}/'+view+params;
            }
            else{
                url = '{{route("ajax.view")}}/'+view;
            }
            $.ajax({
                type: "GET",
                url: url,
                success: function(data)
                {
                    $('#'+id).html(data);
                },
                error:function( data){
                    // console.log(data);
                }
            });
        }

        $(function(){
            
            callAjax('footer', 'ajax-footer');

            $('.btn-login-social').on('click', function(){
                provider = $(this).attr('data-id');
                // $.ajax({
                //     type: 'GET',
                //     url: 'http://moovtoo.com/login/provider/'+provider,
                //     data: {},
                //     dataType:'json',

                // }).done(function (result) {
                //     console.log(result);
                // }).fail(function (result) {
                //     console.log(result);
                // });
                location.href= 'https://moovtoo.com/login/provider/'+provider;
            })

            $.fn.visible = function(partial) {

                var $t            = $(this),
                    $w            = $(window),
                    viewTop       = $w.scrollTop(),
                    viewBottom    = viewTop + $w.height(),
                    _top          = $t.offset().top,
                    _bottom       = _top + $t.height(),
                    compareTop    = partial === true ? _bottom : _top,
                    compareBottom = partial === true ? _top : _bottom;

                return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

            };
        })

        $(document).ready(function () {
            $('.select2').select2({
                width:'100%'
            });
        });

        $(window).on("scroll", function() {
            if($(window).scrollTop() > 50) {
                $("header").addClass("active");
            } else {
                $("header").removeClass("active");
            }
        });
        
        function login(){
            var password =  $('#login_password').val(),
                email = $('#login_email').val();


            var error = false;

            if(password == ''){
                $('#help-login_password').html('Champ Requis');
                error = true;
            }else{
                $('#help-login_password').html('');
            }if(email == ''){
                $('#help-login_email').html('Champ Requis');
                error = true;
            }else{
                $('#help-login_email').html('');
            }

            if(!error){
                $.ajax({
                    type: 'POST',
                    url: 'http://cms.moovtoo.com/api/login',
                    data: {
                        email : email,
                        password : password
                    },
                    dataType:'json',

                }).done(function (result) {
                    if(result.success == 'Success'){
                        $.ajax({
                            type: 'POST',
                            url: '{{route('session.post')}}',
                            data: result,
                            dataType:'json',

                        }).done(function (result) {
                            location.href = '{{route('home')}}'

                        }).fail(function (data) {
                            console.log(data)
                        });
                    }

                }).fail(function (result) {
                    $('#login-error').html(result.responseJSON['error']);
                    $('#login-error').show();
                });
            }
        }
        function register(){
            var password =  $('#register_password').val(),
                email = $('#register_email').val(),
                name = $('#register_name').val(),
                phone = $('#register_phone').val(),
                you_are = $('#register_you_are').val(),
                password2 = $('#register_password_confirmation').val(),
                country = $('#register_country_id').val();


            var error = false;

            if(password == ''){
                $('#help-register_password').html('Champ Requis');
                error = true;
            }else{
                $('#help-register_password').html('');
            }
            if(password2 == ""){
                $('#help-register_password_confirmation').html('Champ Requis');
            }else if(password != password2){
                $('#help-register_password_confirmation').html('Passwords do not match!');
            }else{
                $('#help-register_password_confirmation').html('');
            }
            if(email == ''){
                $('#help-register_email').html('Champ Requis');
                error = true;
            }else{
                $('#help-register_email').html('');
            }
            if(phone == ''){
                $('#help-register_phone').html('Champ Requis');
                error = true;
            }else{
                $('#help-register_phone').html('');
            }
            if(you_are == ''){
                $('#help-register_you_are').html('Champ Requis');
                error = true;
            }else{
                $('#help-register_phone_you_are').html('');
            }
            if(country == ''){
                $('#help-register_country_id').html('Champ Requis');
                error = true;
            }else{
                $('#help-register_phone_country_id').html('');
            }

            if(!error){
                $.ajax({
                    type: 'POST',
                    url: 'http://cms.moovtoo.com/api/register',
                    data: {
                        email : email,
                        password : password,
                        password_confirmation: password2,
                        phone: phone,
                        business_type: you_are,
                        country_id: country,
                        name: name
                    },
                    dataType:'json',

                }).done(function (result) {
                    if(result.success != null){
                        $('#register-success').html(result.success);
                        $('#register-success').show();
                        $('#register-error').html('');
                        $('#register-error').hide();
                    }

                }).fail(function (data) {
                    $('#register-error').html(data.responseJSON['error']);
                    $('#register-error').show();
                    $('#register-success').html('');
                    $('#register-success').hide();
                });
            }

        }
    </script>

    @stack('js')
</body>
</html>
