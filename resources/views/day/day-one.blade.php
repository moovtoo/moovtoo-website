@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fbfbfb;
        }
    </style>
@endpush

@section('content')
    @include('layouts.partials.header')
    @include('layouts.partials.day-by-day.day-banner')
    @include('layouts.partials.day-by-day.day-nav')
    @include('layouts.partials.day-by-day.map')
    @include('layouts.partials.day-by-day.day-steps')
    @include('layouts.partials.day-by-day.day-sum')

@endsection

@push('js')
<script>   if ($(".display-day-nav")[0]) {
        var stickyHeaderTop;
        $('.home-slider').ready(function(){
            stickyHeaderTop = $('.display-day-nav').parent().parent().offset().top ;
            console.log(stickyHeaderTop);
        });


        $(window).scroll(function () {
            if ($(document).scrollTop() > stickyHeaderTop - 50) {
                $(".display-day-nav").addClass("fixed");
                $('body').css('padding-top', '104px');
            } else {
                $(".display-day-nav").removeClass("fixed");
                $('body').css('padding-top', '50px');
            }
        });
    }</script>
@endpush