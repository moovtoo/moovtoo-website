@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fbfbfb;
        }
    </style>
@endpush

@section('content')
    @include('layouts.partials.header')
    @include('layouts.partials.day-by-day.day-banner')
    @include('layouts.partials.day-by-day.all-days-nav')
    @include('layouts.partials.day-by-day.all-days-info')


@endsection

@push('js')
<script>   if ($(".display-day-nav")[0]) {
        var navHeaderTop;
        $('.home-slider').ready(function(){
            navHeaderTop = $('.display-day-nav').parent().parent().offset().top ;
            console.log(navHeaderTop);
        });


        $(window).scroll(function () {
            if ($(document).scrollTop() > navHeaderTop - 50) {
                $(".display-day-nav").addClass("fixed");
                $('body').css('padding-top', '104px');
            } else {
                $(".display-day-nav").removeClass("fixed");
                $('body').css('padding-top', '50px');
            }
        });
    }</script>
@endpush