@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fafafb;
        }

    </style>   
@endpush

@section('content')
    @include('layouts.partials.header')
    @include('layouts.partials.guide.guide-slider')
    <section id="ajax-guide-slider">

    </section>
    @include('layouts.partials.home-nav')
    @include('layouts.partials.guide.top-attraction')
    @include('layouts.partials.guide.city-guide')
    {{--@include('layouts.partials.guide')--}}
    @include('layouts.partials.guide.map', ['title' => "What's Near Me"])
{{--    @include('layouts.partials.advertisement', ['name'=>'test'])--}}
    @include('layouts.partials.guide.media-gallery')
    @include('layouts.partials.guide.top-rated-viewed')
{{--    @include('layouts.partials.advertisement', ['name'=>'test'])--}}
@endsection

@push('js')

    <script>
        $(function(){
            // callAjax('guide-slider', 'ajax-guide-slider');
        })
    </script>
    <script>   if ($(".home-nav")[0]) {
            var GuidenavHeaderTop;
            setTimeout(function () {
                $('.home-slider').ready(function(){
                    GuidenavHeaderTop = $('.home-nav').parent().parent().offset().top ;
                    console.log(GuidenavHeaderTop);
                });
            },8000);

            $(window).scroll(function () {
                if ($(document).scrollTop() > GuidenavHeaderTop - 50) {
                    $(".home-nav").addClass("fixed");
                    // $('body').css('padding-top', '104px');
                } else {
                    $(".home-nav").removeClass("fixed");
                    // $('body').css('padding-top', '50px');
                }
            });
        }</script>
@endpush