@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fafafb;
        }
    </style>   
@endpush

@section('content')
    @include('layouts.partials.guide.guide-slider')
    @include('layouts.partials.guide.list-places', ['title'=>'Restaurants'])
    @include('layouts.partials.guide.list-places', ['title'=>'Cafe'])
    @include('layouts.partials.guide.list-places', ['title'=>'Pubs'])
@endsection

@push('js')

@endpush