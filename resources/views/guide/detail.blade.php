@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fafafb;
        }
    </style>
@endpush

@section('content')
    @include('layouts.partials.search-bar')
    @include('layouts.partials.filter')
    @include('layouts.partials.guide.detail')
    @include('layouts.partials.guide.list-places', ['title'=>'These Might Interest You'])
@endsection

@push('js')

@endpush