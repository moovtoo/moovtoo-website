@extends('layouts.app')

@push('css')
    <style>
        body{
            background-color: #fafafb;
        }
    </style>   
@endpush

@section('content')
    @include('layouts.partials.guide.static-banner')
    @include('layouts.partials.guide.nav')
    @include('layouts.partials.guide.top-attractions')
    @include('layouts.partials.guide.city-guide')
    @include('layouts.partials.guide.map', ['title' => "Map"])
    @include('layouts.partials.guide.media-gallery')
    @include('layouts.partials.guide.latest-article-city-facts')
@endsection

@push('js')

@endpush