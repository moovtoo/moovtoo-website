@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-page">
                    <div class="error-box">
                        <div class="error-title">
                            Error 404
                        </div>
                        <div class="error-sub-title">
                            Page not found
                        </div>
                        <div class="error-btn">
                            <a href="{{route('home')}}">Return to homepage</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection