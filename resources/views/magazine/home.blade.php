@extends('layouts.app')

@section('content')
{{--    @include('layouts.partials.mag.banner')--}}
    <section id="ajax-mag-banner">

    </section>
    <section id="ajax-categories-nav">

    </section>
    <section id="ajax-mag-article-slider">
        
    </section>
    @include('layouts.partials.advertisement', ['name'=>'test'])
    @include('layouts.partials.mag.latest-videos')
    @include('layouts.partials.advertisement', ['name'=>'test'])
    <section id="ajax-latest-from">

    </section>
    @include('layouts.partials.disconnect-slider')
    @include('layouts.partials.advertisement', ['name'=>'test'])
@endsection

@push('js')
    <script>
        $(function(){
            callAjax('mag-banner', 'ajax-mag-banner');
            callAjax('categories-nav', 'ajax-categories-nav');
            callAjax('mag-article-slider', 'ajax-mag-article-slider');
            callAjax('latest-from', 'ajax-latest-from');
        })
    </script>
@endpush