@extends('layouts.app')

{{-- @section('title', $article->name.' - ') --}}

@section('content')
    <section id="ajax-categories-nav">

    </section>
    @include('layouts.partials.advertisement', ['name'=>'test'])
    <section id="ajax-mag-article">

    </section>
    {{-- @include('layouts.partials.mag.article-detail', ['article'=>$article, 'sub'=>'mag']) --}}
    @include('layouts.partials.advertisement', ['name'=>'test'])
    @include('layouts.partials.mag.footer')
@endsection

@push('js')
    <script>
        $(function(){
            callAjax('categories-nav', 'ajax-categories-nav');
            callAjax('mag-article', 'ajax-mag-article', '?article='+'{{$slug}}');
        })
    </script>
@endpush