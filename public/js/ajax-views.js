function getFooter(){
    $.ajax({
        type: "GET",
        url: '{{route("ajax.view", "footer")}}',
        success: function(data)
        {
            $('#ajax-footer').html(data);
        },
        error:function( data){
            // console.log(data);
        }
    });
}
function getMagNav(){
    $.ajax({
        type: "GET",
        url: '{{route("ajax.view", "mag-nav")}}',
        success: function(data)
        {
            $('#ajax-categories-nav').html(data);
        },
        error:function( data){
            // console.log(data);
        }
    });
}
function getBlogNav(){
    $.ajax({
        type: "GET",
        url: '{{route("ajax.view", "blog-nav")}}',
        success: function(data)
        {
            $('#ajax-categories-nav').html(data);
        },
        error:function( data){
            // console.log(data);
        }
    });
}
function getMagArticleSlider(){
    $.ajax({
        type: "GET",
        url: '{{route("ajax.view", "mag-article-slider")}}',
        success: function(data)
        {
            $('#ajax-mag-article-slider').html(data);
        },
        error:function( data){
            // console.log(data);
        }
    });
}
function getLatestFrom(){
    $.ajax({
        type: "GET",
        url: '{{route("ajax.view", "latest-from")}}',
        success: function(data)
        {
            $('#ajax-latest-from').html(data);
        },
        error:function( data){
            // console.log(data);
        }
    });
}
function getArticleList(slug){
    $.ajax({
        type: "GET",
        url: '{{route("ajax.view", "article-list")}}'+'?slug='+slug,
        success: function(data)
        {
            $('#ajax-article-list').html(data);
        },
        error:function( data){
            // console.log(data);
        }
    });
}